﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Jint;
using Jsbeautifier;
using Newtonsoft.Json;
using Nito.AsyncEx;


namespace Headless
{
    class Program
    {
        private static Dictionary<string, string> signs;
        
        private static string szDecoded;
        private static long token;
        private static int interrogation;

        static void Main(string[] args)
        {
            AsyncContext.Run(async delegate
            {
                var t2 = MainAsync();
                await Task.WhenAll(t2);
            });
        }
        #region fp
        public class fingerPrint
        {
            // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
            public class Audio
            {
                public string ogg { get; set; }
                public string mp3 { get; set; }
                public string wav { get; set; }
                public string m4a { get; set; }
            }

            public class Browser
            {
                public bool ie { get; set; }
                public Chrome chrome { get; set; }
                public bool webdriver { get; set; }
                public bool has_chrome_object { get; set; }
                public int connection_rtt { get; set; }
            }

            public class Canvas
            {
                public bool winding { get; set; }
                public bool towebp { get; set; }
                public bool blending { get; set; }
                public string img { get; set; }
            }

            public class Chrome
            {
                public bool load_times_native { get; set; }
                public List<List<string>> app { get; set; }
            }

            public class CurrentTime
            {
                public string date { get; set; }
                public string file { get; set; }
                public string performance { get; set; }
                public string timeline { get; set; }
                public string navigation_start { get; set; }
            }

            public class Environment
            {
                public bool puppeteer_stealth_to_string_proxy { get; set; }
                public bool puppeteer_stealth_web_gl_vendor_evasion { get; set; }
                public string json_stringify_snippet { get; set; }
            }

            public class Languages
            {
                public bool property_descriptor { get; set; }
                public List<string> array { get; set; }
            }

            public class Location
            {
                public string protocol { get; set; }
            }

            public class PerformanceObserver
            {
                public List<string> supported_entry_types { get; set; }
            }

            public class PluginsMeta
            {
                public string named_item_name { get; set; }
                public string item_name { get; set; }
                public string refresh_name { get; set; }
            }

            public string user_agent { get; set; }
            public string language { get; set; }
            public Languages languages { get; set; }
            public CurrentTime current_time { get; set; }
            public Screen screen { get; set; }
            public int timezone { get; set; }
            public bool indexed_db { get; set; }
            public bool add_behavior { get; set; }
            public bool open_database { get; set; }
            public string cpu_class { get; set; }
            public string platform { get; set; }
            public string do_not_track { get; set; }
            public string plugins { get; set; }
            public PluginsMeta plugins_meta { get; set; }
            public Canvas canvas { get; set; }
            public WebGl web_gl { get; set; }
            public WebGlMeta web_gl_meta { get; set; }
            public Touch touch { get; set; }
            public Video video { get; set; }
            public Audio audio { get; set; }
            public string vendor { get; set; }
            public string product { get; set; }

            [JsonProperty("product_sub:")]
            public string ProductSub { get; set; }
            public Browser browser { get; set; }
            public Window window { get; set; }
            public Location location { get; set; }
            public List<string> fonts_array { get; set; }
            public Scripts scripts { get; set; }
            public Environment environment { get; set; }
            public string long_window_properties { get; set; }
            public VisualViewport visual_viewport { get; set; }
            public string version { get; set; }
        
            public class Screen
            {
                public int width { get; set; }
                public int height { get; set; }
                public int avail_height { get; set; }
                public int avail_left { get; set; }
                public int avail_top { get; set; }
                public int avail_width { get; set; }
                public int pixel_depth { get; set; }
                public int inner_width { get; set; }
                public int inner_height { get; set; }
                public int outer_width { get; set; }
                public int outer_height { get; set; }
                public int device_pixel_ratio { get; set; }
                public string orientation_type { get; set; }
                public int screen_x { get; set; }
                public int screen_y { get; set; }
            }

            public class Scripts
            {
                public List<object> document_element { get; set; }
                public List<object> head { get; set; }
            }

            public class Touch
            {
                public int max_touch_points { get; set; }
                public bool touch_event { get; set; }
                public bool touch_start { get; set; }
            }

            public class Video
            {
                public string ogg { get; set; }
                public string h264 { get; set; }
                public string webm { get; set; }
            }

            public class VisualViewport
            {
                public int width { get; set; }
                public int height { get; set; }
                public int scale { get; set; }
            }

            public class WebGl
            {
                public string img { get; set; }
                public string extensions { get; set; }
                public List<int> aliased_line_width_range { get; set; }
                public List<int> aliased_point_size_range { get; set; }
                public int alpha_bits { get; set; }
                public bool antialiasing { get; set; }
                public int blue_bits { get; set; }
                public int depth_bits { get; set; }
                public int green_bits { get; set; }
                public int max_anisotropy { get; set; }
                public int max_combined_texture_image_units { get; set; }
                public int max_cube_map_texture_size { get; set; }
                public int max_fragment_uniform_vectors { get; set; }
                public int max_render_buffer_size { get; set; }
                public int max_texture_image_units { get; set; }
                public int max_texture_size { get; set; }
                public int max_varying_vectors { get; set; }
                public int max_vertex_attribs { get; set; }
                public int max_vertex_texture_image_units { get; set; }
                public int max_vertex_uniform_vectors { get; set; }
                public List<int> max_viewport_dims { get; set; }
                public int red_bits { get; set; }
                public string renderer { get; set; }
                public string shading_language_version { get; set; }
                public int stencil_bits { get; set; }
                public string vendor { get; set; }
                public string version { get; set; }
                public int vertex_shader_high_float_precision { get; set; }
                public int vertex_shader_high_float_precision_range_min { get; set; }
                public int vertex_shader_high_float_precision_range_max { get; set; }
                public int vertex_shader_medium_float_precision { get; set; }
                public int vertex_shader_medium_float_precision_range_min { get; set; }
                public int vertex_shader_medium_float_precision_range_max { get; set; }
                public int vertex_shader_low_float_precision { get; set; }
                public int vertex_shader_low_float_precision_range_min { get; set; }
                public int vertex_shader_low_float_precision_range_max { get; set; }
                public int fragment_shader_high_float_precision { get; set; }
                public int fragment_shader_high_float_precision_range_min { get; set; }
                public int fragment_shader_high_float_precision_range_max { get; set; }
                public int fragment_shader_medium_float_precision { get; set; }
                public int fragment_shader_medium_float_precision_range_min { get; set; }
                public int fragment_shader_medium_float_precision_range_max { get; set; }
                public int fragment_shader_low_float_precision { get; set; }
                public int fragment_shader_low_float_precision_range_min { get; set; }
                public int fragment_shader_low_float_precision_range_max { get; set; }
                public int vertex_shader_high_int_precision { get; set; }
                public int vertex_shader_high_int_precision_range_min { get; set; }
                public int vertex_shader_high_int_precision_range_max { get; set; }
                public int vertex_shader_medium_int_precision { get; set; }
                public int vertex_shader_medium_int_precision_range_min { get; set; }
                public int vertex_shader_medium_int_precision_range_max { get; set; }
                public int vertex_shader_low_int_precision { get; set; }
                public int vertex_shader_low_int_precision_range_min { get; set; }
                public int vertex_shader_low_int_precision_range_max { get; set; }
                public int fragment_shader_high_int_precision { get; set; }
                public int fragment_shader_high_int_precision_range_min { get; set; }
                public int fragment_shader_high_int_precision_range_max { get; set; }
                public int fragment_shader_medium_int_precision { get; set; }
                public int fragment_shader_medium_int_precision_range_min { get; set; }
                public int fragment_shader_medium_int_precision_range_max { get; set; }
                public int fragment_shader_low_int_precision { get; set; }
                public int fragment_shader_low_int_precision_range_min { get; set; }
                public int fragment_shader_low_int_precision_range_max { get; set; }
                public string unmasked_vendor { get; set; }
                public string unmasked_renderer { get; set; }
            }

            public class WebGlMeta
            {
                public string get_parameter_name { get; set; }
                public bool get_parameter_native { get; set; }
            }

            public class Window
            {
                public int history_length { get; set; }
                public int hardware_concurrency { get; set; }
                public bool iframe { get; set; }
                public bool battery { get; set; }
                public string console_debug_name { get; set; }
                public bool console_debug_native { get; set; }
                public bool has_underscore_phantom { get; set; }
                public bool has_call_phantom { get; set; }
                public List<object> non_native_functions { get; set; }
                public int persistent { get; set; }
                public int temporary { get; set; }
                public PerformanceObserver performance_observer { get; set; }
            }
        }
        #endregion

        public class resultFp
        {
            public class Interrogation
            {
                public string p { get; set; }
                public string st { get; set; }
                public string sr { get; set; }
                public string cr { get; set; }
            }

            public class Performance
            {
                public int interrogation { get; set; }
            }
            public class Solution
            {
                public Interrogation interrogation { get; set; }
                public string version { get; set; }
            }
            public Solution solution { get; set; }
            public object old_token { get; set; }
            public object error { get; set; }
            public Performance performance { get; set; }


            public resultFp(string p, string st, string sr, string cr, int intr)
            {
                this.solution = new Solution();

                this.solution.interrogation = new Interrogation();

                this.solution.interrogation.p = p;
                this.solution.interrogation.st = st;
                this.solution.interrogation.sr = sr;
                this.solution.interrogation.cr = cr;

                this.solution.version = "stable";

                this.performance = new Performance();

                this.performance.interrogation = intr;

                this.old_token = null;
                this.error = null;

            }

        }

        private static string GetBeaty(string content)
        {
            Beautifier beautifier;
            beautifier = new Beautifier();

            beautifier.Opts.IndentSize = 4;
            beautifier.Opts.IndentChar = ' ';
            beautifier.Opts.PreserveNewlines = true;
            beautifier.Opts.JslintHappy = false;
            beautifier.Opts.KeepArrayIndentation = false;
            beautifier.Opts.BraceStyle = BraceStyle.Collapse;
            beautifier.Flags.IndentationLevel = 0;

            beautifier.Opts.BreakChainedMethods = false;


            return beautifier.Beautify(content);
        }
        private static async Task MainAsync()
        {
            string[] files = { "last_test.js" };
            string content = "";
            var currentDir = @"D:\headless\Headless\";

            Random random = new Random();
            token = 957375338;//(long)(random.NextDouble() * 1073741824);

            interrogation = random.Next(240, 300);//time of work the script

            foreach (var file in files)
            {
                string fileNameNew = file.Substring(0, file.IndexOf('.')) + "_new.js";
                content = ReplaceInFile(file);

                try
                {
                    var bigStrings = await GetFuncsOnStart(content);
                    if (bigStrings.Count < 3)
                    {
                        Console.WriteLine("Error parse! bigStrings!");
                        break;
                    }
                    content = DecodedReplace(content, bigStrings, currentDir + fileNameNew);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }

                var tokens = GetStSr(content);
                if (tokens != null && tokens.Count < 2)
                {
                    Console.WriteLine("Error parse! tokens!");
                    break;
                }

                signs = GetSigns(content);
                if (signs != null && signs.Count < 162)
                {
                    Console.WriteLine("Error parse! signs!");
                    break;
                }

                var is_encode = false;

                if (!is_encode)
                {
                    var map = GetMap(content, is_encode);
                    var prop_o = map["prop_o"].Trim().Split("\n");
                    map.Remove("prop_o");

                    List<string> reverseFunc = GetReverseFunc(prop_o);
                    var arrFunc = funcSelect(reverseFunc);

                    //var p = @"IJq7u3mKVrTa6O7/RodtiOgwtN4EDvYmgmT06AhQnSwgloZRoBgmaiLR+ZvHs7Dmj0nZD2cuJmGMo5cbVZpgu96KT7Rm6Pn/64dJiOcw+97jDngm9mTo6K5QVCxrljBR8xiWavDRb5uUs/TmEEn5D44uWmFyo4gbZ5ptu8SK7rQj6Hb/1IfNiHEwjt7GDusmA2Sw6HNQsSy3ljNRchjWasDRi5vzs+/mRkkeD6wuvWFXo0Ub2ppGuwSKiLTG6Er/+IfliOswHN6BDvomsWQX6AlQRSzNlr1RlBgtaunRy5s5s43mg0lFD2Yu62F+o2MbnJrFu3WKX7Tv6N//HIfpiIIwtd4CDoome2SY6KFQPSxsls9Rgxjnap/RkZtes1fmrkmwD2AuZmHbo34bcJqKu7uKRbTk6Kn/v4cQiOcwm96tDjomZ2RF6IdQ+SyTlmNR0RjdahrR05u8s17mSUmiD30uZmGfo4cbJpoNu6uKtbQe6OL/64eniDkwj95eDq8m7WR96CpQwizIlrlR+BiBamrRGpuvs7LmL0krD8AuSmFRo9gb/pp8u+WKt7S96A7/94eOiNcwQ95hDiImgGT46OFQdSzylo1RGRjeaqvRaJsts6nmOkk6D4Qun2Enow0btJq5u2SK9rTE6KT/GoeriGswj96UDgMmL2T76JtQpyzblvZRbRgHavnRi5tcs2rmlUl+Dxwul2G5oysbupr9u8iKE7Tl6OL/wIdBiGIwSN6fDosm4GQL6O1Q6yxUluVRtRhtamzRt5tos0Dm4EnKD3kueGHPo8gbTprju5qKrbRP6Lz/IoeBiKcwJd5wDpsm92Sb6N5QwCx+lgtRrhiqaiTRUJvQs2vmR0nsD9MuR2GGo9sb5Jpdu7GKzLS66Fv/KYdQiPwwl96rDnomkmT+6BFQ6yy6lmxRZBiKamTRYpuSs/nmRklkD6Uu0mEEo/YbwZrpux6Kq7Q+6If/lIdziAUw9N7+Dqsm82T56FBQDyyGloFRRxg3apXRfpscs5fmwEkrD7EuumGio1AbhZrnu86KT7Q06DD/g4ejiIEwFt6NDo8mGmSd6PtQZyx/lp1RKxgjauXRzJt3s1rm9knsD18uh2H0o+4bGZq/u3+Ku7SB6Af/dof3iJwwlN7nDvImeWQz6K5QuixjlkRRmhhBanLRn5uhszPmnUm6D7wuX2GOo/IbpJovuwCKZLTc6P7/p4cyiMIw2N43Dssmm2Qx6GdQjixzln9R+RjxalLRSJuhs9rmT0n/D8gu52FZo7IbI5qpu7iKAbQr6OL/+oeRiP4w595xDi8muWSM6DZQVSzQlkZRZxjjaurRdputs6Hm50kGD6YukmHSoy4bH5pKu8+KmLSV6AT/kYfZiDsw7N67DjomVGTw6F5QOSzYluRRXRhoaszRzZtUs5Xm0knXD18uv2FWo60bhZomuyWKyLTC6Kj/2YfFiHswCt6UDpcmZmRk6P1QMSxmlsZR2BhAaoLRyJvds3fm/0nWD7MuIGEYo1sb6pqAu52KILTF6ML/GIfWiKkwe94zDromYWRi6NhQ0CxAlmZR8Rj8agfRuZvFs97mKkm0D3su1GH/owIbNJqFu++KkLTU6Jf/YIcLiPgwod5pDlImm2Q66HZQnCysllBRshjxauLRa5uys+nmqEk4D2EudGHIo4wbhpovu8OKy7QJ6I//gIdMiEYwud46DkAm32Ti6EVQdizSltJRXRi4avrRuJsjs6jmIEmpD4QuemF6o+EbwJqku9CKkrR86Ef/p4eoiHgwdt7sDmomW2Sd6N9QYSy+lsFRzhhIav7R0ZuXs1jmJUl6D5kuuGHjozYb9prquwyK87SM6Fr/Q4eMiEMwR96bDv0mPmRp6OBQriwOlpZRxRjoahTRiZt6s6nmhklgDy8u7WH9o7wb3prRu06KNrSL6K3/TId7iMowMd4HDp4m62Qn6KZQ+izDlnZRjxjEaqzRLJsjs0rmnEmMD7QuDmHOo9obHJqQu7aKW7RW6Jr/QId6iPgw5t54DlQmrGSj6FBQuCzilt5ROxjxaifRoZv+syfmEknjD+QuvmHNo+wbYZocu6WKtrRo6DP/4odjiFEw0N7yDiMmpmTQ6N5QXyz3lsdRzRgqajTRPZucs6Hm4Ul1D5Mu1GEIo8sbsZpGu0WK9rRg6Hv/kofMiEEwet79DqgmdmSl6JdQ3ywPlrFRdxi3arnRGJsJs8nm7EmzD+su4GFaoykbopqMu0OKO7TA6Ez/f4fpiKgwUt6hDtIm5mR26IxQ+SyBlhVRGxgqap7RopvisyLm6EmcDz0u8mGho38bc5rzu3yKPrTj6MD/ZodKiNkw1t5bDpMm2mTh6FhQnixWls1R9BgTahDRnpvis7rmiEnuDzguGmGTo54be5pOu/CKWrR/6JP/2YdbiLAwzd6kDgwmv2Tq6KNQISw5lnVR1BiparrRJ5vMs5TmMUn8D4kuUWFjo5cbbJpCu8WK3LRQ6Ef/+oeoiGQw4t7jDrkmHWSL6F5QuyyBljRRaBjvat3RipuNs+jmTUk4D6kurmFro3Qbx5pAu2aK8LT26GT/gIf8iOYwFd6kDpImkGQB6BVQUyz4lphRuhgIapfR25srs8zmkEk7D1IuhmFlo2wbz5rxu3aKXbTw6O3/XIeuiMkwyN4fDqomYWSz6JVQNixslulRyBiCapTR1JtQs0nm8UmwD3gudmGRo0IbZ5qcu/KKdbTs6N//4Yd0iPEwk96kDiYmZmR86MxQuyy7lhZR1BjkajXR6puqs2zmNkmBD1suWWHIo+0bZ5oJu/CK3rR66L3/k4f2iC4wg95kDqomhGQY6CBQ4CzFlqtRlBjOajnRNJvis+TmLUkuD50uamFUo+wbw5pCu6SK1LTX6GT/q4fmiNAwJd4dDicmlGTv6JxQYSzrlvpRKhjyauzRZJtgs5rmYUk8D/0unmFXo1Mb+prvu1qKjLT66Pz/VIeCiFswit66DjgmDWSc6OBQuCzwltlRQBgWapDR4Jtrs1Xmy0k6D28u7WHBo2Mbo5rdu8KKC7Sv6PX/sIdYiDkwft7NDqwmgGQF6PRQ9Cw5lsVRlxhOakjRl5tos3/mzkntD1wub2Hao9MbaZqfu+mK5rQg6LT/V4eIiIIwY94JDscm4mSf6PlQ9SxsljZRmhiMaljRW5vps2fmaEmZD9UuIGGGo6kb/5pSu5OKyLSo6Cv/BodniJcwmt63Dncm0WTB6B1Q1yzoll5RYBjzalvRSJvzs/jmWEltD9Yu6GFno74b7Zr1uwKKk7RC6Iv//IcFiCcwwN7vDuEm9GTK6FtQTSyPlpRRSRhWas3RQJtks//mzElID5Uu9mHZow0biZqRu7CKXLQh6H7/z4eliLowFN70DuUmPWT76IpQfixtlqxRRRhmavvR7Jtes0fm0UnyD24u7WHEo9gbD5r0u0CKpbSa6AH/NIfFiOAw597XDvQmW2QF6KhQtSxslmNR2BhOamvR85vZsyLmh0nfD8EuU2Gmo9AbhpoouxyKQ7Ts6IX/i4cIiOgw1N4xDswmgGRj6E9QuixmlltR+hjBaiDRapv8s/XmfEnnD+su5GE/o/UbXZqvu4uKBbQg6Of/94fiiOYw+N5LDicmlmSQ6EJQMyz6lnBRThjGavrRS5u8s8/m2kl2D4Uu5GGxoxobPpp+u+KKpLST6Af/6IebiD0w0d6PDkAmT2Tx6FtQdizalsRRSBhkatbRzptas6Hm+km8DzkurGFAo9AbgpocuwWK+rTO6Jv/5YeRiFAwJt6JDoomQmRx6MdQTSxulsNR/xhJao7R3ZvJs3nmpUnID4YuNWELo2Ebl5qIu4aKKrTK6On/LIfHiJcwQd5VDocmT2RI6PlQ3yx1lnhR1RjPamTRv5v3s+7mJEmUD10ukWGEoxUbDJqdu+eKu7Tw6Oz/T4criKMwkN5VDjIm8WRQ6HxQ8izNllhRmRipat3RR5u3s8zmtkk3DxsuVGH5o7gb65oUu8SKmbQP6NP/u4dhiDMwl95rDjwm+GTv6EhQbizRlshRcRinavnRy5sis/TmWEmLD5guHGENo8Qbz5qbu+aKkbRe6AX/goe3iFkwd97mDjgmbGTZ6OtQeiydlvBR+BhXaonRkpuvswfmHklqD+cupGGCo3obxZr1ux2K9rSD6Dn/WIeJiEswR978Dv0mX2QP6MpQ7Cxolr5R6RjkahnRqZtfs9vmokkVDxMux2Gco4Yb6pqSu1yKTLSV6Oz/f4duiP4wPd5dDsYm/mQj6KNQzyzYlklRjRj2aoDRPZsys3zm9En8D6YuAGHoo/QbCJrWu5aKfrRL6Kn/WYc2iM4w8N51Dmom9GTP6GlQhyzIluFROxiQakjRmJuvs2TmAUnqD/guiGGGo8obbZoKu5KKo7RE6Gn/z4c4iHIww97RDlImt2St6MlQUyymltRRqBg1ah3RZZv4s5rmi0k0D+8u+mElo8Ub7JpOu2CKqLRA6Ez/44eeiCAwVN7NDukmaGS56PZQ+iwjloNRSBi2ar3ROpsas9rmykm4D90ulWE4oygbk5quuz6KabTK6GL/WYfBiM4waN6VDuIm7mQL6I1Q5yy0lllRMhh+as3R/5uqswTm+EnfDzYujmG7o3sbTJqYu22KfLTe6O3/WIdUiO8w8t5xDqwm6mTm6AhQiiwjlqVRrxgLaivR5pv3s5fm+Un2D2suC2Gko4EbVZoxu9yKSrRq6PP/wodXiIcw4d7/DnkmkGTn6KRQKiwIlmVRxhiharDRF5v7s+jmKEn0D5AuZWFgo/QbU5pYu/mK37Rp6FX/+4fwiFowv97yDtcmAWT06FlQiSyqlmBRDxjHasrRlpvxs9jmZkkFD5QusGFko0Yb+5pxu1KK4rTM6Fb/pIfeiNkwSd6vDuYmr2Q56AhQRyyMlvBR8BhvatTRz5sTs9vmt0l+D1Auk2Fco24b3prEu0aKCLTg6M3/VoexiNYwtt4aDpQmL2Tf6PdQcyxvlp9RzBjwavnR+5taswrmlEmOD1UucmHuo1Ebdpqeu82KI7SO6NT/64dxiLAwxt6xDgAmCGRz6NlQpyzwln5R1RiXagrRzZv1s2zmY0niD20uQGHdo8AbcJpQu/mK+rRG6ID/+Yf5iCcwqd5LDq4mi2QL6BBQxSz5loVR5xjRak/RTJuIs7DmWUkwD4ouRWFYo5Ab95pwu7GK67St6BL/v4f4iIkwVN5jDkEmwGSF6J9QJyzJludREhjSauHRQJtVs5nmc0lFD5gu+2F/o2YbrZr5u0mKkrT36Nz/HYefiHwwgN7wDhQmeWSM6N9QpizaltRRKxhTavPRgZtls0TmmklBD3Iu0mH+o0gbrpr8u+GKSbSm6O//14c0iCYwVN72Dp4momQb6JtQjywKlp1Rshhnam3Rrptns3jmhkmKD3IuHGH8o+4bSZqgu+OK4bQP6JX/foeniPswOd4RDvYm92Sq6IZQySxalglR/Riuai3RP5uKszPmV0n9D6guf2H2o6obrZoSu+yK+LTS6An/P4dliM8wht6GDhomy2TV6BJQ5iyVln1RSBiaan/RSZv8s/vmYUkRD/Yu+WFIo5Ibk5rHuyGKlbQo6MD/uIdziCgwwN7CDrwm0WTW6CVQXSyklvtRYxhnasvRZ5tVs/3m+Ul3D6Au82G6o0QbhJqTu4CKP7Q66Ff/jIfwiPAwed7JDu4mEGTN6LxQZCxSluBRTxhLavzRiJt0s1fm/UnsDwQupmHwo7UbH5qou3OKlrSh6DP/Y4fLiMIwlt7KDsImbGQS6LRQtyxJliJR2hhmal/Rx5uhs2XmtUmiD/8uTmGuo8wbg5oOuziKZrTA6KL/oodsiMcwyd4PDsEmv2Rt6HZQ7CxxlmpRhRjcanXRTpvms/3mWkmkD5su6WEFo4QbfZqQu6SKPLQu6Nv//YfyiPkw5d5fDl8mpWSw6GpQdSySlntRbhiQaunRfpu1s+zm40lcD+cuxWGNoykbJJpxu8uKoLSm6C3/54eNiA0w0N64DmQmOGS06GhQNSzalthReBhSavnR+JtXs6DmzkngDx0u6GF0o4YbvZovuyWKyrTZ6P7/x4fEiCYwC96kDqkmcWRk6MxQeSwFlsxR8hhcaqbR+5v8s1HmoUnPD8IuGWE1o1cby5qqu4KKY7Tn6On/KYefiLwwYN5nDrcmM2R96PpQkixjllFR+RjvalLRtpuAs+rmAkmDD2cugWGoozwbN5rAu+uK8rTN6M7/aIcSiPowrd5uDjgm2GR56FdQxiz6lnJRvRjsauHRUZu3s4/mhEkJDyEubGHIo6kbpppvu9mKyLRQ6Mv/vYd5iHwwpt5tDn8mkWTO6FVQbiy4lt1RVxiCatfR6psfs6LmKEnYD6IuAWE3o9wbz5qzu9qKzbRN6F//o4e2iGIwd97GDmwmHmTe6PRQVyy5lupR+xhfav3RmpuSswzmPUliD80upGGmoyIbzprEu1CK0bS96Hn/fIemiG0wf96GDs4mdGRK6OxQ4yxOlvtRxhjrah3RuZt0s4bmmklqDyEuy2HEo5kbn5rVu3yKHbSs6LD/Iod2iMYwYN5uDoom3WRV6O5Qoizglm5RsxjSaoDRDJs0s37m6kntD7EuLGHMo9gbEZrau/mKfbRt6Ib/ZId9iN4wyd4rDgQm6GT66FJQpCzDluZRGRiman3Ri5visz/mJ0neD9MutGHbo9EbJZoZu6WKrbR76Hr/2IcmiFgwzt7rDm8msmT86KNQHCykloJRhhhGaibRdJucs6DmiEkiD9ku+WEmo48bn5pku2GKjLRP6EX/+YfbiEkwZt7CDusmeGTn6OhQ/yxYlvVRMBiuarbRa5shs4zm60miD+wuyWE6oygbmpqpu0mKWrTg6GL/B4ebiPowXd6wDv0m+GQO6JBQzSyAlgxRKRhGavfRq5vnsy7m+EnhDxsu2mGeo30bVZqXuyuKfbSI6JL/cIdQiP0w9t53DrAmxWT76E9Qqiwwls5RlRgzaiXR6Jufs5rm80mUD2wuJmG0o70bYpp1u8mKXrRk6Mv/q4dXiIMwy97/DmYm/WSC6ItQTyw0lmZRjBigavDRb5vus8zmGUnbD7QufmFjo/YbXZpWu96K0LR86Gf/1IfNiE4wt97MDskmOmSU6GJQgCyjljJRLxjoap7Rmpvzs+/mM0ldD7Iu+2Fpo2cbzZp7u1aK2bTm6DP/9oe0iLowZ96vDuYmtWQ+6CFQJSzAlqlRuhgFapLR5Zszs/Hm40krD0gu4mFtoz4b3Zr/u1iKV7To6Mz/bIeDiO4wx947Dp8mUGSP6IZQKCwKlsRR+Ri4avXRyptvsybmkUm0D2suMmHto2QbbZqfu9WKerS36PP/2odQiKQwk96WDiUmC2RR6MZQ/SydlhlR9xj5ajLR+ZuUs2PmTEm5D0cuYWH4o90bX5pVu8mK1bR76L//xIe6iDcwg95LDqUmoWQV6BdQ9CzjlodR3BiQakzRK5ujs6DmZ0lED/4uWGEPo+8bwppBu52KzrTq6Er/joeTiLgwBN4pDkom92S/6LZQcSz7ls9RNRjSaoPRe5s3s4jmREljD/wu0mFEo1cbzprMu1yKtrTp6Nv/I4e2iFgwqt6pDmAmNGT56OdQuyzPluZRWxg4ao7Rt5tZs1vm6klkD2kunGHBo1cbtZrSu8KKe7ST6Nn/q4coiB8wY97kDoUmtmRy6NZQ+iw2ltZRjxhkamDR8Jtts2Lmy0mRD1suDmHCo8gbRpqBu/qK/LQJ6K3/XYe0iL0wGd4QDv0m72SG6OlQ2yw5lkxRjRjgamDRa5vLsznmHEmXD7kuPWHho/wb45p4u5CK87SS6CH/YodKiMAwi97gDgsm7WTV6EVQnSyTlitRMRiramrRa5ubs+bma0lzD9wuqWFbo6Mb55roux+KlrRR6Jj/jIcliDUwl96eDpEm22Tn6FlQPSy6lpZRVxhBavLRJpt5s9jmw0lGD4Mu4GG2o08bkprku5OKXLQH6Gv/5IeAiKAwc96SDpgmJ2Tq6OFQfixplqdRXRg2auvRmZtis3HmqUnPD0ou/2HRo98bCprwu1OKtbSl6Cf/L4fPiOswvN7+Ds8mMGRG6PVQ4Sx5lmlR6hhGalbR3JvasyDmg0ntD9wuTmH+o5QbmpoquxmKPbTn6Jj/vodyiNgw5d4tDtkmqmRk6ENQhSxslmRR7BjdamfRWpvBs6jmWEmmD5Eu+mFdo7YbdJqqu/yKBrQF6Nz/+IfriOgw8t45DgwmmWTs6HdQTyzslnBRRBj5asPRUpvgs8/m5ElED60u6GGXozkbBpp3u/SKnrSi6Af/kof5iCowxd6hDkImMGTx6DBQSiyfluRRJBhMauvR0ptxs5Dm2knsDwYupGFxo5IbnZo5uzqKhbSe6L7/3ofSiGEwON63DrgmemRT6ONQbixkltNR6xhdaoDRwJvYs1Dmk0nED9EuD2EXoyAb6Jqbu56KCbTQ6MD/L4fIiJwwQt5wDvkmU2Rs6OZQzSxLlk5RyBjOamrR4pvIs8bmDEn3D0YupGGpoyEbGZr+u5WKp7SI6Pf/aocLiJAwst5gDlYmxWRw6HJQ+CzhlnJR4hj5aufRb5uPs8zmkEkIDwcuamGbo7gbnZowu46K5bQp6O7/4Ic+iGMw895zDkgm8mTx6EdQcyyzlvRRWxiyavLRuJsgs6rmZ0m2D7wuNGEGo94bmZrqu42K8LR96Az/74fuiFYwKd7rDmsmZGSF6OhQQyztlsBR3RhSaq/R7puYs1/mZklVD/8uvmGmo3Qb+JqduxqKkLSL6Dz/MoexiHAwSN7+DoMmUGQN6NVQ9iwGlqBR1BjCalrRgZtls9DmmUkiD3Iu/mHYo4Eb7prnu1GKULT56K3/NodKiPwwZN5GDpMmyWQ+6IdQqyzulmxR8RiVao3RJptis0Tm5km6D6ouJGH1o58bSpqOu6GKc7Qz6Lb/Z4dhiPsw295pDg8mqmTh6GZQkSzGlsJRFRiyairRrpuhszzmcknFD8Eu5mHLo9kbY5otu/mKkrRW6C3/zYdciEgw6N7sDkkmsmTe6O5QTiy3lslRjxgBahrRRZv0s53mpkkVD5Uu1GEdo9QbvJptu3CKpbQ66Fr/n4ebiFQwbd7UDtEmSmS16M5Q+CwslohRaBiHaqjRMJsXs/7m40nkD/wukWFooyUbipqpu1uKMbT86Dr/aYf+iOMwXt6uDs4m+WR56JZQkCyyll1ROhhKas/R/5uQsyTmz0mVD0ku3mHso1gbQJq5u2+Ke7Ty6Mv/YYdIiNYwrd5qDuUm92TB6DtQpix2ltZRhRgVaiLRxZvVs/nmxUnlD00uMGGVo6MbeJpKu5uKP7Rf6M7/2odQiJUw7t65DmYms2TD6NVQWywIljlRzBj+aojRe5vTs8PmKUmPD7gucWFuo6UbYpo7u52KxrR36Fr/oYfViGkwhN7zDusmC2Ty6FNQgyy0lhlRNRiFauzR4ZvQs8/mIkkLD6QulGE2o1Ab4Zpju2SK37Sq6HT/vIfoiNkwed6dDpMmuGQi6CtQQSzElpVR4xgyapjR2Zsns9jmlkluDzoukGFYo24b0pr6u1mKWbSs6Pz/RIeHiNowxt4dDpQmIGSU6LRQPCwVlsdR6xiVaovR5Js+syrmmkmeD3cud2HSo1obWprTu6KKfrSe6K3/+YdwiIMw8t7aDgwmEGR36M1QmyzhlhBR1hjDaivR1JuDsz/mREmRD3AuQGHPo5kbWppHu8KK9bRH6IT/2ofIiAgw8d5oDocm5mQW6AFQ6Szmlp1R7xjOanvRFJuIs4jmSUlGD9ouR2Fbo+gbtJo6u5GK6LT56G//k4fYiKwwJt43Dj0m/mSL6IhQOizZls5RRxiLaqPRTptks7PmXUlYD8Mu/WFCo1Ab35rwu3qKorSX6Pf/GYeGiFowm96/DgEmJWT+6O9Q5Cz4lsBRaxhOapLR4JtFs07my0k5D2Yu7GHKoz4bvJr7u7mKB7T06NX/gYdYiAAwVN7EDpwmlWQT6OxQwCwulvVRrhh+akjRj5sis17myUnmD0kuR2Hao6IbSpq5u5mK17Q86K7/WIfJiPQwYt4pDsAm+2Ti6PlQxCxIlgVRoRiUal3RaJvhs1DmBkmZD/UuXGGWo+kb7Jp5u/CKxrSa6An/Z4dWiJ4wit6EDnQmkGT96AxQ8CyLlmRRYBi3al/RIJuTs/vme0lTD/Eu6WFwo7EbwprXuyaKg7Qg6In/poc+iDkw597hDpMmxmTO6FFQTSzrlrZRfxh4apnRX5tfs8bmyUlnD5IuwGG7o30bl5rNu7SKILQl6HT/n4eFiLYwOt6YDpgmD2SJ6KlQXyxwlqlRPxhMasnR3ptLs2bm/knZDw0uoGGPo9gbD5rou2KKobT/6AT/EIfFiPkwu975DswmPWRU6IxQvixclklR+Bg/akbR3Zuus0fmkkmgD+EuTGG2o88bupoFuxCKdLTq6Kj/iIcPiPUw4N4wDswmjWQ56GFQlyxflm5R/Rj+al7RD5vBs6vmTkmnD9oupGEHo7AbRZqhu4GKCbQ26Jv/zIeHiPYw4N5jDgom8mSR6HtQayztlj1RVBjMas3RVZu4s/3mw0kID5Mu9WGzoyUbOZp9u/eKvbT96Bv/94fXiAswy96hDn8mY2T46EJQdSzNlv5RYhhJatPR+Jtxs5zm7UnAD1guo2Eno5Yb4ppnuyyK2LT86Kb/5YeZiGYwDN75DuomWmQ66OJQJixElsZR8RhZarbR6pves27mnUnWD6UuI2EIo0cbzZqsu/mKELSW6OH/Nof3iKowZd41DvEmT2Rf6IVQ+ywlllNRzRjpag/RopvTs9bmP0mpD3AuhWGbo34bcZrdu+GK4bSM6Nn/U4caiIEwn95qDkYm0WRj6HdQ8yyjllVRmBjZavfRWZuXs83m00kbDwsua2Gfo5sb4Jpwu/SK1LQy6NP/oYdHiFYwlt5YDmgmzmTL6GlQUCzsls5RBhjsaszRw5sCs6LmWkmgD/QuM2Eso8obzpqCu92KxrQ46Ef/jIeyiEIwd97cDmMmB2TG6NxQSSyAls5R5xhVavPRlJu2s13mN0lbD8guvmHroxYbxZr1uz2K27SN6Hn/LYeTiFEwZt76DtkmXWRN6MtQ2CxUlptR4RjhajXRkZtzs6zmg0kgDyUu4WHio5Qb5ZqZu3GKGrTw6L3/R4cxiP4wXN5EDv0m62Rl6LFQ3izClnRRlxjTatvRDJs5s3Xm6EmED7QuaGHzo+MbKZrvu5aKcbR06Lj/aodYiOgwnN5DDmkmwGT36GxQmCzylvxRVBioaiHRrZuUsyHme0noD8UukWHRo5cbWZoRu/CKgLR26FX/xId9iAow0t7ODmQmgWS36P9QWiytlsRRtBgBagbRQZuXs/TmqEknD+ku/mE+o9kb6JpGu1uKjLRK6Gz/8ofbiF8wT97hDt0mVGSb6O5Q6yw7lpJRKhimaqnRNZt6s53m3km/D+kuzGE8ozIbtZquu16KV7T96Dv/C4eFiKMwIN77DqgmvGRM6KNQ7iyxliFRHhhoatTR5pvqsyHmz0nVDzsu6WHgo0MbMJqTu3yKPLTn6Pn/dod8iM8w2N5bDo0m6mTi6FtQlCxwlrVRnhgYagnRy5uVs4Dm7UnmDz8uKGGvo+8bZppsu+CKYbQK6OD/q4d1iI4w/968DmomvWTR6IBQGiwLlkBRxhiJauvROpvEs9rmHknvD4MuT2Eto64bf5pku8mK3bRj6G7/oYf4iHwwl97kDrYmPWSL6HxQhCyZlhNRCRjeas/Rtpvas8TmWUkcD4oui2Fuo0ob+ZpYu0qK8LTL6CP/h4fwiNwwDN6gDtAmk2QA6AFQdSzilqlRvxg7atfRyJsKs/rmlEk7D2Qu72Ewo1gb5Jr7u1SKWLTc6LD/f4e7iO4w6943DoEmSmTT6J9QZiwNltxRzhj9asnRk5tasyrmr0m6DzguamHBoz0bbprDu9aKdrSs6M3/yIdxiIIwx96BDiAmZ2Qr6JdQ9SzmlnFR1hiUai3R95uDs2rmaEmrD3EuYmHIo+sbQ5pwu7eK8LRy6IL/8Ye6iDcw/95xDqcmvGRo6ApQyyzOlp1RjRiUaibRSJuGs67mfElWD5guUWFGo8Ub7Joju5GKqLTu6Ar/hYfQiI8wNd4HDiEmyGSl6LZQISzZlpVRJxiPauDRPJtAs5DmWklND5gu62FhoxUbzpqsu1KKsrTj6Nn/WIe3iCEwp963DmImGWTJ6J1QuCyNlspRWxgZarTRnpt7s3XmxkllD3MuxmGqozob7Jrpu+eKdrS/6Pb/hoc0iCcwUN7PDrUm6mQQ6JhQ3yw2lvlRmhg5amXRsZtRs2jmyUnqD3kufGGgo6gbX5qBu/OK37RY6JH/ZYfNiIEwAN4WDsYm5WSZ6I1Q6SxDlihRhxi/akfRN5vEs2LmDUnCD88uXGGfo7cbwZpru6KKwrSt6Br/HYdqiPwwgt6ADjEm8GTq6DdQ6Cy9lmVRMxj5anvRZ5v6s/3mXUlvD/YuqmFSo6wbxZrmuz+KkbRK6If/+IcfiHYw2d78DrgmjGTT6F5QJSyolu9RTRhwasrRTptks57mo0lzD4Iu62Hmo20bhpqWu6+KHbQj6GX/z4e5iIQwKt7kDuUmEWTS6LxQRSxSlpNRORh5aprRzZt2s3Tm90nxDwouoGHzo8cbGZqNu3qKmrSU6Bf/AYediJgwnN6LDo4mZ2RM6JFQ6CxdljVR8Rhxak/R85vJs2LmjUnCD+guW2GTo+wboZoFuyKKSrSF6Lr/mIcgiNAw995PDvgmn2Rn6HJQpSw8lnVR3hjEakTRXZvSs9XmR0mQD48u4mFeo+wbdpqWu5qKJrQE6Pz/1IfiiI8w6t5+DjomqmSw6DhQaiz8lkFRCRjLavPRdpujs6zm/kl7D5Qu7mG4owcbJJpVu+2KobSc6A//lYebiA0w9d6xDkomaWSm6ENQRSySluBRaxh8auzRqJsOs+3m+knkDwsu92Ego7sbr5o/uyKKzbTJ6Lj/iof0iEYwCN6RDqEma2RG6JtQWCxYlv9R+RhYaq7Rrpu4s1nmnknOD68uPGEFoyUb2JqZu/2KB7Tq6ML/SIfMiJAwZt54Do0mR2R26OZQ+SxElnZRqRjBamrRn5vRs8nmP0n2D1AulGH0owgbIprpu+aK4LTz6OD/f4cUiLIw695LDjsmmmRQ6EpQniyrlldRhhjzavbRcpuCs/fmhkkfDx4uVWHMo/0bi5oMu/OKz7Qn6Iz/kYdeiFUwq945DjYm2GTd6FlQWCzLlvZRVRiSaszR1Zsms7TmVkm0D6IuIWF3o50bxZqxu86Ky7Q56DP/moeciEEwZt7YDnsmbGSb6MlQYSyslvlRvxhyaq3R95uGswXmNUlxD80uhmGaowob8pr/uxyKybS36HP/ToekiEQwa96aDsUmZGRM6MpQ1CwOlrpRyBi/aj/Rv5tUs9rmoUkcDzsu9mH6o4Qb6ZrtuzGKErSI6Jr/Zod4iOEwQt56DsMmy2Qh6KRQ/izklhVR/hiZapTRPJtnsyPmyEmID6EuOmH3o/kbFJrPu+qKOrRE6Kz/PIdZiJMw3t5nDlUm/mTd6GRQoizRls5RNxj2aiTRppu1syHmJEn7D/0uu2GJo8obappQu6WKmrRc6Hj/g4c/iHUw/d7DDmEmt2TZ6PVQayyDls1RgxgFamDRRpubs6Tm+UkFD+0u/GEJo8wbrZpquzuK8bRa6Gf/gYfjiCQwEd75Dqomb2SM6NlQwywilopRcBjbaq/RIpsxs53m90mfD4Yu7mFcow8buprru1iKarTc6Dz/B4fwiKgwfd6yDssm32RS6PZQxSytlipRFBgqasjRnZvmsyXm60mcDzEu72HqozAbU5r0u2WKILTm6J3/e4dQiM4wyd5oDp0m+mT/6FpQ9CxIlohRhxgcanDR+Zues6Tm20nND2ouOWGoo60bQJpnu+WKXLRo6Nz/o4dWiL8w/N7/DlcmkWTS6KdQBiwQlmJRlxj7aqrREpv0s5zmC0mUD6EuZWFso4gbSJpEu/yKzLRV6Hj/roeiiHQwo96RDsMmWmSK6FpQhyyVliFREhjHapjRupvts9LmJklGD6cuo2Fbo1cbkJp6u1CK5bTB6Hr/tof9iL8wc97sDuEmrmQD6GVQWyzrloJRhxg1ao7R6JsLs/rm6ElZD24ulmFKo2MbxJrYuzqKVbS36Ov/SYeTiIswyd4XDoEmd2SK6J9QBSx1lu1R5hi/avLR4ptLs1Dm60mID38ubmGdozEbb5rmu/CKdrSt6Nv/uYdciIkwxN6ADj8mJ2Qi6NdQqiy7li9R7hjEak7R2Jvus0bmVUmsD1AudmGfo/0bJ5pQu+2K/LRy6LD/64f8iBQwl951DrEmn2QL6BRQ4CyblrxR1RjyaljRM5uIs6zmWkkwD8YuRmFuo+4b05pXu4WK1LTj6An/kYfniJQwA94yDnYm6GT16KVQDizllupRGxjIarPRbpszs7HmXUlJD/oum2FBo1sboJrPu2+KlbTm6OH/NYeyiEcwuN61DjImDGTg6M9Q4izolsBRPBhLapHRvZtMs2jm6kllD1ku8GHvozobvprXu+qKV7T36M7/locZiAYwdN74DrsmtmQT6NZQ7CwHlopR6BhvajbR+Ztcs33m4EmdD2QuSmH8o/cbZ5q4u+6K7LQX6Pf/XofRiKwwY947Dt0m9WSd6NVQ1CxtlixR8BipajzRe5vKs23mf0nED68uI2Hho6wb55pdu72KybSF6Af/BIdViOwwnd6gDiEmymTA6CdQjSzslkdRMRikaj7RXJvTs83mR0kHD8Eu6mFqo+Qb15rXuySKtbR/6I//q4coiC0wxd6eDr4mx2SQ6G1QGCytlr5ReRhBau3RZZtns9PmrElwD4wuq2HCo3gbs5qWu9WKCrQT6H7/n4f/iKIwLN7QDvQmFmTH6O1QUSxhlrJRQxggasPRwps+swjmrknND2kup2GZo/UbF5qJu0GKhbSs6Dz/eoftiJgwud7aDuQmWmQc6PNQrSx6lmxR7hheannRzJv5s0vmoEnLD9cuWWGKo8gbqpoiuzKKS7T26OL/nYcFiMwww94eDt4ml2R46FdQqSw6lj5R/BjmakvRFZu3s+3mEUm4D8Yu3WEMo7AbIZqyu4mKFbQS6J//wIeIiM8wmd59Dk8mi2SP6GVQUizplkpRThjfavnRZpuEs9rm3kl8D40u1WGAoyobFZoiu8iKj7Sq6A7/mIebiA0w8N69DmMmS2SH6GFQaCzHloNRRxhGavfR0ZsKs6zm8kmkD1ou92F1o4QblJoGuzeK47TG6Jb/7YfKiFgwFt6BDq8mY2Rp6M1QTSx7lsNR8xhzapXR0Jvfs1HmjknrD7AuPWE/o1wb15qvu52KM7Tv6MP/SIfuiJ8wRt42DogmQGRa6M9Q3yxTlkhR/xj6alrRvZvts/nmK0n+DyIuk2GVowIbd5rZu+yKtLTr6Nf/b4c5iLYwu942DmQmz2RP6FhQ0iyqlndRhBiraqDRD5u/s9fm20laDx4uI2H6o6Qbn5oFu46K7LQv6I//jIc+iEcwj94wDkQmkmSY6HtQeSzvlqJRVRijasbR45tZs4nmZUmtD4MuImEho9kbhpqmu9mK+bRC6Er/iYehiGUwQd6aDmEmWmTk6NlQIyzslvZRwxh4apTR8ZuqsyDmeklaD5cuhWGdoyob7Zr0uxCK2LT26Fj/SIeMiEUwbN7JDssmf2RI6NdQyCxMlpxRwhjCajvR85tHs9fmt0k8DxIuhWHJo+obi5rbu3OKN7Sm6I7/PIdXiJkwTt5tDuwmymQp6KZQwiy5llNRgRjJaqvRLJs7s3Tmy0mgD5QuDmHmo5gbM5r9u7yKeLRL6LX/Q4d9iJgwxN5EDlIm4GT46A1QhSz3lqBRPBiganbRupu8swfmIknaD/suk2GGo84beJo8u5mK7bQ56DH/y4cxiGgw3d6uDnQmv2TX6M5QWSydlstRlhgOamTRRZvAs5vml0knD+Mum2E5o+8biZpQu0uKh7Q56H7/3YeaiHcwbt7bDugmUWS66NFQxywOlqhRZhixapTRYZsRs5bm90mhD9Auw2FLozcbmJqJu1aKd7SY6E3/cofyiLQwU97gDusm2GRQ6JBQ5SzSlgpRCBhiapfRlZuoszvm2UnDDzgu+WGuo3kbSJqCu2SKbLTc6Ov/VIcJiNkw3N5JDoEm6GTY6DdQ8SwrlpNR/xg6ajvRw5uGs+vmiknQD2MuFGGho+kbdppMu8yKaLRd6M7/w4d4iK4w3N7bDnsmjWTZ6JpQNSw2llZR3xiLarHRKpuUs8PmT0nXD5EuT2Fjo5AbYJpEu5OK4LRc6Gb/1IfSiGow497LDvomOmSC6EdQsSzilh1REhjiatfRnJvZs9DmcEkRD48ulGFso1Ib+5pYuwmK0rTC6Hj/4Yf9iPowVd7oDugmtWQ56DxQQyyflp5RqxgpavfR6ptMs9vm70l9D2ougWFOo2Yb4pqeu1mKerT06Nf/WIeliMEwyN43Dp8mcWSD6JhQeiwplpxRyRjhatvRwpt4s0zmlEmsD2wuWmHto1EbVZrwu/mKOrSZ6Kn/6IdIiPMw+N6SDlQmI2RB6JpQuCyTlgxR7xiVag/R9JupszvmWkmVD18ue2GSo/8bQZoMu8KK6LRo6Kb/4YfjiCIw895BDqomiGQd6HBQhSz1loJR/BjJalPREZu6s4Pmd0l3D4cuZGFNo/0byppLu7iK7bTi6En/nYeQiLYwFN4HDiUmlmSV6LBQFSyKls5RJRjKaoDRXptjs7jmcUlhD+MunmFmowwbt5rhu0SKu7T46Pb/Wof/iGUwqt6mDh8mE2Tq6OhQ5SzclpNRfhgqapTRiptqszTmzEl7DwouwWHjo3wb55qhu7qKdbSt6JD/gIddiCgwIN7rDqgmi2QA6MdQ7CwIlstRqxg7amHR9pt5s17m0kmdD3Quf2H7o60bVpqRu5SK+rQ56Kn/dIeBiIkwMt5zDsEmmGS86O5Q1CwmlkpRrhiBajzRVpv7s1HmWEmTD9IuKGGHo68b35oLu/CK9rSv6AL/IYdwiP8wnd6kDhUm6mTJ6DFQ2iyglkdRLRjxakXRQpv6s5rmeEldD+ku9mF5o+Qb65r8u16K9LQm6Jr/nIc4iHAwzd7uDpAmjmTC6EpQKyyEluhRXRhQavnRaJtrs8Xmy0kgD4Iu9GH7oxEbtpryu5CKJLQy6GH/loebiKMwcN7yDpUmMmSH6L5QbCx6lrJRWxh5atvR3Jt5s1jm+0nxD38ujWGVo78bApqvu1CKmLSY6Br/LYeXiMMwld7bDs4mbWQb6JNQiSw9ljdRyxhAanPR4Zv1s2vm50nVD8MudGGOo/gbzZofuyGKcbSZ6Kn/tYcEiMUwmt4XDu8mq2RY6HpQ8yxblmtR8BjaainRaZvys9jmCEnsD8Qu/GE6o4YbUZqLu+KKabQo6OX/wIf9iM0w8N4mDicm8mSc6HxQdCyRll5RRBjAavnRUpuFs+7m+0kMD/EumGGbowMbBZpAu5eK9LTl6DP/+IfriEsw9t6xDlAmdGSl6DpQXCyclulRchhcavrRzJsVs7zmmUm4D0YugWFKo5cbuZopuy6Kn7T66OL/5ofviFswEd75Dr0mSGRE6PxQWCx5ltlRrhhSauHR0JvKswzm8EnED5IuJGFmo3ob4Jq5u4aKILTw6Nn/N4faiJUwW95wDq0mRmRl6MVQgyxBllpRyxjzanfR5Zv1s8bmGkn+D1ousGGGoxYbKZr5u9mKkbTO6Mr/S4cUiK4wvN51DmYm7WRb6G9Qwyzhll5RsBjxavvRS5uKs8HmmEkADwQuJGHKo7wbhpobu+2K6LQo6M3/qYc8iDMwhd5oDkEm8GTa6FRQayzxlvhRRxjtaujR5Jses+jmJUmQD6UuJ2Foo5gb/Jqfu+2Kl7Rf6Br/9YeviGswWt76DmwmD2Tn6OFQayyclqpR1xhEapXR1ZuDsyLmAkl3D/Quq2HmozYbmJr3u0yK2bSa6HH/R4fxiHowTd7SDuImJWRO6P9Q8SxolplR6Ri6agbRiJtYs7rmnEkXDwku42Gdo+sb65rku2KKTrSr6Jf/TodoiMYwX94NDv0m+GRe6LlQ8CzHlldR6BiRas3RBpt6syTmxkmPD+QuLGGOo8MbOprWu6GKZbQ26Pb/e4d1iMkwzd4oDgcmrGT+6A9QuiySlstRVBiLannR2punsynmOkmdD+sut2Hqo+wba5o+u+uKtbRD6FT/+Yd5iEYw097wDl0ml2Tf6OxQfyzylstR2hgAahLRfpv9s5fm/UkgD5Qu9WElo9IbiJoiu0aKibQw6Fn/yIfPiFAwTd63DugmWGT/6M5QtywvlpdRfBikapXRBpsJs+Hm/UmID9ku4GFaoxMb8ZrguzmKcLSH6Dj/XYf5iOowR967DsImzGR76JVQ9CyYlidRYhhRatbRnpuAsxHmjknJDy0u+2G1oz8bQJqyu3uKYrTM6PH/KIdMiMAw3d5yDpImw2TI6FhQoywllqxRuxh+ahDR25vGs5Pm2EnyD28uDmGWo+0bWZpyu9uKbLRd6Pr/qIcgiOQw+t7eDnAmjWTw6JZQPywTln5RzBiOauvRMJvPs8PmFUn7D78uSGFTo+sbXZo8u9+K5bR36Fb/+4etiA8wgd6PDqQmLmSk6FdQtyz9ljdRCRjhat7Rh5uLs+TmU0kOD5Yu9mF/o1Yb3Jpfu2eK47TN6HP/+4f0iKAwUN70DvMmtGQb6BJQaiztlrRR5hg7auzR5ptJs+vmv0liDy0uqmFdo2Yb05qeuySKR7Sg6K3/eYe5iPowyd5YDvYmKmSE6J5QYSwgls1R7BjkaovR4ptTsxTm8EmJD1kuN2HEoyIbVJrSu+mKXrSZ6Ov/3ocPiIgw9d7UDlomO2RF6PpQnCy+lnFR2xieajfRj5u6s2XmeknxDyYuTmHco/AbQpp8u9yK1rRJ6KX/4ofZiCcwit5aDqgmvGQc6HFQ1CzrlrZR+BjEakLRMpuxs6jmbEl7D/AubmF1o+4b3Zoku4KKrrTM6A7/lYfXiNMwHt5iDiQm/mSm6LFQFyzHluVRGBjOapLRZJtSs4Tmb0lqD50u2GFDo2gbyJrLu0eKobTy6Mn/K4fsiF8whd6jDmImF2Tq6ONQvSzdluhRcxhOapXRqJtZs03mn0k9D2YuzGH6o3wbo5qqu+OKZrTo6O3/o4cUiCkwSN6YDoYmgGQl6NhQ6ywultpRiBhnam7R9Jtjs2TmwknED0guSWHdo9YbVZq8u9aK27Q+6PD/VIeQiJkwNN42DuMm+2So6MhQ+yw8lk9RmBi7aknRTpvLsznmT0nJD/guZ2G7o9ob2Zptu5aK1bSg6DT/O4dliOQwmN6CDjom42TJ6E1QzSyoll9RKRiyamXReZvps/3mdElsD6wu6mF/o7Yb15reuzWKlLQr6I3/r4c/iHswxN7JDrUmxWTk6FpQEiyiluhRTRhFap/RTZtus+nmw0k+D6Au/mG4owsbq5rnu4WKNLQf6GD/wYeLiIIwAd7WDs8mPmTz6ItQSyxKlvBRPhh/au7R/ZtGs2zmwknPDwsurWHZo9gbN5rsu1eKtLTi6H7/NofJiMswi97zDtImcGQ46IZQ6yxKlkxRwhhQagnRzpvTs3jmnknAD+UucWGSo/UbjJo9uxaKVrTI6Iz/i4dyiPgw9N4nDusm7mRO6GNQjixPliRRmxiRamfReZvAs/rmUEmMD9Qu5WEEo/QbVZqXu7SKHLR36J3/mYe+iOswkN5kDkgmiGSM6DpQSSzOlmZRXBjtasHRJZuRs/nm2klTD4sukWGGo14bIZpIu5qKmLSB6CX/wofUiBww996yDlsmSWTv6ERQfSzjluVRJBhPasLR1ZtJs6Dm10n5DzUunmF2o4EbvpoXu3qKnLTv6KT/+YfviHAwON6EDrImXWQy6OVQfCxcltlRzBhkarDR3JvKs3LmqknXD7IuGGEHo0Qb95qFu/mKO7Tz6OL/R4fYiKowON5nDpomfWR86M1Q6yxDlldR1RiiamTRppuYs93mB0n1D2UutmGXozcbFZqcu56KgbTI6O//OYdIiLcwod5lDmUm0mRK6ERQ7CztlnVRuBjvarfRZ5u2s9rmh0k9D2QuV2HUo4AbsJpxu/SKwbQ16Mz/rodwiDMwq947Dn4mm2Th6GFQESz+lvBRSBidasTRxJsns6bme0mJD6AuOmEuo5wb+Zrnu8uKk7Q56AX/oYe0iGwwUt76DkEmDWTn6O1QIiyflv5R/Rhfar3Rz5uwsyvmG0leD4EuvWG5ow0bkprHuzGK2bS76EX/W4etiDswN976Dp4ma2R26P5Q3Cx/lp9R7Bjeah/R9ptxs4nmi0kfDzQu2WHXo4Qb3Jrmu0GKKLSn6PL/PYduiO8wSt5qDs8mqGR16IJQyizdllpR8BjFarXRL5sLsz3m6kmZD5kuC2H1o/kbGJqIu7iKP7RG6Ib/W4cgiPwwkN4nDl8m6WSv6G1QrSzqlr9RGRiCal7RspuVs2DmKEmFD9UugGHto+4bOJosu4WKv7RX6EX/6odQiAsw+970DkMmlmT36LtQWizwltpRzRgXagDRXpvss/TmhUkGD+ou3mEso9gb6Zpmu2+KhLRv6H3/4YfMiF8wDd63DsEmWmTt6JlQvCwelqpRRRjNaqXRHJs2s9jmyUmHD+ou0WF9owwbp5qBu3mKbrSb6Hr/WoeTiMgwZt7/DswmuGRr6KBQ0SzblhlRJxhratnRnZuesy3mlEmZD0ou0mHyo2cbKZqHu22KZbTs6Pz/IYd6iNQw4t5tDqwmx2Tq6F1QgCxIlqVR9BgFanrRzJves+Xm/EnyDz4uHmGCo60ba5pUu8KKSrQN6JL/y4chiI0wod7BDmomrmTi6KFQNSwDllRR3hj/aqXRJJvXs+rmKEnWD/YueGE1o6wbXJpYu+yK4rQm6An/7ofLiBEwrd7qDscmXmSz6ERQ0CyjliVRBxiBasvR4pvls9PmOUlLD5kuimFEozMb5Jplu1mK4LTp6Gb/k4fviMIwV97wDuEmjmQf6AJQfizklqlR/Rg3apLR+5sls+7ml0lhD3AujGFvo2Ibnprju1OKV7T26Pf/aYeTiOIwud48DrAmVmTQ6JVQPSwFlspR2hiqaojRx5tGsyjmjUmOD2guQGHjo30beJrpu/yKdLTg6LP/wYdziLAwxN6vDkYmNGRH6MpQviynlhJR2Rj4ainR65ucs2zmLUmoD00uSWH4o9wbXJoJu/2K67Rc6Jv/84fbiFUwtd4kDqQmlWQf6AdQ3SzkluJRixjTambRRpuys5rmQkluD/0uRWF4o8kb9JpAu6yKq7TG6A3//ofFiJswBd4dDloml2So6IhQJyz0lvdRNBjqau/RIptMs4nmIklZD9gu62Ego0cbwJrTu0mKrrT66Oj/WYfziCAw096oDjYmDWTm6JtQ4iz8lvRRWBgdarjR9pt5s3Lm20lRD1Uu6GHKo18b4pryu8OKa7S36O3/r4dViAMwSt6ZDo8m+WQa6OlQ+ywrls1Rgxh+ajDRr5t7s0nmyUnED3kuDmHqo+4bapqRu8aK7LQ16KH/IoewiPgwP94SDv8mmmTm6NRQySxRlhhRkxiBamnRO5vNszDmR0mcD9QuJ2Hjo/Qb3ZpRu7eK7LSx6Cr/O4dEiOUwoN6cDiYm2GTj6AlQjiyVllxRShitamDRbJvps8PmVUkOD8su0mELo+QblJr3uyaKrrQh6JH/lIcSiDsw3d7uDr0m+2SQ6FtQPSyZlrxRWhhlauTROJtos8bm3UkoD+0uymHXo10bpJrUu9qKK7Ri6CH//Ye+iJwwN97CDsgmB2TO6ItQfyxzlrRRMBhpasnRwptQs2fmrUmrD1EuvGHio+AbPJqyuyWKmLS/6GL/G4fiiO8w697oDu0mQmQM6JNQviw/lm1R3xhZaljRnZvQs3/mmkmrD6AuZGG0o5kb25oPuz6KILT56OL/uocOiNcw294tDsYmjGR56HZQjSxulldR3BjEanzRDpvCs//mbUmjD4su22FZo4IbRJrVu7qKBbQp6Mf/m4eKiNkw5t4xDjYmmmSL6CRQbCzFlkFRexj4auPRIZvks9vm30ltD5Uu7GGaoxsbaJpVu9mKtLTh6BL/94fCiAkwxd72DlgmNGTy6ERQfCz+luNRJhhqaqzR6JtYs4zm8Um8DzcuoGEio9Ybt5oZuyeKybTv6OH/iYfHiGcwEt6UDu4mOGR26JBQRSxolu9R3RhHauPRrZvks0bmj0nZD6kuJ2E3oyUbzJqAu6CKMrTq6Ob/HYfqiI0wZ95MDrImXWRn6PpQxix1lnRRqRiwakvRv5vRs+LmJ0m/D3cuimGloxkbAJr6u++KlrTF6Mf/OIcPiIIwjd5ODjAmn2R66FtQmCzdll5Rthj0asPRe5uSs9XmmkkbDzIuf2Hmo54bh5oxu8mK4LRH6NT/r4d8iEgwr95QDj4m22Te6FlQSyzRlu5RcBiZapDR1ZsVs//mIkmOD6IuNGEUo5ob+5qfu9WKz7RD6Cf/koeaiF0wdt6cDmYmDWT86MpQUiyNlqBR7BhIaqbRmJursxrmMklZD9cu+GGWo2wb95rUuwiKxbTv6Gz/O4f4iD0wRt7rDv4mS2RG6KFQ6CxTlqdR9hi8ajnRi5tms4fm9UllDxcu+2GGo6Mb25rtu12KT7SD6KD/PIdOiMswPN4IDpsm7WRA6P9QqSzhlmZR/xiVapLRLptks3/mhUmKD5cuDGH7o8AbC5rvu4mKZbRk6KX/OIdBiJ8wmN4gDngmoWTj6FZQ4izHlr9RGBiVaiHRrJufsx3mDEnKD8su4mHVo9Eba5o3u5eKm7Rd6Gv/kYdGiG0w/N7NDn8msGTi6LdQfSyMlsxRhBgPaiPRKpuYs6jmgEk6D+Qu+WE3o90br5pnuzWKlrRA6Db/x4fNiFswZN63Dv0meGS56JNQxywPlpdRYhjXapfRHpt0s//m+0mcD88u12FzoxYbiJqYu2yKY7TO6GX/UofNiO0wI96GDu0mvGRz6LRQmCzbli5RExhBatTRgpuzs3HmlEngDyUuimGJo2UbVpqQu0uKYLTw6NL/JodTiP4wtN54DqcmmWTB6B9QsixHlolRgBgQaijRnpuGs7Hm70nvD34uGmHwo7Ybd5pYu9iKMLRS6Nn/34cpiJkwwt6kDkQmhWTY6KRQASwXll9RhRiBapbRLZv7s5/mNUnwD78uemFFo5kbU5p6u9KKh7Rr6Hv/9ofWiHswvN7hDuImG2Sm6FxQkSzmliBRBhjAasjRuZuLs8LmfEkvD44u6mFto0sb4pp8uwyKmrTp6EL/4ofIiMUwbt6UDvUmg2QU6CtQdCznlpdR4Rh6avLR15tKs43miUlkD3cujmFxo2Ub0pqQu2iKULTd6Pz/B4eXiJUw7t4pDo8mcWSw6ItQHCw7ls1R6xi8apDRyZtDsxDmp0m9D2EuamGbo2IbDprOu8OKR7SM6M3/vIdLiK0w8d6ADl8mKWRD6MVQtCz5lnNRwhjVahPR+Jv2s2bmZEnwD14ueWGdo90bYpoNu8KK9bRs6Jf/9IfEiAIwvt5wDpsmiGQ/6BdQzSzLlrlR5xjvak3RFJvys7XmWklDD/EuRmF1o8sby5pQu5mK9bS26BX/vofxiIwwXd4CDmcmwGT86L1QACzmlu5RPhjxarjRQ5s3s/DmfUlmD+gu0mE+oxEb2prcu2SKorTw6Nv/B4f2iHAwu96+DmYmImTB6MdQlSzplu1RcRhHavjRu5t5s0zm+0lQD1Uu72H3o2sbtZrsu8OKBrSg6I//24cCiDYwd96eDqYmpWRs6OBQ5Cw9lsxRqhhPalTR8ZtLs3Xm0knZD2guB2Huo/YbbZqzu8yK67QL6L3/OYeGiJUwad4GDt8mm2Tk6NFQ7ixZljBR+BjpanjRQ5vEs3nmCEn9D94uVWGGo+IbzZpuu/KKy7SK6AD/MId7iN4w5t7iDhEmy2T66EZQ2iyDlllRQRiYak7RXZves87mckl3D+EuymFuo4Ab2prBuw+KkbQm6LD/j4cpiHswzd75Dr8m2mSQ6FxQKyyxlpRRWxh7auPRRZsKs+Tmq0lWD7guqmG5o18bv5rSu5OKLLQS6EH/gYeXiLEwDd71DoImDmTu6PZQcSxNlodRQxh5apPRxZtrs3rm4knTD1guomHUo8gbW5qXu3+Kj7Sb6AP/bIfaiJowut6LDucmRWQ26KpQgCx5lnJRyhhbanbR65v9sybmtkmoD7kuT2GAo5cbyZpcuwmKcLT06Pr/todyiPkw694lDo8mrWRZ6HVQkyxBlklR8BjDanPRdpvqs93mR0nnD+Uu6WFeo6IbapqPu/yKELQN6Jn/24e3iN4w1d5MDgomjGTu6DxQaizulmNRURjlasvReZu9s/HmzUlMD7Mu02HRox0bBZpnu/eKmbSD6Cv/yYfPiE0w2t6PDk0mYWSl6HpQJCz8luFRKBhwau/RsJtss63m7Um3DwourmFYo9IbmZphuzOK27SY6Jv/kIf1iF8wEN64DvImVmRj6O9QIixmlv1Rqxh+aqPRypvAswbmi0mSD5EuXGE+o3Mb9Jriu/2KCLTw6J//LofPiK0wat5oDqkmeWRh6PBQkCwklgdRtxi0alDR5puRs7jmP0mID3QupGGooxAbdpr+u/yK5LSK6Ov/JoctiO8ws95MDkommGR46AtQ6SzhlnhRlxirasnRSJuNs8vm1kk+DwYuRWH6o6wblJoMu5eK1LRG6Pf/vodRiEQw9t5hDmMmwGTs6CRQCCyglstRWxigatrR5Zsas5XmY0mjD4IuZGFzo+kbx5qRu/CKxrRT6E7/ioeciD4wYd7nDkwmXWTt6O1QZyyslvlRzRhXaojR2putswnmP0lnD9guumGaow8b7Zrbuy+K77T26ET/TIexiGMwZN7/DuUmZWR06PxQ0ixylpJR9hj9aivR6Jsks7jm4kkZDyYu52HLo5QbjpqRu3iKKbSI6JP/W4dbiIcwbd5wDt0m2WQn6LpQ/SzVlnZRjRjtaqjRBJtis3nmy0n6D5QuLGH7o8YbBZqNu4OKY7Q36I//MYd6iO4wzN53DlYm3WTT6FlQjSzFlsFROxiuanHRlJv8sxLmDUmWD5kup2GLo5YbX5omu5KKlLQ66Dv/8odOiFowk96iDjomrGTh6PlQaiyVlsZRpBggajPRQJvhs7nmhUkgD8Yu5mFGo4gbrJp8u1CK77Rt6Fz/24fqiFgwUd6zDt0mVWSc6MRQ1iwrluhRORilavTRGZsMs8Pm60mfD8guymFroxMbuJqWu0KKKbT+6GD/dYfTiK8wV96VDq0m9mRX6JNQyCyMlhVRHRhBapzRlJuAswHm2En/DxIu0WG6o1gbM5qHuzmKZLTc6MP/d4dviPMw6t4MDpMm8mTm6F1QpixilqtRvBg8aiLR75ucs5fm3EmYD0MuOGGUo7MbJJpSu92KQbRu6MX/wYd2iOEwyd7cDn8mlGTh6KBQOSxlll1R9hj8aoDRKZvls+PmB0nFD48uQWFFo5kbXZpuu8aKx7RJ6Hj/3ofyiG0wod7DDtsmKmSM6HFQkyyJljZRCBjsaujR55vps/fmaElPD5IutmF7o3Ebm5o/uw2KxrT+6Fj/k4f2iPkwfN7xDpEmp2QF6DtQXCyalrpRohgCauTR45s7s5DmuEk/DzIupmFEozsb4pqYu0SKS7TS6On/XYeDiNYw2N4MDqEmZmSE6LdQJiwXlvdR/BjnavLR5ptnswnmuUmrD0EuWmHco0MbSprOu6qKXLTk6K3/zodciJ8w2t6GDkYmO2R56MZQnyyxlgxR8xjJakrRhpu3s13mWkmBD3EuamH5o8MbcJpNu9GK47QG6J7/zYfCiF4wtN5UDqMmiGRh6HVQ3yzZlqRRlBjgambRN5uUs+3mWUlSD9wuXGFoo/IbyZpiu7eKr7T36HL/6If4iNowVd5gDl8m/2SZ6J1QKiySlsVRJhj2aujRMJtGs5XmYkk+D8sumWFlowsboJrMu1SKn7TZ6OX/LIf3iFswid78DhUmBGTp6PVQlSzPlsdRcRg7aorRt5s+s3jm30lCD1AuwmH1o1Ub4JrUu8aKbrSk6PP/zYcFiCswQd7EDv0mh2QT6MZQ6SwFlu1R8hg7ambRoptPs1jm2UnmD10uD2HKo+MbTJqnu5GK6rQ/6JT/WIehiPowZt41DsgmmmSE6PZQmCxOlh1RihibamXRdJvus2TmVEnrD8EuQWGeo+gb15pwu/CKx7S06CP/IYdkiJ8woN6WDgImwmT06BlQ0yy1lkJRYxjyalvRXJvys4fmYkkGD6Eu2GF/o4cbl5r7uziK9bQn6JH/nYdkiBMw+N76Dpcm/WTO6FNQBSz3lrRRaRh0asPReZsHs8Hm00lfD4Eu82HWo3YbqZqQu62KVLQB6FH/2Ye4iP0wLN7ZDpgmD2TR6LVQeixGlvhRRBhVaoHR0ptDs0bm3kn2D3gumWHXo+cbB5qtu0iKrbSD6B7/FYf5iMgw6t72DucmSGQP6LlQsCxflndR4RhxanfRw5ups0Tm5UnSD70ubGGso+gbgZo4uyGKX7TF6Kb/pocGiMcw2d42Dssm6WRO6FFQgyw5ln9R2hjmanLRU5v5s9rmCkmFD9Mu7mEFo+gbRpqmu+aKaLQm6On/14ehiM8w2948Dkgml2Sc6F1QQyz9lmJRCxjsavXRVJuNs/LmzUlwD/cuxmGWoygbaZpzu/yK+7S+6Af/jofsiEkw+t69DkYmYWSW6HxQSizBltlRUhhJatPR85t0s+fm10neDy4ugWFBo7AbqZoHuzqKmLTb6OD/3ofWiD8wG965DpgmOmRE6O1QbyxEluZR9Bh7aqbRyZvKs2fmoUmXD4YuW2Fmo2cb45q6u7yKBLTA6Nr/NYf2iL8wP95wDpYmQ2Rp6OxQkSxolmdR9Rj2anTR5JvSs97mBEmvDyEupmGLo2gbB5rdu5uKgrTQ6OD/R4cJiO8w4d5XDjIm7WRH6HpQnCyslkRRgBjtarbRUZums8nmiEkpDwIuIGHno6Ib/Zo1u/KK47Qt6I7/7YdRiGswp95oDnkm/GTG6ClQByyhlvBRSRinapLR+pses6LmREmMD/QuHGEwo+sb+Jrhu92KmbRt6FD/9IeNiHkwV97ODmsmbGTO6M9QKCy9lv1RxhhQaq/R8JuOsyTmNElzD/guumGEoykbiprIuweK8rTq6F7/R4eWiFwwQd6fDucme2RL6MpQ9ixxlpdRmRjCahfR6Jt8s7rm/EkSDxEu4WH5o/0b65rjuzmKKrS46IP/PodWiNIwcd4RDskm+WRZ6ORQ1yzrlldR7BjDasnRB5sys2Tm9EmhD6EuMGHIo9QbKJrmu4CKb7R36KP/MIdHiIEw/d5lDncmt2S06HpQgCyTluBRBhjsaljRlpuFsxLmd0nDD8cuuWHao+UbQJoVu/CKirQg6DL/4YcxiHwwx97oDmImgGT16NxQTSyTlsZR1hgvahvRe5vns4Hmikl2D44uw2Eno/wbmppEu0qK67Re6Ev/hYfeiEswWt72DvYmbWSH6JlQuiw/lq9ReBjRaq7RBZt6s+Dmn0mDD/gu8GFAo0Ybh5qIuz+KU7T46GD/aYfEiOkwS97tDvUmyWRt6I5Q4SyLlgtRCRhQasLRu5uVsyjmy0nlDxku9WHoo0MbWprwu12KWLTw6Nr/YYdziOkwrd5YDpsm+mTu6FRQ9ywilqVRrBhpahbR/JvOs6TmhknoD2UuGmH4o5EbTpp3u8mKRbRS6Mn/rocoiIIwyt7WDlsmnWSP6JhQFCxmliFR/xj8aqbRCJvAs53mMEnbD48uQGFLo4kbPJplu4GK57RB6Ef/+4epiEsw5N7MDvkmH2So6CJQ0iyflmdRKxjbasPRp5v9s5Dma0k+D5IuvGE5o2Qb4Jo6u1mK4LSq6Fz/g4eqiOYwVd6xDuImhmRG6D9QXizplrVR5Bgtau3R7ptUs/bmn0lBD3UurmFko0cb/pr6u2mKVbTi6Mr/coeHiPIw+N4VDrEmK2S16KFQYyxwlstR1RjlasnR25tBsyzm9EnrD14uNWHMo0IbCZrAu+KKYrSy6Kv/7odLiKsw4d7aDikmIGRU6ORQ9SyqlnFRkRjYajfR+ZuTs1vmcEmjDz4uZ2H/o+sbVJp0u/KKwrRX6IT/0ofOiD0wjd5kDskmgWQB6AtQlyzploJR1Rj3akbRTZvxs+3mbklND/wuTmFfo5gb0ppYu/+Kr7T+6Gv/kYeRiNUwJd42DlEm+WSm6JtQKCyOluNRRhj2ap/RbptIs/fme0lqD/wu5WFfo24b+preu1SKvLT26MD/I4eliEEwtt67DgQmGWSF6JhQtyyNludRYxhUaonR75t7s0fm2ElAD1gunGHNoyUbgprSu/uKW7T36O3/0YdciGEwcN7/DoMm42QK6NVQyCw9lulRjhhKajvRjZs9szzmxEnSD3cufGHKo7Abaprhu8WKwrQL6IP/KofXiPwwBN4LDpcm7GSx6OZQ8iw5lhlRihi8aiDRV5uds2Lmb0nbD80ud2G8o84bvppmu5eK1rTb6CL/fodKiJkwo967DiQm62SU6ClQ/SyVljlRNhiVanrRbpues9zmPklWD6wu8WFQo+cb05q5uz+KhbQq6NH/iocGiAYw6t6YDr4miWT46FlQTyyjloBRQhg1apzRSptrs/3msEleD5ou6WHgo1cbi5rzu9KKIrQE6EL/9Ie8iIYwAd6KDsYmM2Tp6LtQPCxYlpRRYhh7apzRz5tyswnm2kn2D0cunWHso+EbPpqWuz2KlrS66CP/K4eeiP8wkN7eDpQmYWRL6JdQnCxellFRgxhranTR6ZvQsynmk0npD9cuf2Gio9gbhpo0uwuKJrTM6Jj/gYcBiOcwyN4NDocmkWRY6FdQoSw6lkdR3hjramTRaZvRs9XmZkmiD9cuvmE/o/YbUJqEu/uKJbQW6M3/yYemiPQw1d5sDk4miGSK6HtQYCzylnBRchjyavrRKJuOs+vmtklfD60uxWGGoyAbAJp8u8eKtLTk6DD/8YfJiE8wht6qDj0mamSz6HNQZSz/lt1RSBh7atrR7ZtMs4Tm4knjDzouk2FEo60b/pohuzmK+LT76Lf/zIfEiF4wCt62DromamRa6OJQeyxSluxRyxgpap7R9Zu/s1Lmv0mTD4YuGWFoo30b3Zqku5eKC7TG6Of/VIfqiIswcN5nDqQmT2R86IFQ2ixrllNRrhjSanjRoZvGs+TmCEmSD2sul2GPozcbeprju9uKpbTI6Mn/TYdNiLow795dDngm0mRa6HlQ4iz+lidRvBjNav3RWZups+XmgEkJD2guYWGWo/0blJotu5GK57QL6Pb/t4dQiDUwkt5zDmMm62SD6HRQFSyvlslRRBiyasHR2Zs1s7XmQEmjD/wuB2Ebo8kb+Jq7u/6KzbQ66FD/rofpiFgwWN7CDjEmCGTI6P9QVCyclvFRxBhOav/Rk5vRsynmEEkkD8wu5mGLowobw5qCuxuKiLSR6Fj/V4e3iHEwON77DsUmVWQM6P5Q1ixKluxRlxj6ajrRj5tBs5rmgkljDzAu5WHio7wb75rWu2+KULS46Jf/QYdGiOMwbt4RDtwmw2Ri6LtQ6yzJlglRkxiQaqHRWZsQsyXm5Em/D/kuKWHko58bKprmu66KebRO6KP/WodIiM0wxd5lDngmqmTv6HtQvyzMlupRFxiGalvRh5u+szjmAkngD/suvGGGo+wbfZpUu6SKtLRZ6Hf/xIdGiEYw2t7LDlcmjmTN6PtQXyyTluVRgxhUagfRapvNs6/m5kk6D+UuhmEzo9Ab4Zpcu3WKmrRb6Hr/3Yf+iHIwU978DuImeGTs6JVQ3iw/lvdRdBiHaoDRfpsts+/mgkmUD9cu7GFho0Ybk5q2u0SKeLSf6Dv/DofhiKIwaN66DvAm4WRG6ItQ4yzRli9RaBhBatbRmZvgswvm9EmbD0cuxmHoo0EbdJqVu12KTLST6Ob/aIdSiNIwqN4PDo4m6mT/6FRQkixelqFRuxhiagbRypv0s5Xm1UnLDzwuLmGIo7cbaZpSu/KKbrQK6PL/8odXiJ0w396+Dg4m9GTV6NVQPCwbll1R3xi/aqHRBJvgs/nmM0nnD78uPWFlo7obTZp1u+uKkLRd6Ez/zIf+iHIwsd7jDsAmPWSQ6ENQsSyfljRRIBiYau7Rmpvos5DmX0kaD/cuumFIo1QbzZp+u1iK7LTz6Ff/uYfiiMkwEd6DDtMm1GRU6GRQVizglq9RtBgOavnRnZtGs+/mnklbD04ulmFIo38bk5rxu0WKV7T+6Pb/Z4e8iOUw1d5CDrMmRmSs6KtQAiwKlp5RxBicatLRiptfsxHmtEngD3guRmHLozAbDJrcu+iKdLSV6Pb/2YdbiKswi963DgMmM2Qi6MpQ4iyqliFR9RidaibR/Zu2sz7mREnrD0IuO2Hno+AbXJp3u7eK7bRx6IT/7Yf2iFUwgt5oDqEm/GQh6HdQmiyUlrlR7hjSajzRCpuvs4rmVUlvD/AueGFOo88b4ppju4CK37S56Ff/lYePiLQwRt40DnommmS86JNQISzWluBRFRjVauDRbZsxs/HmPkk8D5su/GF6o1sbrpr3u2yKmrTi6OH/HoetiEYwj96CDmgmAWTN6JVQpSzTluJRcBgNavXRkJths1Tm0klLDw4uyGHao10bhZrVu8uKVbT+6NL/tocoiDQwY97GDqcmqGQa6PVQnCwplodRoRg6anbRo5tzs03mzEneD0EuWmGso6sbaJq/u+qKwLQ16In/fIeGiP4wAt4MDvgm4WSk6OtQyCxBlkdRiBiTakfRRZvxs3PmRkmdD8EuX2GAo+8bxJpZu7SK6bST6F7/PIdDiOUwvt6jDhUmzmT86BZQ+iyYll5RaBj1ajrRXJvOs/rmQElsD9wu+GFao4wbzJr+uyiKtbRG6In/nIcJiC8wzN7MDpUm82T36FpQEiyOluBROBhEavzRWZtas97m+EkiD6UuymG5o1sbi5rPu4GKC7Qr6GD/5If1iPkwOt77DuImJWTK6LFQJixtlrZRXBg8avvR0JtcswfmoEnyD1AugWHio+AbL5qzu0eKj7Sa6H7/JoeBiOgw5d7SDsYmPmQK6JZQiCxellpRhxhmagfR5Ju0s1XmkUnAD94uRGGro88bkJoauzKKIrTk6P7/lIcgiJUw/t4NDtMm7GQ/6FRQpixKlktRmBjramvRFZugs6zmDEmiD5cuy2FUo6MbcJqku/+KC7R66P//1IeziOww9t5MDk8mjWS06H9QUizRllpRcxjzasrRXZvhs9Dmukl6D/EumWG0oyEbaJp8u+KKpLTr6HH/7IfOiCcwkN6zDn4mbGT46HtQSCzIlvBRZRhqauHRw5sMs4bmxEm4DzsukGElo5sbv5oGuwiK6LT16LT/xYfOiFMwN96NDukmS2Ry6MZQRixmlv9Rzhh1avvR1Jvas3zmvUnwD60uA2EroyMb5Zq/u4CKIbTr6OL/FYf9iJYwSN5YDqMmMGRj6PlQ7ix0lk5R/RjJakjRhZvjs8vmJ0m3DyUu22H9o2kbFZrCu+6K4rSM6Jb/Xoc+iK8w795MDjom5GRc6GlQxSzRlmNRthjhaunRe5uSs8bm20keDyAufWHIo4QbgJowu8qKyrQP6Mv/gYdviEwwt95QDjYm3WTD6GlQTSyvlrBRcBj/avfR/ZtUs/LmK0nJD/0uA2EHo8MbwZqfu4qK8LQ76Cr/pIftiGwwcd7+DjkmDGTk6P9QWiy/lv1RpBhvap/RwZunsx3mCEl8D98ul2GTow4b0JrIuxuK1rSY6Hr/SIeKiCYwY97CDpsmd2RE6N5Q7iwVlrZRyhjhahrR8ptEs63mikkGDwwu2WGYo5Ybx5rQu3CKN7SH6LD/dYd2iOQwTt4GDoEm3WR76LNQ2yzLlldRhBjOarDRKZsls13m5km0D50uMmHWo9kbDpr7u7KKX7Rv6Jj/XIc4iNgw0d5dDn8mrGT56A5Q/yzDlr9RPRi/amvR1Jues2DmGkmZD9UukWGOo+cbIpo9u6KKtbRW6EH/ioc0iBwwht7BDksm5GTo6MhQbiyuluZR1xgJaj3RVJv5s57mokkPD5Iu9WERo/kb65pBu06KuLQi6Ff/3ofMiHYwSd6lDqYmBGT26IJQ9CwYlqRRdBiGaprRFpsxs5jmz0mLD8su62FkoxkbkpqMuzqKWLTE6E//Zof5iOswRd7nDsAm+GR46PVQ6CyulitRJBhxasbRqZu/szTmwEmfDysu5WHpo3Ebb5qZu0WKWbSQ6Ir/docciOww/t5fDoQmwWT96AhQqix3lpZR7xgjamHRgpvqs7bm00nrDzouLGGuo5Ubd5pau92KZbQM6Nv/4odWiJww/N7/Dgsmn2Tl6LdQNywylkhRwBi9aoXRKJvrs/vmMEnbD78uZWFao6Mbb5pOu/+K8bRo6Er/qoeuiH0wnN76Du0mAWS36FNQsizwlmxReRiMat/Rppvas9TmJUkCD6Qu+2FJo0gb+5p/u2aK8rTC6GH/koeriMcwUd76DtcmwGRQ6HNQKCzdlo5R/RgKavTRmpsGs9Pmi0leD1IurWFNo0sb4Zqcu12KZrTp6ML/aIewiNMwut5UDqEmdmSs6J5QIywKls1R2hi2avnR8ptPszXmjkntD34uZ2HMozkbVJr5u82KabTs6Mj/xodViIQwmN6FDicmBWRx6PdQryy6lgtR1hjCagzR65uws2rmREmaD1kuRmGSo/IbaJpdu82Ko7RE6Lz/zIfsiD8w9d5BDqwmlWQy6DNQ4iz6lqNRxxjEak7RR5uas7vmaElID8IuU2Fro8Eb1ppfu4yK4LTl6Aj/iIfmiJUwON4wDmgm2WSr6KBQDiyWlv1RDhjYao/RXpt3s7jmaElFD94u4WEiowwb15rVu0SKgrTz6L3/B4eBiEYwu968Dh0mbGTN6OpQniz5lslRUxgxaobRm5t2s3Lm8ElqD38uy2HLo3gbkJr8u8iKZ7Sl6O7/sYckiAQwX970Docm/mRh6MVQjyw5ls9Rihh5albRl5tTs3/m7EnwD1kuSGGlo+wbHJrpu4KKtbQU6Pb/SoepiKQwKd4UDvcm22SY6NFQ+Cxwlj1RqhiSanvRS5uas0bmX0n8D/cuRGG7o84bvZpru/+K8rS66Df/Aod+iO0wgd6iDgom5mTL6BdQ9Sy0llBRaxj5akHRbJvbs+HmeElzD68uq2EMo7Ub45q+uzqKkLRG6Ib/iYcIiC8w2t7GDrQmzGTo6GBQNiz4lrVRYhhbasHRT5sMs8jm7ElbD70u+2H2o1Ebj5rpu4yKN7Rg6Ef/w4f4iIYwEd7IDucmTGT06PVQKyxbluJRTxhVavnRmZtFs2nm+UnrD3gujGHoo/kbUJqwuzCK37Tv6Gv/K4fgiN0w5t7bDusmYWQ86JJQ6CxmlmdR/Bhsal/R4Jvvs2Lm5kngD+YueWH+o+sb0po8uzSKRbSd6Lr/oodziMAwmd4XDv0mjGRQ6EZQ6yw+lnZR7xiZalPRRpuzs7TmaUmYD5Uu/GEIo5IbIJrSu66KPbQT6Mb/2oemiNgw794xDjUmuWT26GdQUizAlkxRXBiZavLRW5vss/Pm6ElVD5Iu72GjozkbAppHu+2KqLTm6DP/5IebiEwwi96cDjkmSmSE6GZQSyzHlstRXxgPatHR/Jtws6Lm1EnYDwgu9WFoo4Ab8JoUu2+KjLTJ6PD/+YfRiFowE973Do4mVWRy6O5QQSx2ltxR0RgiavbR1JutswTmqEnnD4UuAmE0o34b55rtu7GKLbSD6IP/XYeTiPIwa95pDqwmUWRs6MxQ/SwhlmdRqBjvag3R4Zvys8zmBkmkDy8uiWH3o3MbBJqMu86Kt7TQ6Mr/JYddiKMw+95/Djcm+GQ46HRQ7Sz4lnhRkhjSatzRR5vos/fmuEkdDxcuWmHmo/wbnJpxu8WK4bQb6PD/rodviD8wp94rDjImiGSS6CFQDCyrlqtRDxjkapfRv5tBs+XmcEnAD6EuemEgo50b4JqKu+WKx7R96Cr/9YegiEQwMN7LDlsmVWTB6LlQLCz2lqJR9RhtarbRypu1sxzmYkleD+wuq2GIoxIb9prkuymK0bSr6HD/QYexiCYwSN7gDs4mdGRs6MBQ7CwIlphR+Ri5ah7Rvptgs47mo0k4Dy0u42Hso7Qb5Zrwu16KNrSW6Lf/fYd7iOsweN4RDu0m0WR36JtQoSzIlkRRrhjOatfRWZsks3fm3EmhD7wuKmHPo+AbPprZu4OKWLRV6In/X4dhiNgw0d5SDk8mt2Tc6HRQsiyLlt1RNRiwaiTRrpuUs2fmMEnXD98uvmHRo8gbZ5oyu4GKv7RV6FP//4dAiGgwxN7pDmgml2Tp6KBQeSyNlsZRlxhUahbRaJvHs6Pm50l3D9QuyGENo9Mbt5pgu2yKjbRI6Gn/8If5iEYwd97ODvUmTGSt6ONQ/ixYloBRdxi6apzRaZsos9fm7kmwD4guimE9ozMbr5qvu1iKZ7Sb6Ef/fIfHiP8wRd6VDvQmyGRZ6PFQ0iywlhxRBRhKaoXRrpuTs3Lm5UncD0kuiWHsoz8bOpqPu3yKYbT56J7/RYd3iNYw9t5NDoUmxWTF6D9Qtix8lpdRiBgiaiXR2JuUs4bmiEnVD14uDGGYo7sbe5o3u8WKRrQK6MT/8odoiIww0t7eDnkmqGT66K1QCSwDlmtR2hj/aoPRK5vks8PmMUnZD5IuWGFFo68bJppXu8mKh7Qk6Gb/t4f1iHwwrd7JDu0mC2SA6GFQpCyDlilRKBjUat7R4Jvts5fmZ0kdD6Mui2F9o2obmZp8uxGK2LT46D7/o4fAiMQwRN6FDs4mpGQk6ABQRSyFlohRmxg3avbRnpsRs8bmiElmD3ouqmFAo0ob3JrQuyCKDLTW6Pn/c4etiPcwwN5CDq0memST6KRQMywllvpRmRiXavTRlZtlsxPms0nvD3UuNmHgo2cbcJrMu9WKULSX6Ov/+4d5iP8wld6KDhQmZ2R+6OxQpCyoliFRwhjpajHR2Zvss2HmSkm1D3AuJGHBo8AbZpp2u+6KzbR76Oz/y4f9iC8wtd4rDpQmv2Qd6ApQySz5lvlRjBjxalzRHZuzs47mVUk7D/AuemEJo9Mb8popu/+K8rTW6F//kYfIiLUwGN5oDlYm3mT+6L1QIiyVltVRKxiUauvRR5tTs6bmWUlKD+YumGFJo00bs5rMu0SKp7Tu6N//NIeUiHcwuN6gDjgmMGTE6OBQ+SyGlpJRWRgGapLRt5s9s0zm6UlKD2Qu/2H5o1gbuprru7uKV7SU6Mr/p4cpiAEwRN7hDq4mhGQ66OJQyyxGlohRnRhkamDRp5tas1jmmEniD1suU2Gho8kbWpqgu/GK9rRV6Kr/UIfWiKAwBd56Du8m/2SX6PxQ0ixGlhlRsBjvakrRUZvas2HmX0mcD/UuWGGOo/kbuppqu5+K17Sz6AD/IYdjiJ8wud7mDgcmxGTp6CdQhyzrln1Rexi2alDRdpvus8XmS0lWD7QuuWFao/Yb5pq6uz6Kt7Ql6LX/l4chiAUw5t7lDqQm22SS6CtQMyziluNRQxhzauHRb5tas57m+UleD60uq2H1o1obpprIu5OKDLQ66Gf/3oe3iKgwDd7HDpUmKGTe6PZQaixjloFRRRhrat/R7JtSs1zm0UnrD1wujWGVo6AbGZqgu3qKt7S/6CT/FofliOYwtt71DpgmPmQv6KxQlSxHlkhR7Rhiak/Ry5v3s3vmpEnyD/kuTWHoo9Mbqpo+uzCKSLT56Ln/sIcNiM0w/d43DvUmlWRi6HBQpixulktRkhjCamHRUZv9s6zmWEm9D+wu7GEMo4UbZJqGu6eKY7Qp6Mr/4oeYiPQwwN5iDiwmo2Sw6H9QRCzeljxRbRjhaqzRd5u2s9bmoElwD/cu1mG2owsbH5pqu8yKqLSj6Bv/64faiA8w6t68Dj4mS2Sl6EVQbizJlu1RcBh1arPRqJtUs7bm5EnkDxsujGEgo9EbqZo4u3eK6LTf6Pn/6ofXiGgwNd6kDpMmOGRM6NpQeyxqlshR3hh6aqHRzZv/s0vm7EnKD5MuHWEBoycb9JqCu7CKEbTY6O//Gof4iJswb95qDqYmemRB6OFQySxQlg1RsxjtagnRjZvBs7zmCEmUD2IuhGGfoxsbMZrku/+K4bTF6PL/OYcTiLIwg95gDi0m3mQ46A9Q6yzulmhRsRjpav/RaJuKs/bmuEkjDyMuZGH5o5kblZoZu+2K+7QG6Pf/lodliG8wld54DjsmyGTw6F5QbiyslqpRchiVapPRoJsHs6LmfEmKD7guAmF7o9cb4Jq/u8qK2bR/6FD/gof2iH0wV979Dk8mCGT66O9QayyzlsxRvxhGavHRwpvVswPmaEl8D8wuh2HqozkbkZrmuzCK8bS76EP/NIeNiF8wZd75DucmRWR46NRQwixplpBRlRjoakbRkptks5jmtUljD3cu+WH4o+YbyprEuziKKLSD6JL/VodliJwwXt5LDvIm7WRh6KdQ1izplgdRlRjNaqnRCJs1s3rmmkmAD5wuE2GTo/4bD5rxu6mKb7RO6Ij/eYdliNkw295gDlEm+mTD6E9QmiyYludRWhiIaiTRj5u0szPmD0nKD5guiGHUo5Ibf5oQu/GKiLRA6DD/mYdOiEww597eDmkmu2Td6O1QRCyvlsJRkRgraifRc5vCs5rm+Uk3D44unWEeo8sbkZp7u1uKkbRQ6E3/24fjiHowSN7yDskmX2S/6O9QwCxUlvRRdxiWar3RF5t7s9zm/EmBD90uw2FCoxMbipqUu3iKY7TS6Fz/WIefiNQwct6gDuom1mRL6J5Q4iyXll5RHhheauLRmJuesxXmxEnuD0YuiGGdo1MbTJqOu3uKVbTI6OD/OocNiPYw695PDuYmy2Sk6AtQiyxwlo5RvxgbaivRmZvAs4vm/EnKDz4uMmGNo68be5plu4eKSLQG6Nv/+Ic6iKQwyt7eDg8m8mTB6JNQJSw7lnBRxxiLaqDREJvIs8vmEEnRD48uW2Fjo/gbWZoku8uK/7Re6Gv/14ffiAkwjN7xDu0mVGSN6HBQtyy/lgVRDhjDaprRnJvJs9DmaEknD4wuk2FKozsbn5pGu0qKxLT16F7/g4fBiLgwbd6QDvAmrWQE6HpQRyzLlrdRlxh0asPR+5s9s+3mkElrDzsurWFNo2sbmJqdu36KabTp6N//FYfmiJcwwd5bDvQmdmS16JRQAyxolvxRmxiWavnRz5tYsyrmiEmYD20uWGGHo3sbc5rHu6qKVLSn6O//5Id1iIgw7t6vDjsmJGRZ6NtQ/iy6lhlR1xjVag7R6pvts2fmO0muD2EuWmHMo94bVJp1u/mK/7R16I3/i4friF8wjN5TDrEmnWQ16DNQzSzPloJRihjOaj3RJpuQs6nmS0lND58uWGFvo9obwppzu6CK4rTL6Fr/o4eViJIwNN5+DlEml2S56KZQciyOlptRFRiNau3RXpt2s5LmeElID4Uu2WFVo2obwZrRu0uKh7SZ6OX/I4esiCMw2967DikmFmTp6MJQnyz8lpFRPhg5apbRu5tHs0jm4UliD18uyGHvo2cbmprxu+GKa7S16NH/lYcYiGYwSt7GDpsmo2QO6MVQ2ywrluhRoBhhajTRopt6syDmzknBD0MuD2HXo6sbcZqMu5aK3bRa6Lf/cIeDiLowft40Dv4mmGS66M9QxyxxlgZRtRival7RSJvesybmc0nlD6wuQGGlo84bwJpyu5aKyrSI6Dn/FIcriMEwhN65DjYmxWTU6FBQ9SyWlj5RRxjzakbRQ5vOs8zmJUl/D/suqGFTo4cbwZq7uyaKl7Rg6K7/uYc4iCIw5N6CDqsm9GTH6HlQHSyhlrFReRhmasHRe5tXs8/mzUkoD6Iu12HAo2YbqJrWu6iKVLQl6CX/2IfmiLMwFt7WDu8mJmT86LFQRSxllpFRRxh/avrR4ptos0vm6EmoD0kuu2HEo/4bHpqiu3OKu7SM6Dr/GYftiPswtN7oDvEma2Q16JlQvyxqlkZRkRhjalvRk5u0synmgEntD/0uXWHso/obu5oluz6KRrTt6Kz/mYcriPgwz94vDtkmv2R86DZQiixElmBR3BiaamHRW5vSs6PmCEmdD8wu32Epo58bQpq0u7iKBrQn6Nz/24e+iJAw9N57DlQmlWSv6CRQTSz+lj1RFRjwas7RJpuXs8/m40kPD6Uu82GEoz0bPpp4u5eKhLSd6DH/7IfLiDMw6N6JDmImbWSH6GdQXyzClstRXBhSarfRyZsHs73m+Um9D18uiWE9o8kbqJoWuyyK4LT06Ov/zYf7iGIwLN6uDrgmXmRA6JVQNCwSlohR+RgzarDRs5v4s0vmvknND4AuP2E9oyIbmJqnu5SKFLTF6NX/Rof9iIowed4/DrcmK2Qy6NFQkixzlhxR/BiwaknRp5vjs+HmDkmVD34uiWH7oycbEZr2u8+KqrSG6Nn/WocViP0wrt4tDj8mimQz6EpQwizrlmVR7hjravLRHJvls4zmkkkODzkuYmHmo50bu5ovu9GK97QJ6Pn/jodaiHIw995BDmsm0GTu6CBQdizUlqJRWxiNatnR65sKs7DmL0nfD/cuc2Fho/UbzJqRu9aKzbR76B3/4oewiC0wLt7NDkomSmTE6OlQZCz2luFRrRgSaqbR6JuWswjmNElmD90uo2G6oyAbxJrauxOK1rSr6Gz/LofiiEUwLd7PDs0mcGRS6PlQ7CxblrBRghjqak/R65tzs6/moUkjDzcuy2GPo6YbnZqNu3uKL7Sz6LD/Zod2iMkwZ95jDogmuWQ96KFQ+yz/llJRnxjDaoTRLJsUs3bm1Em5D+EudmHzo+IbT5rmu+SKYrQ46OL/K4d0iNswyt4hDkom9WTo6GxQhyyQlu1RBxirakrR1Ju1swfmEUnMD8Eu62GNo/IbZ5pIu42KvbRp6Ej/xYdkiAYw/97cDnMmuGTS6L1QbSyplpNRsBg3amzReZuUs+/mj0kYD4MugWEao+Ybspplu1WKo7Rb6Gb//4fyiCwwVd6iDrkmRWSP6NNQrSwOlrVRKBjAau/Rc5tvs9Pmj0mpD80u0mEroxwb4prju3KKILTT6CX/TYeIiPgwY97uDrom52Qc6LNQ1CyRlh1RfhgoasrR4pu7szHmxEnbDw8uzWG4o3sbdpqju2qKZrTG6Mn/dIdKiOww6N5aDvom02T56ENQpix9loVR4xgnaijR25vDs/3m0EnID3ouEWG0o/QbIpptu8aKf7Rb6Nn/74c8iLEw8N7NDhMmsGTF6JBQDCw4lnRRxhi+aqHRbpuMs8LmFknTD7wuYmFqo+0be5pqu4eKzbR+6Fb/tYf6iFEwmd6CDuEmMGS66D5QnyyXlnNRMxj0av3Rp5vts+nmU0kWD4Yug2Fco3cbyppRu1+K2rTd6En/nIfhiLYwDN6lDuwmhmQY6CZQZiyTlqpR8Bh+aoPRl5s6s+3mjkl9D0cuk2E6o1Ub/JrxuyCKd7Sr6ND/SoeaiOgw6940Drcmf2Ss6KxQJSwXluBRnRicaujRw5tfsznmh0mDD14ubmGbo1sbD5rou82KSLSn6Pf/4Yd3iKsw7t61DjQmZ2Rl6MJQlyy7lihR8hjPag3Rhpu0s13mSUm0D14uXmHio+IbRZpou6uK0bR66LH/lYfJiCkwkN4hDqgmpmQz6DtQ9yzllrZR5hjJalzRHZuzs63mW0lnD/suT2FTo/8b6JpDu5OK17Ti6Af/gof0iJswHd4XDkgmm2Sp6JdQIizNlslROhjlarjRcZtus6LmP0lFD/Iu/2Elo04bzprCu0uKvLSS6Nz/Xoe/iEUwuN6ADgMmKWT56P5QnCz6lsNRQBgXaq7Ri5tls0Dm7UleD0Yu+2Gro0Qbk5rou9iKZ7Sz6O7/jIcjiGIwSN75Dpcmn2QR6PVQ9CwwlvFRnBhoanfRqJtns13mwUnqDyAuaGH1o/MbDZqBu+eKzbQA6I7/foesiIowHd45DtYmwGSI6NZQ5CxiljBRmhihamLRVpvjszzmD0nOD+wudmHko8EbvJp4u72K8rTT6Cv/B4dLiOQwh96HDiYm22T06DtQ2yyzlktRURiNaj3Re5v9s83mQklGD/Uu/2F5o4Yb5prAuwCKjbRX6ID/u4chiAYw9N6YDqom1GTC6FFQGyyHloNRbhhqav7RU5tzs+DmzElGD+AurWHjo1sb8ZrIu9CKPrRi6FT/w4eviKEwEd7MDvomLGTp6JxQXSx4lqdRZBhdasDR0JtUs23m70nJDw8ugWHno+cbOpqKu1mKrLSj6DP/F4fsiOgwhd6PDvgmY2Qm6PdQjyxhlktR/xg4alXR45urs1/mk0nTD7cuD2Gqo/Mbr5pUuxSKSLTE6IP/hocViNkw7N4rDtsmrGRL6ERQmCx6ll5R7hjlambRa5ves9bmD0msD84u7GEBo6AbUZq6u56KG7Qp6Of/54eiiOgw8d5zDjEmh2SP6FtQNyzSlkRRCBjGatPRRpvls//my0lbD58u+mGqow8bO5pIu+aKgLSB6AX/24f7iDgw9d6fDkQma2Sw6CVQLSzwlopRYRh0asjR7Ztas5zm+EnmDxku92Eno5UbhJpiuyGK9LTZ6OT/zIfWiF8wEt6jDvImSWRq6P5QOSxQlu1RwRhaapLR+pvYs0TmvkmSD4AuP2E9oyIbmJq4u7aKFLT56NX/RofHiIowOt5sDvIma2Rh6ONQ2ywzlgNRuhihanDRo5vIs/7mXEmSDyAur2GFo2UbL5qfu+eKiLTd6Ob/UYc2iPAwoN5GDmEmwWRZ6FxQ2yzwlkBRmBiuavXRd5uqs5jmikkLDzcuRmH2o5obmJoku+SK27Qb6I3/m4dMiFIws95FDkwm/mTu6H5QbSzblvhRfRiaas/R95tGs4/mV0mkD7ouMmE7o8cblZqXu+mK87RK6Dz/r4eAiGQwb97uDm0mWWTG6P9QeizmltJRwBh7apHRk5uPsyvmAUlHD+gutWGao3sb7Jr8uxaK+bSN6EL/MoeYiEcwOd7uDusmQGR76MlQ/ix5lr5RkhjZagjRtJt4s5LmmkkJDyou+GGVo+Yb3pruu2iKRrT16Iz/R4dhiMYwaN4VDsEm72RB6IZQ0izOllhRtRjNatrRKJtis0Tm10mdD6Muc2GQo8YbCZrPu76KZ7Qy6PD/ZIdfiPMwz95XDhEmrWTv6FxQkyzBluhRBhi+amTRu5v+sxjmBUnqD+UulmHNo5cbSJozu66KiLRn6GD/kIdjiA0w/d7UDlwmlmTo6OlQciyElvRRmhggahvRUZvMs7nmm0kiD/UuwmEFo/Ibkpptu3aKiLRb6D7/xofyiCkwet79DsgmeGSW6MlQwywdlrRRIxiLaojRJZs6s8PmgkniD/cu62FAo08bl5quu3WKQLSe6E3/a4fjiKswIt6lDqgm+GRx6JZQ8yyOljpRABg5au3Ripvgsw3mkEn/DzYu/GGfo0obUpqnu2GKfbTs6M7/a4daiOwwo95ODp8mxmTB6FlQpSxUlpVRpBgBagnR9Jvgs5Hm8knFD34uHGGqo6gbN5pGu+aKZbRG6OP/7YdyiOUw9d7dDmsm9mTX6LdQXSw/llxR4RicaoTRcJvKs+vmNEn8D4ouUGFXo/IbMZpKu9CK77Rd6Ff/3ofDiHww4t7aDs4mCmT/6ENQtSyJljtRLRj9auTRn5vXs9LmbUlND4gunmFro3UbnJpZu1yK8LTR6EP/gofviOYwc96oDu4mrGQr6BRQVCz8lppRqxhoasXR65sss/XmiklCD2kuhWFPo2QbhZrQu2mKU7Tt6Oz/eYeYiMUwt94sDvUmf2TW6JRQBiwXlt1RnRjiasbRwptwszHmh0nyD1guWmHyo0IbV5ruu/OKULSW6Pn/3YcJiI8w9N6EDlkmOWRk6OlQlSyGliBRlRjkag/R75uSs3HmN0mVD1kuP2HEo+MbIppVu/6KzLRv6O3/4YfuiFkw7N5xDqUmr2Qb6DRQ4izAluJR6RjKam/RD5u5s7fmWUkyD8QuT2FUo5Ib95ohu6eK/LTE6H3/g4ftiKQwFd4kDn0m6mSB6LZQCCzzlvtROxj3apjRUZsxs43mcUltD84un2FGo2sbyJrsu1aKorTp6OH/LIeWiCAwq96XDhsmJ2Ta6MJQhiz7luRRcBgvaovRl5t7s0Xm8kl5D38uzGHpo3wb7Jrdu+OKcLSD6O//lIc0iAAwet78DpgmlmQ66MpQmSwnlvZRnxhiajLR8Zs8s13m20ntD0guWWHPo+MbVJqju8mKwbQJ6PH/foe1iJUwHt4pDu8m4mSb6MhQ+CxOlhVRhhiMamrRcZv6s3jmWknmD9MuVmGgo8IbxJp5u/SK2LS76Az/AYdxiP0wgt6oDgEmxWTv6C5QxyyxlmtRSBiLan/RSpv5s/rma0l3D6Eu6GEPo7kbwZr3uzyKtrQk6Nr//4cbiBIwyd7sDrsm0GSV6ENQMCzvloxRWRg3at3Rfpt9s+fm2Ul8D5EuzWHro2kbvprou6+KCrQV6HP/7YejiOEwFN7FDpkmF2TQ6IxQXSxrlqRRfRg7ap/R5ptXs07m+0neD2QummHIo84bK5qFu1+KqbSa6Cn/EofKiJkwv97yDuYmTmQT6KlQiSxAlmFRwxhgagrR2pvKs3jmrUnRD8gucGGNo8sb0Joau3qKQLTl6Iz/gYcKiPMwnt5NDtcmi2Rr6HFQqSw7lmtR4hjFamvRepv/s/DmSUm3D8Mu12Eqo5IbR5rWu6qKNrQQ6OL/+YexiI0w1t5ADjQmomSx6EZQUSydlmZRVBjnauzRdZvss9zmwElZD6su6mGxoxQbHJpUu9aKh7SW6BP/zof0iBQw1d6QDk8mQGSq6FBQOyzHluJRWxhravTR45sLs47m0UnKDzQuoGEio5obuJppuweK6rTh6L//j4f5iF0wGt7yDo8mPmRy6PlQXixmlsRRqRhLapLR75vOs07mnUnCD7YuPGEio3kb3pqOu4KKM7T76Pn/G4fQiJowWd5IDpomMWRI6PtQkSxDllpRzBjyanDRlpvjs8HmKUmOD2UurGGcoykbMZrsu8WKvLSN6PT/O4c2iJMwvt5JDjEmg2R96ExQ5izelkRR5Bioav/RDpuWs+zmg0k6DxcuRmHio4IblpoJu9KK7LQI6N7/joc+iDAwo95PDmgm3WTH6HVQDCzQltxRfBinavnRxJshs7fmJ0m1D4UuAGEAo+gbhpqmu/GK47RR6DX/kYe4iEwwe97LDk4meWTe6MhQWiytlvdR1xhUaovR+puFswHmYUlhD54uh2G3oywb95qeuxKK+bSJ6Fz/QIe4iDkwP97HDu0mQmRz6NVQsCwOlrVR6BjiainRhpshs4HmjkkIDxIu+mHlo4Qb8Zrku2GKFbSE6Kv/TYdTiNowWN4KDvomz2Rl6OJQzCz4llFR9Rjlas7RT5szszDml0mlD4Euc2H5o58bGprRu5OKRLRv6PD/OYdiiPAwkN5bDmkmy2Tu6GZQ5yzCluFRKxjyakHRl5udswbmIUneD+gu4WGdo5wbK5pFu5eKv7Rm6FP/xYdqiA4w/N6pDnMmrmTO6MdQWiyrlupRpRgIamHRJ5ucs5TmpEl6D8suz2EWo9Abmppdu3iKibRk6Fz/44fQiFQwbd7vDsEmaGSN6M1Q6ywrlqRRWRisaqTRA5srs+TmxEmbD8Uu2WFOoxsbtpqhu0eKZ7SY6Fv/aYfwiO4wKN65Dvom5mRz6L5QwizUliRRBRhCaufR/JvisyfmlEn3DxMu6GG0o2EbRJqju1CKQbTE6Pr/eIdkiPYwyN5XDqcmjGSt6AlQ5Sx+lqBRvBg9anHRnJvOs6vmikmSD2IuTGGso44bbZpIu8SKfLRd6MH/o4dGiLAw/t77Dg4m8mTG6JhQXywIllFR3hiXaoLROZvzs+XmJ0nWD60uOWE3o7AbNJp4u4iKlbQz6AT/rYfJiGkwjd7QDvcmA2Sm6HVQtCyjlgRRKhj/au7RnpvYs+jmZ0kcD6su4GFYo2Ybw5pHu2mKz7Tu6Cj/5YfBiL8waN6uDukmgGRc6DhQRyz3loNRhRgaapDR55s4s+Xmv0ltD2wumWFlo3sbxJrxu2KKd7Tx6NL/b4eaiPcw694HDpEmVWSD6JdQHywulshRmRiAaujR+5tgsy7mg0mVD2UuS2Hgo1wbUpr8u6mKSLS+6PD/14dpiKwwld6lDggmOmRE6MdQhCy7ljRRkBjkak7R/ZuOs1PmbUmODzguVmGBo8QbWZpku/SK87R96I7/04fGiAQwjt5BDrAmlWQ16HNQ4Cz6lrZR0Bjzak/RLZuNs+jmZklTD8EufmF4o+Abqppbu+eK+7S+6Gz/gIf6iLYwI94/DksmyWSh6JFQJyzSluRRFhj2aorRW5tas6TmOUlBD/0uzGF7o2wbqprOuwuKnrTo6Nb/BIeWiFEwrt6mDhgmLWT76NdQhCz3lsVRYhgpaobRg5t4s27m/0ltD2su6GHyo1gbvZrvu8mKdLT26Or/0YcOiGAwQN7pDpcmiGQR6OlQziwYlulRmhhfanHRiZtgs0bm+Un+D1QuZmHKo9kbdpqOu5GK3bQD6J7/e4fXiI4wHd4uDucm+mSF6NNQ8yw7lh1RoxibaljRW5vRs2XmaUnzD/AuX2G9o9Eb5ppJu6uK6rSL6AH/GYd2iNYwm96VDiEm52TZ6CdQ3iyvlkNRQRiaanrRRpvDs+HmRUlWD90uw2Fvo4YbzprauweK/rRF6Lj/t4cfiHEwyN7EDoMm/GTt6GdQNiyXlr1RfRhIav/RaJtEs/jm00lED7guzmG9o2cbrJrIu4uKJLQ46GX/nYeHiLswFd6TDswmFGTT6OhQaiwzloJRXhhVasXR5ptSs2Tm60nSD1cunWGRo+sbAJqeu2KKlrSF6Av/EofgiPowtt7VDvcmTmQe6KlQiCxMlk5RxhhAamnRzpvps1vmg0n5D/UubGGPo/Ubjpo7u2OKS7TF6KX/u4cKiMgw2t5MDvUmsGRf6DBQoSxilmNRmxjLaiDRfJvPs8HmUUmaD+Mu1WEeo44be5qru6qKaLQU6Mr/xofniOgw8N5xDi0mrWSA6GVQRCz/lm1RTRjjavLRWJvls/zm4klnD7cu1GGqozcbB5pYu/aKlLTj6BX/kof0iBYw7t6aDkUmbGSJ6F5QayzYluJRRhhfauHRyZtps4Lmj0nBDyoupWEio7gbpJoYuyqK2bSe6Jv/yYfJiDowJd6NDpMmPWRg6NpQRyx4lvNR9RhXapfRwpv8s3fmrknpD5cuVGEXo0sbgZqHu5qKGbTL6Pv/N4fliK8wZd5vDqImYGRe6OlQ5Cx4lndRzxj/alTRgpv3s+vmD0mJD0Uug2H8oxMbC5r9u52KkLTW6Oj/YIcIiJcwkN4gDkMm7GRc6ERQwiz2llxRoxjBaubRc5uus9fmtUkkD34uU2Hqo5gbq5oru9uK4LRM6Or/sIdEiGswt95nDkYmxmT+6CJQZyz3ls1RehiaatPRxpsEs47mI0moD5ouC2Fyo+Ab6Zqzu4qK8LRk6B3/8YeLiGEwW97BDlgmfWTm6OtQWCy5lvpR6BhSavrRnZvOs0/mNUkwD+guvWGBozMb8Zr1uxuKz7Sf6FH/SoeoiHEwPd7yDuQmdWQV6O9QqSxTlrlR+hjuahXRk5tLs9Hmj0k/DyEu4mHJo/kb+JrYu1qKJ7Ti6Kv/LYc4iJ4wPN54DsMmsGRU6IFQ+iz4lnpRlhjrarfRC5scs3PmyEmHD6guEWHFo9kbJ5qMu62KbrQz6Lj/fYdMiOQww95/Dkwm92Tj6ExQpyzzluNROBiEamvRipvmszrmIUnID8wu4GHTo5gbJpoxu6+KjrRn6Hb/nodZiHswm97YDiUmjGTz6MpQDyyAlspR0hg1aiPRI5vps5Xm4UlyD8Iu6WEuo9Qbjpp4u1uKhLQx6Dr/yIfeiEgwdN7qDs4md2Sk6JVQ7SxdlopRahjQarvRHpsXs5vmzEm3D/gu0GFdo0cb+Zqxu2WKVrTA6Ej/B4eTiNIwOt6gDuAmx2QH6PZQkiyGlhhRExh0au/R/puZswbm1kmVDyoujGGSo28be5qhu0+KQrTB6OT/UIdtiMww6t4HDowm2GT56EZQtSwhlqtRvBg3ajLRx5uCs5fm6knLD1guFmG4o5UbWZpku+WKaLRY6Jr/rYdAiIUw7N7XDhEm62SC6IFQTyw9lmdRzRi3arnRFJvDs8vmC0mPD+wufGFjo/MbU5pdu8GK/bQz6AP/uoehiAswt96QDskmOmSU6GJQqiyjlghRMBjoap/Rmpvzs/fmdEldDw==";
                    var p = @"dhYU5GUDpcRGN4XkE9ZH5HdllJRTM7I2lpZztnYVVqaThiT2NYYUI7ZlZrZTVCeD5sYUxITk9JYTdIUkRXeyZXNGRdRGE3SThZZF1qU1J/SnNDVDR/SnlFTUxHS2dxUDI1QXZpTmpFf0cxbEhxSFhrKEh5WmxvZVlWaXlDdVR4UjpCWlR1VmI/STFvYDtsQng1dGV6c0sgfGlkRk1LTkNXZHVJOl1gU3A3Q1JHc29pQzRRNWpNSTJ0eFRERlA4Zzhkd1BRPWB5P0tLSEUzNjg4Y0Y0d2FRanNTVEwiKEIjTUpOSFNEWTNRSDd0VlRhXW5LQTReSlsvLGgzU1V0cThPKV9MSUtoOV5NZ3B1WUJ4ZGtOYmVof0ReR2FHeGpnW2RjbGl7Y3k4bktlUl5kf2MyYHVSbWA3cFtjO2NientDU3lzQ0pNQWpCSTtGQ1pWUnY7bWRYN3h1VERzYz9qRmJUZm1ETEZXWGdJYDhXMTNGSUdNa0R1M1dUQHM3M2djRW9iYVRKY1d3ZHloY2I1MjZvYmVDTUhOR0lDPkZTRF1JaklHc1ZUZHl9TmEyXkpfInF8Y1NfRHlEQzp/SUlbZkdvSGdxcDlFYHVafmpfSHV6cFhhSXh6ZmAzXGxpe2N5NT5IVVRTNHIzMldlUWdAPUMzcVtlZzpxb0lhc0RKXUlaSktLR0NaXW8pOn1iU2d4ND9DQ2I5WkxCQ2dtQUJmUWpeRGA4XEEzdH5HfWlORTZbKUljNzNnaWVraTFSSmNYYU9raGZiNTZSe2xlQUg4TG9MQT5IWWRXSyJHN3RWVGRnTUhRNFk6WWJ9anNTX0RzSnQ1T0xJS21BV0I3cXA5RUpGb05hZXh3OFxLIUh4amhPaVlcaXVTdmR5QjVSXmRwVnpfRVFiMDk8T2g7ZGJ6dXslfGNEQH1JbkZHa0VDWlRwWTA9Y1N3d09iQzNhOUpEWTVkfURCdlhYZEhgN1cxNHBwX01rSTU7RjhIYzY4N2ZJZHNRVEpyI1IsLUhOQ0NKSThRRFdzVlgxVF5EYT1uS0skXGpTXyV4MTNfJH9ISUlYPE5LZ3lVXWJwdGlOaGVrT0JuSHFEWGdnV3RqbGtrY2k5fkN1WG5rb2JSZHVTPWB3cmtgO2BScztDY3pzQ1pJcWNCTUtKQ1k2VkY6XWJ4O2h0VEdzZV9kdmM0ak1CXEZnVEdGUDhnOWNIWUE9ZkR3Q1tERTM3U2BzRz9nYV1qcmd0RHNYYDI5YjNvZTVGbUJuQ0lITkdDUztGWkRXdWE5RXNdRGEyPk5vIlF6U1J/TGlDUzR/REladklPS2d3YDhlYXVZTmB/SnV6UFhxSnh4ZmlzWmxgO2xpOX5DdVUzOFI0UlR1UzdHbUFjcDtjNzFRZWlqc0NVMHlUSk1LSkNbTWdJOl1vI2p4Ml9Hc2Q5U0xCM2pNQkJnYWFORlA6XERjeF5BPWR+R3ZZSUUzOyNpaWc7Z2FVamk4Yk9jWGFCO2ZWa2U1Qzg+ZXFMSE5PSWE3SFJEV3smVzRkXURhN0k4WWRdalNSf0pzQ1Q0f0p5RU1MR0tncVAyNUF2aU5qRX9HMWxIcUhYayhIeVpsb2VZVml5Q3VUfmI9QlpUdV5iP0kxb2A7bEJ4NXRlenNLIHxpZEZNS05DV2R1STpdYFNwN0NSR3NvaUM0UTVqTUkydHhUREZQOGc4ZHdQUT1geT9LS0hFMzY4OGZGNHdhUWRBU1RMIihCI01KTkhTRFkzUUg3dFZUYV1uS0E0XkpbLyxoM1NVdHE4TylfTElLaDleTWdwdVlCeGRrTmJlaH9EXkdhR3hqZ1tkY2xpe2N5OG5LZVJeZH9jMmB1Um1gN3BbYztjYnp7Q1N5c0NKTUFqQkk7RkNaVlJ2O21kWDd4dVREc2M/akZiVGZtRExGV1hnSWA4VzEzRklHTWtEdTNXVEBzNzNnZENPalFURFNXcDR5aGNiNTI2b2JlQ01ITkdJQz5GU1RbRWpJR3NRNGVyPU5hMl5KXyJxfGNTX0R5REM6f0lJW2ZHb0hncXA5RWB1Wn5qX0h1enBYYUl4emZgM1xsaXtjeTU+SFVUUzRyMzJXZVFnQD1DM3FbZWc6cWNZYXNEQH1JWkpLS0dDWl1vKTp9YlNneDQ/Q0NiOVpMQkNnbUFCZlFqXkRgOFxBM3R+R31pTkU2WylJYzczZ2lla2kxUkpjWGFPa2hmYjU2UztuZUFDaE5vTEE+SFlkV0siRzd0VlRkZ01IUTRZOllifWpzU19Ec0p0NU9MSUttQVdCN3FwOUVKRm9OYWV4dzhcSyFIeGpoT2lZXGl1U3ZkeUI1Ul5kfUp6X0VRYjA5PE9oO2RienV7JXxjREB9SW5GR2tFQ1pUcFkwPWNTd3dPYkMzYTlKRFk1ZH1EQnZYWGRIYDdXMTRwcF9Na0k1O0Y4SGM2ODdlRjRzUVREUiNSLC1ITkNDSkk4UURXc1ZYMVReRGE9bktLJFxqU18leDEzXyR/SElJWDxOS2d5VV1icHRpTmhla09CbkhxRFhnZ1d0amxra2NpOX5DdVhua29iUmR1Uz1gd3JrYDtgUnM7Q2N6c0NaSXFjQk1LSkNZNlZGOl1ieDtodFRHc2VfZHZjNGpNQlxGZ1RHRlA4ZzljSFlBPWZEd0NbREUzN1NgdEc/Z2FfRFFndERzWGNyOWIzb2U1Rm1CbkNJSE5HSXI/RlpEV31hPENzXURhP05ObyJRelNSf0xpQ1M0f0RJWnZJT0tnd2A4ZWF1WU5gf0p1elBYcUp4eGZpc1psYDtsaTl+Q3VVMzhSNFJUdVM3R21BY3A7YzcxUWVpanNDUHF5VEpNS0pDW01nSTpdbyNqeDJfR3NkOVNMQjNqTUJCZ2FhTkZQOlxEY3heQT1kfkd2WUlFMzsjaWlnO2dhVWppOGJPY1hhQjtmVmtlNUM9TmZxTEhOT0lhN0hSRFd7Jlc0ZF1EYTdJOFlkXWpTUn9Kc0NUNH9KeUVNTEdLZ3FQMjVBdmlOakV/RzFsSHFIWGsoSHlabG9lWVZpeUN1VH5iPUJaVHVacj9JMW9gO2xCeDV0ZXpzSyB8aWRGTUtOQ1dkdUk6XWBTcDdDUkdzb2lDNFE1ak1JMnR4VERGUDhnOGR3UFE9YHk/S0tIRTM2ODhlRjR3YVI0UVNUTVIiLCZlT0l0Qll6Syl2W2UyQU1IZnsjMmp2NDhKaDZsZ3NVUTZ5eUg1N2N6Mi1bXC11MipDdU9CWWA5e29CQlg0OGFDOGp2MDJkdjNuYi0yKj9tSWp8bG8hbiUwIDdYLmlvZGN3fkAgJFAxMD4gKz9HVjdZJDFAIHB1bGVnW0JkeWU/JzMzPiAmO0gkWExNQCwpbGVrZ0AjZW9rYCkoY09idW1hPyUwMD4gPiA+I1AmYWJxbyljNT4nNjM8IiJyIzJKdjAyZHYzbmNpPEN3QHVnYUFtMUItMio+ZWVdIiNSLCV3cWtlMkFPYkJ2MzJqdjA+ZHc2amd5cToyIit4aFZ7SWVebENHd0FKTyVCUWF2dnJxdVhVX2hlVHNgeEd3TkVsQURnc2Y1cjF/QzB4MT05SjIhZmN8bCVuYiNBSEdiRFA5e2ViR003ci07WjViLS5jVVwiJWIiLm19UiwiRnEzMWp2NDhKaDZsZ3NdQTBwOUp1OXpFcTdLZDJBSDoyJ1IuTG1PZGtiWE5IQmlyW29AekhCXW1HX0lpSWBZNkZTZj5ERG9mRU5KekVRaEZTNWQ6VVZQelhVXERsaVREeWVrZkhUZjlDSUN5O2NZZV1pUjE3VVN6VkVaSDIzUzpKbUQ1MDlCf2pgWWZFUmJIZVVealpkXE5ESU9HeWZZYTVDV3JUXU9KS2NheGRNSlEwNFFwd19KdGdUXUxgNWYxXWpXeTl+QVpWVlVZM1JeYHtmUHlDSUYxOmFSQTdRWUVGUVI4cDRZWklmZzpDVFByVFZWWTVJVjJQPkBcZURWOkRRalB7YlU5Q1lGMTpkamxlVlRSTmZWT2VeRD5EU2d5O21BPU5qUnJUQV1GVlpcZkA0V35ITEpuQzNoZVtjVTkzUVdbaFlId3lgWWE0U15oZlFgeVpiYDZTM21KXWNlUlpyUlU1Q1ZeQDlHf2pgWWE0UVJMZlVcZGVTZ35OYmpuR0RlelRJUlB+bUU6QTlAN3lgWWJUU1VSVERWRkRVWHlOYi0yLCJGcTMzWnl6T0d3S2UyQUdqMiFiJGJZOm1EU2FnaFxCN1ZXVGZGbEY1Sm1DSGM2Y0JuaUZBUXkzeUNFYDhhZWl1TWJqelRocHlIU2hVUFdEdl1jZDEzTmhgV1NodTlxS2U9aVQ1OFhze0NfS2FWfkliNElfYHM9QTRQNVdWWDZDWEMyUTByVWdfZUlUcDU4MD1EWlV6QUVoeTVOSyVYMHl6ZE5AMlpxe25HRFY+REEyTmxqVkRiWTJXZVNhaGhMSUNYWVRlTyxCfU5jU0hjNmI4W21GQlFpOkgzRFA4YXViS25tSnlUaHlnc11IVVZnRjpJZVQxOX5qRndUaHU5cU9qXW1ENTM4dnFkVltiVm5KUXhLT29jPUp6YjRnVlNmQkhBPkEwclVjTUVBZHE1ODE3aFVlekFVZ1JVTUslU2B4eEhDYDFUUXQ6SE1GPkphPExMaVZEaHk1OVhRUWhubE5laFxEZU8sQTtqY1NHaFZofE5pRkFReTN3ZE1AN2tFYz9qaUp6VHhyQ2deSFZWZ0NGS2FkMTl+YThEVWh0M1FBMH5uRDU4WHN9ZVJrYlBeRn9GTU9geE1DdmMxZ1ZdRk1nMjRhP2JFYjtlSVRxMHg2NVVdRXpHdWQ2SElLJVNwc3RETkAxVFF3dkRPRj1KYTljOmBWRGJZMl9oUlFnaFxKeWhTZGZGbEJDbW5DSGhWY0hLYWZCVjkxNURFYDdhZWE/bWZaelRocHN1UWhWUFdFXktlZDA5bmJ0V15IdTlhSHJcY2Q0Pmh5aXNdS2FQXklxZE9Pb25tSWhAOldVWDZBPEA5UTByVWJ1V0VkcTB4NTdnVWV5R2VhOHdDWyZTYHN/J0VQMVpxd3h3RGY+SnE/Ym1iZkRoaTA5VFNRaGhcSWF4UlRlRmxKd25jY0doVmlqeW5GQVtJOnFETUA3YWVmOUlgWnlUaHk1RlVoVlZ3Rk5KZWQxM05oeEVaWHQ5cUE6S2lUNThYcndXVWtiVn5FOWdBb2B+bUZeYjNXVlg2T2RRMVEweHVmM3NKVHA6SDI1SFZVekFFaXpDTEslXUBwPERPQDFaYXlhNEBWPkRRMlJ+YlZDYlk6eUZUYWhoTEJFZ15EZkZsQ0A7YWNIYzZhPm5lZkJRaTdLZ0ZQOGFlYHN7YWp6Xkh1XUVRaFVWd0ZyXW1EMTluaHJ3VGh0OWFJaHluRDQ+aHI/RF1LYVZuRjtDQF9vaF1JOEE1Z1ZTdkZAWnVRMHhlYDVFRVRwOlgxOUVWVXpHZWJ2V01LJlNgdTxFQWAyWmF2XGdDVj5KcT9kXGFWRGh5NjNzWlFnaExCN0hWVGZAXEl5SWxDR2hGZ3J6aUZCUXk1OUVBYDhhdWMxWmpaelR4eUlYXkhVVndANk1lZDE5bmZAVFVYdTlhQDZLZVQ0OFhxO0VWW2JWfkJ3V01Pb2hdSWp5fUdVXUZGPElwUTByVWpkN0RkcDpIMjtmWVV6R2VmNlVNSyZYMHNUWEFQMlRRclZUQVY+SmE3Vn1qVkNiWTh7aFFRaG5sQXdVVFRmTyxLTU5uQ0djNmlqeW9GQVtJOXNkT0A3YXVpZUtlWnlUaHE5RVZYVlZ3QnZdbUQxM15lPEVVaHQ5YUE4fWNUNThYc3snVVthXy5HeXdEb2BzPU9iYjJnVlN2QDJqc1EwclVpb2hCVHA6WDp5RlRleUFFaWhzT0slXUByNmRPQDFaYXlmRUJmPUpxMTxLalZEYkkydVhVYWhoTEU5d1ZUZkBcRjlebUNIYzZjUF5pRkFReTN5VE5AN2F1anVKb0p5VHh5YDRQWFZWZ0M6S2VUMTNeYnhYVWh1M1FFMHxlVDUzOHlJaFFbYl8uQl1nSU9gfm1HMzM9QDFUQXhwdElGPUphOTM6YVZEYlkyS2ZaUWhoXE5rRVNkZkBcRU1OaVNIYzZgcF1iZkJReTh5VEZQOGFlal9ubUp5VGh6eUVUaFVWd0loeW1EMDNOaHJ0WUh0OWFJMTphVDU4WHJNZlpbYlZuTmtFQ29geE1FQFM5V1VYMi0yLCh/YDMyanYxXmR8Y2NzYHJxV0lVYUFNPToyLCE3MipNRUlYamp5e2ViQVdlP2FDMlp7JDJqZjNoQi00ejVyfCVlciM4enspQmN2Nm5ney1MYHNZRlU6Ykp4eXtrIkFZRjIhRmo8YWVjciwhVURVaUFEaVg/JzIxf0M0Y2Y5WkF1Q1R1eUdGZElJdTsgPTtqMiJ0dWVyLCsgMktmMkFJSGVzMzJqdjN4T2c2amd5ekoyJXIrbm9ubmd8IiZyIzh6cDdhZnYzbmNobyN2QHVhUUpNMUItMio5Z1M+YiIyLChkdnM5WnY1UzB2Nmxnc1N2UXY5SnU5dl9KcntlMkFNSjIlcitub25uZ3wiKUIsSFYwVUFdaENHaEd4VmF+T2RSLTIqOGNPYnVtYFAmREZQJWllZ3oycFoyf2FkfGJgJW9kRXNlbWR+ZkAif2FtajRxajBweWxhY2lkfm9gfyZkYH52ZGR8KHVvJHRgfnZkYHs2aGNPYnltbWVwUCZERlAlaWVnejJwWjJ/YWR8YmAlb2RFc2VtZH5mQCJ/YW1qNHFqMHB5bGFjaWR+b2B/JmRgfnZkZHwodW8kdGB+dmRgezZpbUJzY39mb2AkdGVFZ2BQJkRGUCVpZWd6MnBaMn9hZHxiYCVvZEVzZW1kfmZAIn9hbWo0cWowcHlsYWNpZH5vYH8mZGB+dmRkfCh1byR0YH52ZGB7NmRAUCZJZld1YnVqOj9gVHJyYWVsZEAjb21lfmVgJH9mTWJ0cWo6MHFsYHNpZHFvaW8uZGB+dmRgfCZlZHR4cH8mZGB+dmRnWzJlaWtAJHVybGltJH5pYFAmREo6P2BUcnJhZWxkQCNvbWV+ZWAkf2ZNYnRxajowcWxgc2lkcW9pby5kYH52ZGB8JmVkdHhwfyZkYH52ZGwiI3IjMkpxO2FqZjZoR3xoPGZzWUxlN3ZJenl7ayJCT2UyIUt6N0IpdmJZWTRHeXtlckpXZTZxQzJadU1CYHY2Yzd0c2xsY1lGVT8pSDInYioxbmVtaURlZHItYiwrITJbZTJBQUhoYTMyanYzfmZ0dmpneXhqMiliJWRyLWIsIkZ/YzJqdjlISm1mbGdzU3EwcDlJdTl+RXhzW2QyQUg6MiJyJmVlYnhjfXIiLCJFf2M0Wnl6RXdxa2QyQUN6MicyIkBYdTNgOmNHV1g2NzxAclRCVklFZFlYeWlle2FnR39IbmgxMnQySDshcTJEdGxoeGNoWmdieHpOTyJ0cWdCPylzekVhNUh8SlNHMW8sYnRpVltFNkI9SUdtT0pYTm1ER0JEUDFPYDJ5dztIQkB3VVRqYVQ7JzZgdlZ3YWJ0QVZZbU5nd18gWn5qVEZUdl9OYUUzfmNVV3FyVH9EVjxIRkVyQmduTkhSM0JvaWY1eUdkbkVEaXVfb2NbKXpwfkM2N0FITUQ1SWV/aXNDWTpPZDhPQzRjaHNyNnA4NElXYFpAUnJiOWB3SXlxakM/JGJ/IlhXUW9rKmE+SXl2NkVlYFVAOFQ8a0VMaHp1XGVyUzhUNV9DMXRqZmBcSXsld21gO0lSVH1nTENWQ3E0fkRgM1pxdDlDWHlqRjJReFFpYVVHYWd9by9AMUdaR2kxanE6QnV0a09FcXloT29HelJQWktFMWZFUWB6TyFvSF1JOUJKWUhHTkFlZjliQWp3cDgzc3NsSEtrJ3hhayBzO0ZCcTl/Lkppc0JDdmpjNEM+RlR4U2R4cFltSWNZe0dTcUlBR3xvLEQ3c2BVQVhtTkQ6dGRWaWFldDJxckFvY2VUSWZ6QDFMZTFFWEZUcVRbKUNwX25mSyI2O0Rjd1FqSmRUQFhEN3hQN0tgUkcyODpkbGY2V0JlV08ndlFJeUhFcDVXQ1N0S0VVN3kyMnFMQXVnVVY6dDFpczY3Sm9HPUVTaE1na2ItMiwoZnMzM1p5elV4d1tmMkFJSjInQi9MZXMzPU1sY1Q+QUA0cDczNFtLL0U/JFV2QmJpSWZlZU1pdWZGZFdGUkZ0VzpJUVI7TysleHpWZXdiY39hSG9lNHRVbEYxY1V2QkhpeyxDPUZVdFcyS0xhVD5IaD5pRzI6a0pbRzhEU3EyZG1HaFVlTWl8QkVkV0VYdnFXO0thUDtPKTl5eGZlcUJtQ3RPb2Y0ZFR0SThTVHxiQ1l3d0M9QTV6Yz5LTGJfbkdHdn9nMTRLRXFpdFRTcTJhaUNydWZHWXZ2R1p3SFh2cVc5S2FRNj8mYXQ5RmV3cm9PaUxvZTR0UFxCMnNVdkJEQjp6cz1MZXp6fE1sY1Q+TWgyeDcyOmtJNWk0VFV2QmJpSWplZU1pdWZCamdFWHZ5ZzJLYVIxfyBbJ3U2ZXdibUN4SD9mOTReREp6Y1N2QkNbJDhDPUE1fmMxS0xiX25CN3xoNzI6e09HTyBUVnEyaylZZmVmR0lxckRcR0hSVndqcUAxUjF/I2snNkZmd2JvSDlHb2c+RFV0QDxDVnEyQ0l0OEMwUTVyOnxJfGNZXkpnemlHMjRLSDdPKFRTdlJleV9vJWhHSXY+QW5nR1hmcUpyQVFRNj8jeXIzdmZ3cmZrbEdvZjk0VWhANFNWdkJJeXkzMz9MZXdfJ0VsYlQ+RFd5aUcwNEtNR08oVFR2UmZzaGZ1ZkdZcXpYXydIUkZ6dzlIMVAxby8mPypWZXdibU9tTU9kPkRTdEc/I1Z2QklpeXljPkE1d08iQWxhWU5HdDllVzI0W0g7QzJkVHZCayVWfmVmR1lxfkVcR0VSRnluZ0FRUzFvLysneTZlcVJtT2xLb2Q5NFRwVD8jVHZSQ1I6dmM/QTV4QzxBbGFUPkdwOmdnMDRLQ2VnMFRVdkJpMVxiZWZCOXU+SlxHRVJGeHM2SUFQNj8kcjg2VmR3YmN/YUd/ZjR0WTRJfyNTdlJKcjA6cz9MZXdfJ0I8ZF9uQ0A7YVcyNEtEY0E5dFR2Umk1X2M1aEdZekpFUFdFUlZ4YzpFUVExfyRmN3k2ZHFSbU9tRD9lNHRTeEA6Y1V2QkNSMDMzMFxlcjM8SXxjWV5DQDhodzM0W0N3RDxEVnxibkVbanVmR0l3OklXN0ZSRnJTOUgxUDFvI3Y2OlZmd2Jkb2BUP2Q0dFk0QjxDVXEyQ1l1OnM9TGVzMzxFbGRUPkNAMHFXMTRLTUNBOXRUdlJpNV9idWhHWXpGQmRXSFhmdkcwV2FTO08vQjg6RmVxUm1DdEA/ZTk0XURHOENWdkJDSXl0QzBRNXE6eUV8Y19uRFA5Z2czOntJO08kVFN2Qm8pVHhVZ0dJcXpYUFdFUlZ5ZzNIMVExbyBWPyZWZHdibU9nSU9kOTRZMFQ+Y1Z2UklpeyJjPkxld08nRXxiVD5EV3ptRzA0W0gzTyl0U3ZSbkFcbyVoR0l6TkRUV0ZSVnJfI0QxUDF/I3YyMlZkd2Jmc3JFX2Y5NFk0RjMzU3ZSRElyOnM/RkV6fytBbGRZTkl0MHlXMjRLRGtAPmRTdkJmaVhiZWZCOXY+SlxHSFJWd2MwVUFQMX8kZjYyVmZ3cm1DdkRvZTR0U2xJfENWfGJIeyAyYz5MZXhDPkV8ZFQ+SWd6Z2cyOntNS0g4RFN2QmV5VntFZkdZdzZKXmdHWHZ0Ty9FUVAxfyk7IzhmZXdyaTN3TU9kPkRZNEc2c1N2QkNZdDpzPkZVeEpxSyxjX25EUD9lVzM0W01DTyl0U3ZSbkFcYzVoR1l6SkVQV0VSVnhjOkVRUTF/JGY3OTZkcVJjb2NNT2Q+RF5ERzZzU3ZCQ1l0OWM+QTV4Rz1JfGFZTkd0PGlHMjRLRG9APyRTdlJmY2xiZWZCOXF+RGc3RVJWcVpySVFSMX8gVjg4dmZ3YmRvY0FPZjk0VWRFNFNTdkJDWXl5Yz1BNX5nPUl8YVlOR3Q8aUcyNEtJP0A4VFV2Um8pXGplaEdJekpFZFdIUkZ4an1IMVIxfyBWNTpGZXdiY39nRG9kNHRTfElyc1RxMkd2MTpzMFZVdUp3Q0xhX25KcDF3ZzM6e0k/QThUVHZCZWldZnVmR1lxfkFuZ0dYZnFKckFRUTY/I3IwM3Zld3JrJ2dBT2Q0ZFZkQDxDVnEyQ1l0OEM9QTV4QztBbGNfbkRXfGdnMjp7Q2tBMFRUdlJvI29mdWZHWXJyRFBXRlJWdkM/TUFQMX8pMjAzdmV3cmsnZ0FPZDRkVmRAPENWcTJDWXQ4Qz1BNXhDO0FsY19uRFd8Z2cyOntDa0EwVFR2Um8jb2Z1ZkdZcnJEUFdGUlZ2Qz9NQVAxfyk7J3k2ZnFSY3NyRG9lNHReTElyc1RxMkd2MTpzMFZVdUp9RWxjX25DUDtnZzI6e0N7QThUVHZSaylVdnVmR1lxekVgV0hSVndvI0lRUjF/I2V0OTZmd2JvS2VDf2U0ZFBQVDhDVHxiQ1l6cmM9QTV9bydDXGJfbkRHfG1HMzRLTU9JfERVdkJsQ2pqdWZNaXpGQmRXR1h2dEcyRVFQMX8oOyMyRmR3cmZ/bEtvZDk0U3hKeFNTdlJKYjl5Yz5BNXdXPUNcY1leSHg7Z2cwOntFb0lwVFR2QmBZWGZ1aEdJek5BYFdFUkZ3anxNQVE2PyRiMDN2ZXdyaydnQU9kNGRWZEc8Q1NxMkp5dDpzPkZVd1p3Q0xjX25DQD9oNzI0W01FZzBUVHZSY3VeZzVlTWl5UkRSd0VYdnhqcUFRUjY/KDI4PGZld2JpP2hDf2U0dFk0VzsjU3ZCRFlhM0M/QTV3QzRKbGNfbkdAO2A3MjRLRTtCPyRUfGJgWVJ0VWhNaXY6Q2hXSFhmeWc8RlFRNj8lOyA4NmV3Ym9PZUVfZjR0WlBROmNTcTJOZjczUz1BNX5jOUhcZFQ+R1d2eDcxNFtPQWc4RFV8YmJ5UHF1ZUdZcWJGZmdHUkZzXmlPYVIxfyN7IDRmZXFCay9qSV9nNHRYNEgxY1N2QkdpenRTPkE1dFpsS0xiWU5Hd2N0dzM0S0pHSTFkU3ZSYWlDeEVlR0lydGhXV0ZSRnFTOEpRUDY/JTl3MHZmd2JqX2ZBT2Q5NFNgUDpjU3EySFl0OXM/TGV1XmFCbGNUPkpwPGhnMTRLRXNPJzRWdlJoNVlmdWVHWXlORGtHR1JGdUM9S2FRO08geXE4ZmN3cm1EOUQ/Zzk0VDhCNjNUdlJIWyA3MzBcZXtnP01sYl9uSmd4ZDczNFtLIWczNFR2UmspUnl1ZUdZeVZEaEdIUkZ2WmBWQVM2PyBVeXd2ZXdiZTg0RU9kPkRfZFE3Q1Z2QkFZZD5jP0ZFe2p0QWxiWU5DWDtodzE0S0kzQTp0VHZCYnFZamVmTWlwdkVgV0VSRnlub0lBUzF/JGVxNGZkd2JoM3dGT2U0dFRoWTsjVnZCR01vImM9QTV5bmtHTGJZTklnbG9nMzprSD9Kd0RUdlJlaVV3NWZHWXBWQlJ3RlJWeH8vS2FQO08lZXI5RmV3cmsjeklfZzR0WDRIOmNTdkJHZjI8Qz5GVXRabEtMY1lOQ0djeUczNFtJMWg8RFV2QmZpSWxFZ0dJf0ZFZFdFWHZxTmlHYVExfyV1d3hmY3diaTg3RU9lNGReSEM1c1R2UkltYDRTPkE1enc4TWxkX25OZ3Z5NzE0W0UxaXV0VnZCY3lGcFVnTWl5XkdQV0hYdnhvLEVRUzY/KTlyNHZmd2JqV2JAP2c0ZFRgUTczVHZSQV1gOEM/TGV+bmBTXGFfbkpnemB3MzRbQ3VmN1RUdkJnPUlldWhHSXsmQl1nSFJWc1c4T2FQMX8leyYwdmZxUm1INEQ/Zz5EVGhZfyNTfGJBWXd2cz5BNXhHPURMZFQ+SmdzdkcyNFtDe0ssRFZ2UmJ5Wm9FZUdZdWRhVjdGUkZ6emBXcVM7TyslcT5GZnFCZHtjTU9nOTRWfEAyc1V2QkFNanZzPkZVfWM7QjxkWU5DV3N9RzI0S0N7Qjl0VHZCaTFabEVnTWl2MkpbR0VSRnZKeEJBUjY/I2l5fGZkd3JvZ25GT2U0dF8kUTNDU3ZSSF1pOyM+TGV6Yz5ITGRfbkd4PmlHMjp7RT9DOERTdlJuTUpqdWZNaXU+RmY3R1JGeXM/QVFRO08pNjI2RmVxQm9Pa01PZjk0VnhbLmNTdkJOZjY6cz1GRXp3MFdMY1Q+R0dsYDczOmtDc0E8RFVxMmJzaGZlZU1pek5BYFdIUlZ5bmZBUVE7TyV2OydmZXdyZndnQD9nNHRUdFgxY1N2QkpiMjRDP0ZVdF8sRXxkX25HQDpgNzI6e0k1YjBUVHZCZnlReEVmR1l1OkJuZ0hSVnljP0QxUDtPLyY2MHZjcUJjZ2VHb2Q+RFN4UzpjVnZSSFsiN0MwXGV0XmtObGFUPkhHa2dnMzRLRDNKdERVdkJoPU1mNWZHWXF6SlpnRlh2eGc6QVFRMX8qVjE7ZmZ3cm8nYUNvZzRkU3BZfyNVdlJBSXspczBcZX1nOUNMYVlOQzg+aDcxNEtLK08sRFR8YmZpWGplZkdZcmRkVmdHUkZxTylHYVAxbyZ5fyJWY3dyZmgyS29kNHRZNEg/I1N2QkNdanMzMFxle2c9Q0xkWU5BVDlhVzE0S0pTSDRUVHZSaDNnYnVlR0l/ZGVld0ZSRnxuaE5BUDY/KlVzN2Zld3JkNDBRX2U0ZFV8QDhDU3xiQl1odnM+RkVzOn9JfGFfbkM4PGlHMTRLRWNLLmRVdkJmY2xtZWVHWXJ0aFxHRlJWc08mR2FTNj8vJjQ0dmNxQm1LZ0lPZzk0VXxAO0NTdkJHZjI+YzBWRXl3PUI8YllOSHd8ZTcyNFtJP0lyZFR2UmR1X2p1aEI5e05BVzdGWGZ2VzJFUVMxfy8pfypWZXdyal9pRU9lNGRZPEA+Y1R8YkFGMjRDPUZVcTM5RXxjX25BV3drZzI6a0U1aTxEVXxiYmlJbWVlR1l2ellTV0ZSRnVaYkFRUDtPKTV3dkZjd3Jvb2hIP2U0ZFlcQTczVHEySGY4PmM+TGV2WnhPTGJZXkRXbmFXMzp7T0NGOWRUdkJjc2tsRWZNaXJqQ2RHR1JGcUM9RVFTMW8vKXI5NmN3YmNrbEh/ZjR0VnRJfmNUfGJDMjE1cz9BNXIzO0NMZF9uQ0gxeVcyOmtFa0U8RFV2Qm1BWGM1ZUdJfEpGbWdHUlZ8bm5AcVE2PylVcTVGY3dyalgxQk9kOTRfZFQ7I1N2Qk5iMDpzP0ZFd1p+S0xhVD5DN2xgNzM6a0NzQTc0VXxiYnNoYzVlTWl6TkFgV0hSRnlnN0QxUTF/JWY5N2Zld3Jmd2dAP2c0dFpYWHtDVXZCQ0IyNEM+RlV3TyxGPGJfbkFQOmlHMjp7STViNjRUdlJmeUJwVWVNaXpCQ25nR1JWeXc2SlFRNj8vSyA2RmRxUmN3bEg/ZDRkVGxPLmNWdlJIWyI3QzBcZXReaU5sYVQ+SEdnZGczNFtEM0h0VFZ8YmtJW2M1Z0dJf2pBYFdGWHZyRzdNQVMxfyU5fy5GZndyZWN0S29lOTRfLEI0U1N2QkhGODlzPkxld05sQ0xhVD5IcDZ9RzM0S05BbyNUVXZSY2laanVnTWl/alZWZ0VSRnl/L01BUDF/JHF4cDZld3JqSDpJP2Q0dFlIWndTU3ZSTWlnOEM+RkV8Yz5JbGNZTkp0OmtnMjRLQHdKcWRTdlJkYVhgVWhNaX9GSl9HRlJGd2MwUkFQNj8kayl2RmZ3cmBfaktvZjRkVGhfKmNTcTJKeXAwUzBRNXVHOUVsYllORFA6Z3czOmtPS0A7JFN2UmBZQnJlZUdJdmpEbmdGWHZ2Qz5NQVAxfyU1eyxmY3FCZTQySH9lNHRZVFE3Q1R2QkhpdzRTP0E1dV5hQmxjVD5KcDxjZzM6e09HR3c0VnZSaDVZZnVlR1l5TkRiN0ZSVnhzPUFRUTtPIHsmOGZmd3JuQ3VFT2c0ZFRoWH8jU3xiQVY7JnM9QTV+amNETGRUPkRXc3ZHMjRbQ3VqdmRUdlJvKV1sRWZHSXxCRm8nSFh2dlpgWlFRNj8gcXh3dmZ3YmNzckN/ZTk0VWRJOmNVcTJEUj8iYz5BNXM3NUlsY19uTWg7aVcxOmtPT0Q4VFV2UmtNSmplZ0dJcmpRaEdGUlZ2Qz5BUVA2PyNmNDRmZXdyZndrT29mPkRUdEc0U1V8YkhNaXRTP0xldl8qRWxiWU5IZ3Z3dzM6a08vSXc0U3ZSZX1NYWVlR0lydkZVZ0VSRnNTNk1BUDFvJTI3O2Zld3JuSDFMb2Q0dFg0RjBTVHxiR3l5MnM+TGVzPmpDTGJUPkJQNnJXMzRbTUdHcWRTdlJkYVN/RWVHWXVqUWVnRlJGdVpiRUFQO08rJjY2VmN3YmV3Y0dvZzk0X2hAO0NWdkJJfWE8QzBRNXA+a0VsZF9uSndoYVcxOmtLL0d8RFN8Ym5FXWJlZ01peVZDVEdGUlZzVzVIYVE2PypFenE2ZXdiZTthRG9lOTRYOEI0Q1V2QkhSOnczPUZFcTpwV0xjWV5JZ2llNzE0W0ZvR3dEU3ZSbkNsYmVmR0l6VkJad0VSRnNHNUthUjtPJWlwOVZjd3Jla2RIP2Y0ZFZwUjVzVnZSSF1mPWM9QTV5bmpGbGNUPk1nZnZXMDRLSVtBMzRVfGJnOV1mdWdNaX9qVVxHRVh2eWpyQkFRNj8gUXQ2RmV3cm5Pa0FfZTRkVXxAOENTfGJCXWh2cz5GRXM6f0l8Y19uSWg8ZUcxNEtFY0smdFV8YmZjbWxFZUdJcnRkXEdGUlZzSnhJQVAxbyZlczR2ZnFCY3tnRk9mNHRUOFk7I1N2QkM7KDNTPUE1em5vSFxkVD5CO2FyRzM0W0U1YzdEVnZSZjVUenVlR1l1alZnN0hSRnJTNUJRUTY/I3Y4OVZld3Jjd2xIP2c5NF5EQTczVHZSQVIzMmM9TGV1VzlBfGFfbkdYPmZXMDRLSkVkMjRTdlJhaUN2dWZNaXJqQ2dXRlJGcVM4SlFTNj8lMjc5NmN3Ymg/YUg/ZTR0VGhDPyNUdkJJdjd+Yz5GVXdXOUtMYVlOR1dicTczNFtEN0M3VFN2QmVpXGRFZ0dJeURjbEdFUkZ3bytDYVE7TyNheHh2ZXFCZDQwVD9lNGRTaFI2M1N2UkliOTdTPUE1dEcyTWxiX25KcDdjdzI0S0R3SnRUVHZSY2NnZXVnR1l/alZfR0VSRnFPJUlRUDF/JWY5fGZkd3Jla2JEP2c0dFg8SDhTU3ZSSnIyPyM9RkV0VzBdbGJZXkRbZHA3MTprSk9Ic1RUdlJicVZ0VWVHSXU2Q1p3SFJWd2M7S2FTNj8lYXQ8ZmZ3cm8vaUxvZTR0UFxCMmNVdkJOaWE0Uz1GVXlqfEFsYV9uQkg4YUczOmtJV0p2dFN8YmYxU3J1ZkI5f2pUYWdGUlZxOmRIYVExfyQ7Knk2ZHFCaD9nQU9kNGRWbElyM1N2QkdLID8jPkZFcDc2Q0xjWV5CW21kZzM0S0ZxayFkU3ZSZj1HaEVlR0l5TkRUV0hSRndqcUAxUDF/JHF4e2Zld2JkNDBRX2Q5NFg4SyYzU3ZSSWI5N1M/QTV2VzJNbGRfbkNQN2N3MjRLRHdKcnRWfGJrI21kRWdHSX9qRl9HSFJGeGc4RHFRMX8gcjh0dmZ3YmpHYkA/ZjRkXyhZOyNWdkJHTWM2Yz1BNXlua0V8Yl9uSWd9bUczNEtGf0Q3NFV2Um9DbGZ1ZU1pcX5IX0dGUkZ1Vz1KQVMxfypWMjRmY3FSZX9iR29nNHRZNFc7I1V2QkdpYT8jPUZFen5gU1xiWU5BVD5gNzE0S0VjSnREVXZCaDNuYzVmQjlxaklaZ0dYdnFHOkpBUjF/IHF4f2Zmd3JtS2VDf2U0ZF5EUzhDVnxiTW1iP0M9TGV0RzdGfGNfbkRXfWFHMDprSDFndnRTfGJuQVxldWVHWXsqU2lnRlJWc0puSUFTNj8oNjspNmZ3Ym5Pa0ZfZjk0WTRXOyNUdkJEWWE0Qz1GVXZfLUhcZFQ+SXd3aUczOntAc0MzNFV2UmJ5U3NFZkdZdDZHWFdHWGZ5fm9KUVA2Py9CNjZGY3FSbUdsSD9lNGRbLE8nU1N2UkJJeXZzMFxle2M5RWxjWV5EQDhtRzE6a09nQzRUVXxibUNvanVmQjlycklad0ZSVnE6dEVBUTtPIFF4OkZmd3JvJ2pIP2Q0dF8oSHJjU3xiRF1vLyM9RkV+anRBbGJZXkFIOmN3MjprTktJcWRTdlJvKVxsRWVHSXFuRGhHRVJGcVc0Q3FRO08vQjdwdmN3cmNnbk9vZTR0WDRUOmNUcTJBUjp3QzBcZXRebE5sYVQ+SEdsaUczNFtEN0pyZFRxMmR9TWJ1ZkdZcmJKV1dFUkZzQzhMYVI2Pyg1djJWZHFCb2thSD9kOTRQVFQyc1V8YkRLKTZjP0E1dFc+QmxiVD5HZ2V1NzI0W0B7RjM0VXZCZmFTdnVoTWl2MkNkR0ZYZnh6f0NhUDF/KyY3dTZld2JoM3FEP2U0ZFN0SXJzU3ZCQVY5NFMwUTV1Tm5FbGRfbk1nZXdnMjprSyNJcmRUdlJnPUpgVWhNaX9mSlRXRVJWfGM9RDFRO08gVjE5NmNxQm9LYk9vZTR0VmRHN1NTdlJIeyl8Qz5BNXMzMUp8ZF9uSXd+YVcyOntEZ0lydFR8Ymc5XG8lZkI5cmpRbEdFUkZ4anJJQVA7Ty8leXk2Y3dib0g8SU9lNGRdSFE0U1N8YkRWN3ZjP0ZVdF5qSmxhX25HVD5mVzA0S0pFaHI0VHZSbE1JZXVoR0l7IkRgV0VSRnZfL0lRUDY/JnV3fkZjd3JpO2dBT2Q5NFZsSnNTU3ZSSH1rKFMwVkV5dz9DTGJfbkdrYHU3MTRbRGVhMFRVdlJnMVtmNWdHWXpeR1snR1JGeXp4R3FTMX8kYjE6RmVxQmNzdklfZTk0U2RFMnNTdkJKeXAwUz1BNX1qf0dMYllOSGA6Y3czOmtPS0A0RFV2Umc9S28lZU1pdmpEbmdGWHZ2RzBQMVA7TyU2OHB2ZXFCbkdnSD9kPkRUPEQwU1N8YkhpeHRDP0ZFdV5iR1xjVD5BUDFxRzM0W0ZxbyJkU3ZSZnNgczVlR1l8RkJYV0VYZndvIFlRUDY/JTsjOlZkcVJjaDZHb2Q+RFg0RD8jVXxiSE1iOEM/QTV1VzFCPGNZTkh3a2U3MzRbTkFnMFRTcTJkfUdjVWhHWXZ6V1NXRlJWdl5tSHFQMX8qSyl5RmV3YmZ3Y0tvZT5EWUhAOmNUdlJCXWA0Uz9GRX5uYUJsYlQ+REg5YDczNFtGe0p4RFZ8YmtJWGM1ZUdJekpFXydIUkZxQzlJQVA7Ty9GNDFWZndyaTdjST9lNHRQWEk2Y1R2QkJbKypzPkxlempwVXxjWU5JcDhhVzA0W0NvSXRUVXEyY3lTeXVlR1l/JkpbJ0hSRnNKe0thUjY/KUI4dkZjcVJuR2BeT2U5NF1IQjVzU3ZSSFI5PmM/RlVwPm1HXGJZTkdUPGJXMDRbQ29FMmRTdlJpPUxgVWZHSXpKQmZ3RVh2emp2S2FRMX8lNjl8ZmVxUmpHZU9vZjR0VmRRPyNTcTJObWk7Iz1MZXpzOUhMZF9uR3g+aUczOntFP0p8RFR2UmR9Smc1Z01pcmpWX0dIUkZ3dzpPYVE2PyBWN3pWZHFCb29jSV9lNGRZOEIzU1R2UkFNaX5jPkZVem8sRFxjVD5DQDNwNzA0W0VjSXBUVXZSaTVebmVlR1l7TkJqd0VSRnhuak5BUTY/LUl1PGZkcVJlaDFAf2Q0dFZgWypzVXEyQ1Y/JEM9RkV2VzJGfGRUPkFAOmg3MDRLSy9IcFRUcTJrKV5jNWdNaX9CQmhXSFh2c0M2RVFQMW8kcjMwdmV3YmpfbkA/ZTRkVWRUPyNTdlJHZjk4Uz5BNXVTMFlsZFlOQ1tgc2czOntJP0d8RFVxMmk5UnhVaE1pdjJFU1dGUkZyWnlDcVIxfysmOXtmY3dyaTdvT29lNGRWYFA+Y1VxMkNGOThDMFxlcj5gUWxjWV5CV2Z2VzA0S0pFaHI0VHZSbE1JZXVoR0l7JkRmZ0ZSRnZfL0VRUDY/JnV3fkZjd3JpO2dMb2Q0dFZsSnNTU3ZSSHl/KFM/RkV+anlDTGRfbkRbYHU3MTRbSyVhMmRUdlJiYVtmNWdHWXpeR1RXR1hmeXM6QVFTNj8kYjA6RmVxQmN7bElfZTR0U3hHcnNUdkJDQjlycz5GVX5ua01sY1lOSHgxeVcyOntFM0Q3NFNxMmR5UHdFZUdZeU5KVzdHWHZzXy5BUVMxbyg2MDZGY3dyb29oSD9kNGRWeE8ic1RxMkRWOD5jPkxldlp4RWxkWV5Hd21lVzM0W09DRjlkVHZCY3FZbEVmTWlyZGRUR0ZSRnJTPUVRUTFvLyY0MkZkd3Jja2xFP2Y0dFZ0SXhTVnEyQUIxMXM/QTVyNzVLTGRfbkh3c31HMjp7SyNGNFRWcTJvSV9qdWVCOXFqVlNHR1JGfGc5QHFRNj8pVXh1RmR3cmlYNUg/ZTR0WkBQMnNUdlJCWXdyYz5GRXhfIFRMY1Q+SHQ5ZTcxNFtDe0A0VFV8Ym8jYHxFZkdJcnZFaEdIWHZxTmpJUVE2PyV1d3pGY3FCaT9qS29nPkRbJEQ1c1R2UklmPyRTPkE1cjM7SyxkX25JZDxkNzA6a0NlYTVkVHZSY31AfEVmTWl7SkZdZ0ZSVnRfIFVBUjF/KyVwOkZld3JuR2lBP2Q0dFBQWHhDVHxiSFY4cXM9QTVwPmtHXGNZXkNXfmA3MjprSktGNERUcTJgXU9qZWZCOXtOR1FnRlJGd25qRkFTNj8lNjUyRmVxUmQ7YkU/ZzR0WURXNjNTdlJKeWE0Qz5GVXM/LUNcY19uQld6ZUczOntAc0MwVFVxMmJ5Unl1ZUdZf2pTaFdGWGZxTyxJMVAxfyZpeTB2ZnFSaTg7R29lNGRTcFkxY1R2QkhtanhTP0E1fWM0R0xkVD5IYDV9RzI6e08jRjRUVnZCaD1KayVmR1l5QkZWZ0hYZndnNk1BUTtPJnl3N2Zld3Jmb2dBX2Y5NFBYQj5jVHEyQ0l3dmM/RkV5fy9DTGJZTkh3dXlHMjp7TkdAMzRTdkJnOVV6ZWVNaXpEZFRXRVJGc1c4QVFQNj8leyE2RmV3cm5DcUFfZj5EVGhKemNVcTJCSyl/Iz9GRXI/IkxMZFQ+Q1A6Z3cyNEtAe0h2dFV2QmZlV2Z1ZU1peUJGbEdIWHZ3fy5PYVE2PyRhdDZWZnFSb0duRT9mOTRUOFI1Y1R2UkM9YTdDMFxlc0M/Q0xjVD5KdDxpRzI0W0BVanxEVHxiaTVdbEVmR0l5RkRrJ0ZSRnROaUlRUTFvJTIxPkZjd2JvJD1GX2Y0dFBYQzZzVXZSQjY6dWM/QTV8anRHXGRZXklnd2NnMTRbSTdBOFRTfGJvQVNxdWdHWXlWQmJ3Rlh2em5sR2FTO08mcjl8ZmN3YmpXYUtvZzk0VnxFMzNUcTJCSX8kUz1MZXhaY0VsZF9uSHg5aDcyNFtGf0E6dFV8YmVlXGZlZkdJdTJGYWdGUkZyVz1PYVI7TylJcjpWZXFCbkN4SD9kNHRdREQ6c1R2UkJbLyJjPkZVeEpzRXxkWV5HWDF5RzE0S0ZlYzJkVnZSaylSfmVlQjlxdkZqd0VSRnl3MFFBUTFvIFsoeGZmd2JgU3pJT2c0ZFpYVjpjVXxiTmlhN0M9QTV0WmZEXGRUPkNba2FXMzp7T0dBN1RTdlJgU2xqdWVCOXBWQmp3SFJGfG5pR2FQMX8rKyU+RmVxUmN3Zk5PZTk0WTRKfmNTcTJIQjsqYz5MZXdXPEdcYVleSndqbUczNFtPRWp4VFV8YmZpQnplZUI5cWpDY0dFUkZxXm9DYVM2Py8mNDR2ZHdiY3thRV9mNGRUeFlyc1N2Ukl9byJjPkZFcjMxR1xkX25HZD1pVzE0S09FZDhUU3ZSZn1NaWVnR1lyYkVid0VYdnE3PExhUTY/L0V3eHZjd2JuQ3NHf2Q+RFQ0STJzVHEySnY2MmM/RlV3SnFAXGRUPkRUNnpXMjRbRX9COmRTfGJhZVlldWdHWXZkYVdXRlJGeGpgXUFRMW8lcjI5RmV3cmRzd0tvZT5EWVxHdmNVdlJIXWl2cz5MZXVDNENMZFleSng3aGczNEtNQ0YyZFN2QmZ9QHM1Zk1pdnJHU0dGUkZ0XmtBUVM2PyNrJTh2ZXFCayN4QV9kPkRVaEM3M1R2QklyOTJjPkE1dE5rQ0xhX25IZDZzdzM6a0B1YjhUVHEyZHVZZFVnR0l0NkZoR0hSVnl+bkRhUDY/I2sjNTZlcUJuR2hHb2Y5NF5ESHNTU3ZSSHYwOmM9RkV8by1BfGNfbkJAM3g3MDRbSDNIdzRWdlJrRVljRWdHWX9uRVBXRVh2clM9RUFTNj8kYjp6RmZ3cmRrZ0FfZD5EXyxHMnNVdkJCTWE4Qz5BNXdac0ssZFlORFdmcDcwNFtPQ0I0VFN8Ym5FW2hFZ0dJeV5FbmdFUlZ2XylFUVM7TyBxeHA2ZXdyakg6RH9lNHRZRFE3Q1N2QkFSMjdDPUE1cTM9QWxkWV5CN3xgNzI0W0BVYTREVHxiaD1AcnVnQjl2NkFqd0ZSRndqf0VBUjF/KUV2MHZmcUJqQ3NAf2Q0dFZ4V3ZjVXEySWshPyM+RkV4Qz5NbGNUPkd4O2FXMTRLRm9GNmRUdlJgWV5jNWZHSXJyRlRHRlJGdkc0SUFSMW8rIjh5VmN3cmVrZ0N/Zjk0WThTNzNUfGJJZj8jMz5GRXp3PkVsYVQ+Smd+ZTcyNFtDY0p4RFN2QmRlWmRFZ0dJeU5HUFdHWGZxVzpLYVM2PykyOHZGY3FCb0Q9S29nNGRUOEI4U1V2QkpiOHhDPUE1eF8tT0xjWV5CV3lhRzE0S0ZlaXZ0VHxiZXVfaFVlTWlyalZTN0dSVnlqfU1BUztPJGIzOlZjcUJrK2FNT2c5NFRwWX8jVXZCSF1odjM+QTV1XmRITGRfbk1nbmR3MjRLQHtEN1RTdkJuRVhqdWdCOXFuRFRHSFJGen5vQ2FTO08kZXYyRmN3cmNnaEh/ZTR0VWRANnNWfGJCOXp+Yz9GRXxjPkJ8ZFQ+QldqYVczNEtAc0QwVFR2UmNjZ2p1ZUdJcHZEZmdGUlZ6ZzJJMVAxfyg1eH5GZHdyZXg0R29lOTRYOFI2M1V2UkdtbyJjP0ZFe2c0S0xhVD5CVDxlRzA0S0VzRT8kVXxiaDFZYFVnR0l/IkZhZ0ZSRnhnO0VBUjF/KUshNkZld3JmeDtEP2U+RFlcSXpjVHZCQkI4d1M/QTV0Uz1CPGNfbkdUPmg3MDRbQ3NFNFRWdkJjZVB8RWdNaXlaSlhHRVh2enc0QkFQNj8kZXsmRmVxUmQ4P0U/ZzR0WURXNjNVdlJDSWE0Qz1GVXZfLU1sYV9uSnd3aDczOntAc0MzNFV2UmJzZ2hFZkdJdDpTaFdGWGZxRzxJMVAxfyg5dTB2ZnFSZGg7R29kNGRUMFk6Y1V8YkNNaHRDP0E1fWM0R0xkVD5IYDJyVzI0W08vSXRUVHZCb0NhenVoR1l5SlJoV0hSVnl/JURxUTF/JmsjMlZmcVJja2hIf2c0dFg0SDpjU3ZCR2soenM+TGV0Uz5LTGRZTkI4O2A3MTRbT2dIeFRTdkJvLU1vJWVHSXBaQmc3SFhmcUp3SDFRMX8gWXY4dmZ3YmpDdUg/Zzk0X2xJcnNVdkJEWXh4Qz1GVXZKeUF8YllOR1d7aVczNFtORWh5ZFR2QmshU3I1ZkdZeVpWWFdGWGZ2Ty5FUVExfyR5czVWY3dyaD9vQ39kNHRQVEd4U1V2Qkl9YzxDMFxlcj5pRmxhVD5DODB2VzM0S08laHI0VHZSbE1JZXVoR0l7IkRnN0hSVnFfL0lRUDY/JnV3eTZjd2JpO2xHf2Q0dFZsSnNTU3ZSSHl/KFM9RkV0Sn5DTGJfbkdrYHU3MTRbSyVhMFRUdlJjcVtrJWdHWXpeR1c3RlJWclM/QDFTMX8kYjE6RmVxQmN4MUlfZTR0XUxEMnNTdkJJcjlycz5GVXp/LEdcY1lOSGtkfUcyOntEb0UwVFR2QmV9R28lZ0I5e0paVFdGWHZ5anNJQVA7TykxdD1GZXdyaDN6QH9lNHRbKFAzM1V2UkJJeHpjPkxlcz8rSWxhX25EQDBxRzE0W09FZDtEVHZSb0labyVmR0l/YkNvJ0hSRnlvIFlBUTY/I3spe2Zkd3JvY3NBT2U0ZFQ8SH5jU3ZCSH1qcFM/RlV0UzlETGNUPkdQOm9nMjRLRWNCMzRVdkJrI25tZWZHWXtORFM3RVJWcTp9SDFRO08gcjh4NmN3YmU0MkU/ZTR0VDRXMXNUdlJKaWE0Qz5GVXM/LUNcY1Q+Qld6YVczOntAc0MwVFVxMmJ5UXl1ZkdZdDpHWFdGWGZxTyxNQVExfypJdzJWZndyayN0S29lOTRbLEI6c1N2QkJWNzlzPkxld05sQ0xhVD5Id21tRzM0S05FaHNUVXZSY21PanVnTWl5VkdWZ0dSRnlvL0lRUjF/L0l2OGZjcUJlZ2JJX2c5NF9gUDIzU3ZSRFY3cmMwXGV5cz5LTGRUPkdwOWVXMTp7Q3tKcFRVdkJpM21rJWhHWXlaSlZ3RVJWfG8qQ2FSNj8tSXY+RmR3cmsnY0NvZzRkVmhCMnNUfGJCSXp/Qz1BNXhOZUtMZFleTmdidlcxNFtFMWgzVFR2UmZpSWJ1ZkI5fEJKWEdFUkZ5YzlNQVExbypJcDJWY3FSak9qTG9kNHRWeEk3M1NxMkl7KHlzPUxlfm5hQXxjWV5DV2Z9RzE6a0snRjFkU3ZSYn1OYzVnR1lxakdUV0ZSRnNfJklRUDF/JmY4PkZmd2Jjc3pLb2Y0dFpUSn5jU3EySEI7KmM+TGV3VzxHXGFZXkp3am1HMzRbT0VqeFRVfGJmaUJ6ZWVCOXFqQ2NHRVJGcV5vQ2FTNj8vJjQ0dmR3YmN7YUVfZjRkVHhZcnNTdlJJfW8iYz5GRXIzMUdcZF9uR2Q9aVcxNEtPRWU4VFN2UmZ9TWllZUdZdnJFYzdGWHZ8ZzxMYVM2PyR1d35GZXdibUNzR39kPkRUNEk0Q1R2Qkp2MDxDPkZFcjp3QFxkVD5EUDB6VzM0W0RrSypkU3xiYWVZaWVnR1l2alJjV0ZSRnVaYkNhUDY/JGsgPkZmcVJuS2dJP2Q0dF8gWnczVXEySWY3dmM/RkV0XytCbGNUPkdnc3VXMzRbQ3dFOWRVdkJifUBwVWZNaXJ6VGxHRVh2dk8oT2FRMX8geyhyVmN3cmZjek9vZT5EUFRANnNVdkJKYjIwUz5BNXI/LkFsYlQ+Q1A5aUczNEtKV0I6dFR2QmJxWWplaE1pe0ZFbEdIUkZ1Tm9JQVMxfyRlcTZGZXFSYHN0QV9kPkReREgyc1N2Ukh5eD8jMFxlcjM/QXxiWU5HR2phRzM0S0N7QjxEU3xiaTNgdEVlTWl6WkVmZ0VSVnZeZUZBUTY/JHl0NUZmd3JrK2JFX2U0dFQ0QDNTU3ZSRF1gMWM9QTV3WnVPTGRZXkhwM3FHMjRLRHVncmRWfGJvSVZ+ZWVHWXF2RFVnRlJGdFp0QHFRNj8pVXh1RmR3cmlYNUA/ZTR0WkBQOnNUcTJCXWh0Qz5MZXhfIFRMY1Q+Qlg5YVcwNEtIO0l0VFN2UmNjZ2p1ZkdJe0ZEandFWHZyRzJCUVA2Pyg1eHdmZHdyZXdmRD9lNGRYOFp2M1V2UkdmNjJjPUZFfW5tS0xhVD5IZDxlRzA0S0VzRThUVHEyZmFaaFVnR0l/IkZgV0ZYZnhjO0thUjY/KUsndkZld3Jmc3hEP2Y+RF8sSXpjVHZCSGI4d1MwUTV7Yz1KbGNfbkdUPmRnMDRbQ3NFOyRWdlJjYV5kRWdHSXlaSlhHRVh2enM6QkFTNj8kdXsmRmVxUmQ4P0h/ZzR0WURRN0NTdkJCUjI3Uz1BNXEzPUFsZFleQjA8bUcyOmtPJ0I0RFN8Ym1NQHJ1aEI5eV5BWEdGUkZ3an9PYVIxbylFdjJGY3dyayN2S29kNGRWeFd2Y1VxMklpenhDPkE1eFp1TWxkVD5HeDtkNzI0W0NvQT8kVHZSbUlScjVlR1l5VkRjN0ZSVnNTPE9hUTF/KkI2MHZmd3JlO2dBX2Y0ZFRkR3ZzVnZCQ0I3dEM9RlVzOnZHTGRUPkdINnU3MjRbSktDPERWfGJrQVJ/JWdCOXJ2QVRXRlhmclc/Q3FSO08pSyY2RmN3cm9IM09vZzRkU3BQPWNUdlJCXWsoQz1BNXp+bUhcYVQ+RFg5bUcxOntIO0M0RFN2Umc1UHRFaE1pcXpRZmdGUlZ6fm1JQVIxbysldTlGZXdiakgxRT9mOTRQcFI6c1R2Uk5rIDJjPkE1dl5rQ0xjX25CRDdlRzI6e0VzSnY0U3ZSbkFVeFVmR0l8TkRoV0hSVnRaY0xhUTY/KTl1OlZmd3JvR2hEf2c0dF9EST5jU3ZCSmIwNFM/QTV+bytHTGJZTkNQOWlHMTRbRGNDOnRUdkJnMVlqZWdNaXtGRWRHSFJGcV5vS2FTMW8kZXE2VmNxUmVvbUN/ZDk0XkRHMnNTdlJIcjA/Iz1MZX5uakF8YllOR1dqYUcyNEtPQW8qdFR8YmRjYHhVZU1pelpFaFdIWHZzSmNLYVE2PyR5djVGZndyayN8SD9kPkRfYFlzU1N2UkptYDFjPkE1emp1RWxjWU5BQDNxRzI0S0R1Z3c0VXEyY3lUeyVlR1lxckNlZ0ZSRnRXMkBxUTY/KVV6fUZld3JkNDBQP2Q+RFg4WHczVHEySmspc0M9QTVzMzRBbGFZXkRIOG1HMTprSkdKdjRTdlJsQVF3NWhNaXQyRGNHSFJGeX8lRUFTMW8jdXc+RmZxQm1HbElPZTRkVXBaeFNUdkJJcjI5Yz5MZXdHO0VsYllOQ0Q4aUcxNFtFe0Y8RFZxMmVlW2J1ZUI5fypFZmdIWHZzWmNFUVE7TyVpdDpWZHdyb2N2RD9lNHRYMFlzM1VxMklyMz8jP0xldk5sSyxjWU5EVDVyRzA0W0Nxbyp0VnZCY2lYYnVmTWlxakdTN0dYdnNfL0VBUzY/KDIyOlZmd3JkZD1GT2Q0dF8oWTsjVnZCR01jMmM9QTV5bytLTGNZXkFXZ2FXMjRbSTdBM1RUdlJtQ2FzRWhHWXY6VlxHR1h2dU8vTUFQMX8lYXg0ZmNxQmRrakdvZzR0VDxKeWNVdlJIbW8hcz5BNXp6dE1sYl9uSWgxdVcwOmtDZ0d4RFNxMm8pWmc1ZkdJdD5GamdGUlZxUz9JQVE7Tyk2MjRmZXdib0dqQ39mNHRWeFp+Y1N2Qk5tbypzPkZFe2c8S0xjVD5HR2xgNzA6a09DQThUVHZCYnNoZmVlTWl6TkFuZ0hYdnlvIFlRUTY/L0Y0N2Zld3Jmd2dAP2c0dF9kWDtDU3ZCSnIyNEM/RlV0XyxGPGNfbkFQOmA3Mjp7STViMFRUdkJmeVFzRWZHWXU6VG5nSFJWeW5mRDFRO08kZXh8ZmVxQm5HZUdvZz5EVDhTMWNWdkJIWyI3QzBcZXRea05sYVQ+SEduaUczNFtEN0pyZFRxMmR9TWJ1ZkdZcmJKV1dFUkZzQzhMYVI2Pyg1djJWZHFCb2thSD9kOTRQVFQyc1V8YkRLKTZjP0E1dFc+QmxiVD5HZ2V1NzI0W0B7RjM0VXZCZmFTdnVoTWl2OkFkR0ZYZnJKf0NhUzF/I2Y3dTZld2JoM3FEP2U0ZFN0SXJzU3ZCQVY5NFMwUTV1Tm5FbGRfbk1nZXdnMjprSyNJcmRUdlJnPUpgVWhNaX9mSlRXRVJWfGM9RDFRO08gVjE5NmNxQm9LYk9vZTR0VmRIN1NTdlJIdjl8Qz9BNXRKY0p8ZF9uSXd+YVcxOntKR0lydFR2QmR5XGBVZkI5cmpRbEdFUlZ4anhMYVA2Py8leXk2Y3dib0g8SU9lNGRdSFE0U1N8YkRWN3ZjP0ZVdF5qSmxhX25HVD5mVzA0S0pFanI0VnZSZj1HYnVlR1l1YkZoR0VSRnpuZkthUDFvJHl1PUZkd3JlaDFAf2Q0dFZgWypzVXEyQ1Y/JEM9RkV2VzJGfGRUPkFAOmg3MDRLSy9IcFRUcTJrKV5jNWdNaX9CQmhXSFh2c0M2RVFQMW8kcjMwdmV3YmpfbkA/ZTRkVWRUPyNUdlJHZjk2Yz5BNXVTMFtMZFQ+Q1d8aDcwOntDf0E8RFVxMmk5UnhVaE1pcXpSYWdHUkZ3enlDcVExfyNmOXhmY3diZWduS29lNGRWYFA+Y1VxMkNJeXZjPUZFfm5gUWxjWV5CWD9mVzA0S0pFZ3I0U3ZSYWlDdzVlR1l8SlVYV0VYZnhvKU5BUzY/JnIzMHZmcVJqV2VHb2c0ZFQwUD8jVHxiSGI5NmM9RlVyPyRKfGJfbkdQNXNnMTRbQ2VpcFRUfGJsRVFwVWhHSXleRWRHR1JGcVM7TUFRNj8tSyp6RmZ3YmN3YklfZDRkWTxHOnNUfGJDQjd1cz5BNXRHMFhMZFQ+Q1gwclczNFtLI0k/RFN2UmVhUXplZkdZf2JGb0dHUkZzRzRJQVAxbyNpd3xmY3FCalN2S29nPkRUaFE3M1VxMkIyMjxDP0E1eFp+RXxjX25NZDxpRzM0S08vQjc0VHEybyVbbyVmQjl2YkZgV0dSRnFHPE1BUTF/JTI4fGZjcUJrJ25Df2Q0ZFlMRTczVnZSSXsqfEM9RlV0TmVHPGJUPk1ra2pXMzRbQHFpdXRUdkJgU2pnRWVHWXpKSlBXRlhmdk8lQVFTMW8mZXM0ZmZxQmVoO0g/ZT5EUFBYdmNUdlJCSXA+YzBWVXl+YUV8ZF9uQ0A1dlcyNFtFZWMzNFVxMmVja2Y1ZUdZcn5DWEdGUkZySn9BQVE2PyNmMDpGY3FCay9nQU9lNGRfJEpyc1V2UkhNanhTPUZFemM5SXxkWU5CODtvZzI6a0pbSXM0U3xiZnFcYnVmQjl2dkRaZ0ZYdnRafE1BUztPJXV3OlZmd3JgX2lFP2U0dFVkWDpzU3ZSRFI7JmM/TGV3TmBTTGNZXkRUO2VHMTRbQF9AMFRUcTJkbU1idWZCOXJ+RFRHRlh2cU5uSUFTMW8leXU1NmZxQmV4NklPZTRkU2REPmNTcTJCXWM2MzBRNXReZEhMZF9uR0dsZlcwNEtJV0h8RFR2UmJlWm8lZkdZcHpWaFdIUkZyXylHcVAxfy9JcztmZXdyZng7ST9mNHRQUFE4Q1N8YkhtaHZzPkZFdlp/RjxkX25HSDxlRzE0S0VjSyNEVHZSaDNrZmVlR0lyelZcR0ZSVnNKfElBUTFvIFY2NHZjcUJoO2dJT2Q5NFZ4SyhTU3ZSSGYyMnMwVkV5dz1CfGRUPkNQNXA3MjRbST9JcmRUdlJkdVdqdWZNaXc+Qmc3RlhmdlcyRVFTMX8vJXA5NmR3Ym9vaUlPZTRkWTxANnNVdkJKZjgzMz1GRXEzOUV8Y19uQVd3bUcwNFtPZWk8RFV8YmJpSWdVZUdJfypKU1dGUlZ0Ty1FUVAxbypbJ3pWZndyYFgxQ39nNGRdSFsoQ1V2QkhWOnlzPUxlcTp7R1xhWU5DODhoNzE0W08vQDY0VHZSaD1MYFVnQjl/KlZXR0hSRnFaeE9hUjY/Kyl4eHZld2Jma2ZHf2U0dFRkR35jU3xiSX1gNnM+RkV4RzBfTGNUPkRUPmlXMzprTUdIdnRWfGJrSVtldWVHWXxGRFFnRVJGd3M9RkFRNj8qSXU3ZmZ3cm9HaklfZTRkWDxGNzNUcTJJZjQycz9GRXVHMkdMYllOSmdlcVczOntKX0h0RFZ8Ym1JRnJ1ZUdZcnJGaydGUkZ0Xm5FQVA7TyNyNTpWZndyZT9oS29nNHReSFsuY1NxMkhZdDJzP0ZFdEc4R0xkVD5NaDZ5VzI0W0V3QDY0U3ZSZzlCc1VlR1lwdGVld0ZSRnZOaEhxUTF/IHsiOUZld2Jme2NLb2Q+RFlIQDZzVHZSQl1gNFM/RkV+bmFLLGJfbkdYOWhnMzRLRntKeERWfGJrQV5oRWdHWXpaSVsnRVJGeX5pT2FTMW8lNXYzZmN3cmg/aklfZjR0VnxBOENUdkJKayh0Qz1BNXI6f01sZF9uSndsaDcyOmtEY0I2ZFR2UmV1WGplZU1pcXJEYndHWHZ1SnpKQVE2Py5JeDFGZndyY2NzQT9lNHRZXEk3U1R2UkFWOT5jPUZFeFc1Q0xjWV5KaDB0NzE6a0VjRDhUVXxiZW1PYnVlQjl/KkRoR0dYdnNKY01BUTY/JHI3e2Zld3JvKDRAP2Q0ZFkwWTZzVHZSTml6cmM/QTV9bmFFbGRZXkdQMHQ3MTprSlNLKFRTdlJiYVB6ZWZHWXlCSVsnRlJGcTpnQTFRNj8mcXhwNmV3cmB4OkJPZTk0X2hCOyNTdkJIdjIyYz1GVXpjO0FsY1Q+QVdsYUcxOmtJNWU7RFV2UmshW2BVZUdZek5GZEdHWHZ1Sn1BUVAxfy8lenxmY3dyY39nRD9kNGRfREQ5Y1R2UkRGMjNDPUE1e2M0SmxkX25BUDtjZzI0W0U7QjNUVHZSYFVfbEVnR0l7TkRYV0hYZnlqckZRUDY/LkY3d2Zmd3JvT2VFX2Q0dF5AUTMzU3EyQ1Y7IXM9QTV+YzlLTGRUPkdbYH1HMDRLRWVgOERVfGJidVphdWVHWXU+SlZnRVJGeX5pT2FSMX8jeyA0dmRxQmN/bEVfZTk0X2RCNFNTcTJIVjd4UzBRNXA6bEdMZFleR0djdlcwNEtJU0hyZFN2UmJlWmp1ZkdZcHpWaFdIUkZyXylGUVA2Py9JcztmZXdyZn9sT29nNGRdREI3M1N2QkNZZDFzP0E1e25tS0xjVD5NaDliRzE0W08rRjhUVnEyaDNrYzVnR1lyYkJkV0ZYdnFPJk9hUTY/KTVzNGZld2Jjc3dFX2U+RFlYUDBTU3xiSGY3OEM+RlV6ZzJBbGNZXkpwMnRnMzRbRXtCPyRWfGJjbU5jNWZNaXc+Q1ZnSFJGfGp6TUFQO08jYjh+RmNxUm1HaUlfZDk0WyRFPmNVfGJEUj8oQz1GVXVXMUp8Y19uQldmeVcxNFtDZWl4RFN2UmZjaGp1aEI5f0pTbEdIUkZ3fyZCQVI2PysmMjg2ZndiYHN0TU9nNGRUbEY6c1N2QkdyMT5jPkxlcDc0Q0xhWV5KcDV5RzM6e0g1ZTdEVnZSYnFcb0VlR1l1alRnV0ZSRnVaYkVBUTtPJHV4fkZmcVJuS2dJP2U0dFNoSTpjVHEySFY3dmM/RkV0XytFfGNfbkhnc3lXMzRLQ3tIeWRVdkJicVF5dWVHWXJ6UWxHRVh2dk8oT2FRMX8maX8iVmZ3cmRjekN/ZT5EUFRAOFNVcTJKYjM0Uz5BNXI6fkFsYlQ+QVd3YlcwNFtLJ0l2dFN8YmRtTWhFZk1pcH5CZndGUlZxNz9JUVMxfyspeTNmZndyZWN4SU9lOTRYOFsuY1N2Qkd5eHhDPUZVcTpxSWxiWU5KZDprZzI0S0VvRT8kU3ZSZGFYZEVoTWl/RkpbJ0ZSRnhqdkpBUDY/JGsodkZmd3JgVD1If2Q0dFN0TyhTU3EySEY3MmMwXGV5fm9PTGFUPkhwNH9nMzprTUNKeFRVdlJlYV5gVWVNaXpORFc3RVJWempwXGFQNj8qVXsmVmNxQmU0Mkh/ZTR0WVRRN0NUdkJKcjA0Uz1MZXEzPUFsZFleQjg6bUcyOntPK0c0RFR8Ymg9QHRFZkdZdj5KUWdFUkZ0WndGUVE2PyVpeXk2ZXdiayN4RG9lOTRZVEE2Y1R2Uk5rIjNDPUE1eE8iR1xiX25Kd3hhVzE6e0N7SDpkVHZCYFFQd0VmR1l2fkJoV0VSVnFXOUFBUzFvJHl2OkZlcVJpP2NAf2c0dFQ4UjpzVXZSR31iOFM9RkV8YzFMTGNUPkFHZnlXMzRbQHVhNFRUcTJuQVF3RWdHWXFuQmhHRlJGcT8tQlFTNj8jZXc2RmV3YmZoM0Q/Zz5EXkhZOyNWdkJHTWM0Qz1BNXlvK0NcY1Q+QVA4aUcyOmtOR0IzVFR2Um1FW2NFZ0dZe0paVmdGWHZxPy9NQVAxfyZheDpWZHFSaytqT29nNHRUPEpzM1V2UkhpaDhTPkZVdEMwXWxiX25JZ3tlVzE6a0NnQDhEVnEyY2laaFVlTWlwWlFkV0VYZnpvIFAxUzF/Kyl3dGZjd2JlaDREf2Q0dF5ISnBTU3xiRFY6dmMwXGV9anBSfGRUPkd4PGd3MzprTydBNzRUdkJoOVF4RWhHWXFmRG8nR1JGdU8gXGFTNj8ldjc2VmZxQmkzc0A/ZTR0WVBfLyNTdlJESyl2cz5GVXVXNkV8ZFleQUduY2cyNFtNS0c2dFN8YmV5U3BVZUdZdTZFaFdGWHZ0RzpHYVExfy8rJTpGY3dyb2tnS29mNGRQVFEzQ1N2UkM9aTsjPUxldFM7SExkX25Hd3hpRzI6e0NzQTxEVHZSZG1KZzVmTWl1OlZTN0VSVnxnPUJBUTY/IFY3elZkcVJva2ZJT2c0ZF9ISHNTVHZSQUIzPmM/RlV5fyBWbGNUPklgM3A3MzRbSTNJdmRWdlJoNVxmZWVNaXtGQVdXRVJGdl8gVVFRMW8mZjsoZmVxQmV/ZUFfZTRkX2hXdnNUdkJERjU8Qz5BNXVKfkI8Y19uSmdmcVczOntFc0c4VFRxMmR5UHM1Z01pcWpFWmdGUlZ5YzVJMVExfyZldjxmY3FCY2dnT29lNHRYOEI2M1V2UkJCMTBTPkE1dl8iQWxhWU5CRDVzZzM6e0pPR3xEVXZCb0FTemVoR0l2OkdSZ0ZYZnNXP01BUDFvKyY3MkZjcVJpM3JEb2Q5NFssSXBTU3xiTW1mM0M/QTV6amZAXGNUPkh7a2Q3MDRbT2dJdmRTcTJkYVpgVWdHWXJ+SFBXRlJWc18oQ2FQNj8geyl2RmVxQmB/bElfZDR0U2hfLyNUdkJETWh4Uz1MZXpzP0V8Y1leSXA0dVczNFtGc0UxZFR2UmJ5RnJ1Z0dJfyRlaEdGUlZ1XmhIMVAxfy9GMDk2ZndibU9gVV9lNGRQdEAwU1N2QkdJZTJzP0ZFc0M+QjxhX25EQDpmVzE0S0lBaXV0VXZCa0VXaEVmTWlxZkVvJ0hYdnE3NkJBUzY/I2V3PkZjcVJvY3NAP2Y0dFk8TyZjVnxiR2Y3OEM+RlV6ZzJJbGRfbkJQP2lXMzRbRXtCPyRWfGJjYVtgVWZHWXJqVmZnSFJGfGp6TUFQO08jaXh+RmZxUmk3aUN/ZDk0WyhWM0NVdlJOaWE8Qz1GRXlnP0I8Y1lOQlgxeTczNEtJW0snNFR8YmVza2dVaEdJdjZGUWdFUlZ4enRHcVAxfyNyOHdmZXdiaVQ7QV9kNGRbJFM+Y1N8YkpmN3BTPUE1d1pxRXxkX25JcDxmRzM0W0lVZ3VkVHZSaDlHZnVnR1lxfkZgV0ZSVnZDOkZBUDY/KyF6dUZld3Jkc3ZMb2Y0dFVoUzxDU3EyREljOnM9RlVzPydCbGFUPkhHbW1HMzprSVNAM1RTdlJifUhldWdHSXV6QlInQiwkWGsheyRjQzhkUntJZVl6VXh3W2YyQUlIZnMzMVp9PToyIit8ZnA5STU3ZU8rRFl8QlJ5YXk/IXRBOXpVeHFLZjJBSUJRdjMxanY3bmN8Zmxnc1g6MidiJHVhYFFidW1lZHIiciwjZTZqQnVDVT9OQlZjSUg2cDtkM1shcXspM3xjVWJWcD5uR0xBVjhBR29gc0ZoTkJ0QXlxd3RtPToyInR1ZXwtfmIjR2hIdlNaeXtvQkJNN3ItMio4WlNxMjNoalppST9oUFJQcD1KdlplUkJaYVJ2XmFQUH5mVlxlUWJzcDVVekIxUnFXU2h1M0ZZZ3IzaGZUQm1gc1BSWkVQVjZaZVJOSnNgNVthUFpDMWY8ZlFieVVWXEpBMVJ5Uj1LaH5laWVKZGNxSmlKYHZQUTJGQ2ZOS2NjcHVOQ0EwPUl5Pm1FOVRUW2JFUVxkdVFiQlhEWmJDM2Q9SFNjdkNSWkk3UVU2SFFpY3wiLWIjQUhCNGNZeXtlckFdN3ItMio5dHhLTEk2SltKRFRpfGprR2lOY2lpN1lTekhRY3lJaUlJMDdXWkZeSUxoU1JWRFRReUltSXJCNmhqTWlQeHljaWd7a0N1MDJZeVZXXEhlWV5GSW1Jf2hJTypNaVlmR0JpfGhcTCIsYiNBSEIyRFE5e2VyQV03ci0yKjJ5QDZgOGI3WXB4U1Y1WW1JckI8SHpNaVloeWlZfGNMR2N1T0F4e2NkPGplZ2lJY2lpN1Njek1pUHlJY2xnczRsTkhRakpMZWl/aElGeU5pWWZHQmlqSFtDeUl7TWhyN1lweFNWOGltSWE4W0xmTWlZZ3haSGlOZW9iXmleaDA1WUR2VGlwfWdVMHI1aXZVUltkcjBURkttQTB0VFNWRFxBeUIwXGU4SVlncjxMaUp5VDRmXkhhMTNiTGReSGU0Q1haSFFpaldXXylJdFl6SmRhNUtmZkk7ZGp7anVbaldRZTxiNWl2VlFcZGhOREZHT0EwcjFZakpqQXlObUlyQjJoakhRZHh5Y2wiJHIjOGpwdGNYeXtlckFrZTIhQio/Z0dvZWxpQCNuYi4iLChZQlxCVjhDcV1jc0ZoTkpqYn0/ajInQiNlb2tsIiJyIXpkbUZ/JFNLSHVfZklgOXtlYkJrZTJxQzJKdD0xUi0yKjAyMzAxMDcwPCItQixDNjJTclloQ0doQXc2YX5PZEItMioyeUVFaEk1RFdWRFBWMDltTWZDOVN4d1pJclp5S0pJdWVAeFdXPGlxaEpFU1l1VVlGNHZNTWlJcWRMZUJdRkRBZl5IUVI6WXBSUkVGZ1hlUFNWV1hUSGZRWEhpdFpmW2ZmNkdEamN5f0ReQDNWWkxmUzA5aVAwOmVpdlNGZ2ZZbUJ0e2lDRl5jUHlBNWl/YTtFPk1iZUxjM2hod1pZaHdKQ3lDOkp6V1pTUTE7Tmd5aVEyV1pSQlhKUHh5aVl+Q0xCNGIzZHZZZVNkcjpLaUdUZT5NZFVMYzNoaHdaWWh3SkN5QzpKeldaU1ExO0B3eWlbLkdZWDJXQmpyWEZZZkdaQ3lDOkp6V1pTUTE7SWd5Y2xlN1VuZT1ianJYRllmR1pDeUM6SnpXWlNRMTtAd3lhWy5HWVgyV0JqclhGWWZHWkN5QzpKeldaU1ExO0lneWVcZTdVbmU9YmpyWEZZZkdaQ3lDOkp6V1pTUThcQ1EzTEQ2UTRsZHttTEZIQWU2SnNhdkpkayRoSlI5V1FlNGxlaX9nWUpqV0ZsZ3ZJXWlIWlNManJeSkRBZl5IUVc5WXtCSTVGZ1hgPUhuQjhUSGIyV0hmTUhqW2ZgNkdCVDdpd1RYYDNROGxmUzA5aVAwOmVpdlNGZHZZYV9IempQUTxpX2RwMVQ0ZENdYH5pUkU9ZW1iXW5NY3A1U0R2UlFaV1dbJUVZXyxlWUhwOWFYcDpvR3FIUFwiJHIjOGpzelNZeXtlckFrZTIhQio5dHA8Q2hgM1h5UjVaSGZZU05FT0V2W2JheThTZUE5cWRSVkNXOGIzUWZdaVhlOF9ERklybkZEUFF5RF1JZ3RZRjdsY1JGXGRhXkl3WGpVU1lGRENXYHE9SHpJdmh5WERkRTM2V1pFWl8qVkNUSHpiWlZFR1hhMDVUNl1iWypIUVJCWmNuYlRWaXA6a0g5SWlJclpxUXN2RGB4aEVidlA6VkpDPkNGSmtJclhUY3ZZaURBNUBXTkZdSUZHU1RWRENWSG1iYFU+YWR5OW1AP25jYTZTQmhpS2RsTGRaWHdiNlBUeFtDUlVIWWxkVWh2SXZgNUtjVjxgNmhke2lQMVVdRDJaeVY9TWNiN21vR0hjOVg0ZEFjXkprS2lHWlxqQjRjeUlpSTNrY1xqWFFaaGtnV0k1WlRMbmJXN2RJVV5IXUJzeFlFPkE9SWh3VlIxRVZjdTVUWXlSOFtIYjpTTkVPTWpbYmY6WFNlQTM6WTA5bUA/bmNhNlNCbWlNZWl2R05JaDAzY15AN1EyTWdbI2RJUl5MYVJ4O2NSRldTZjpYU2I6RlRhOlZHWDdkUVZYaElXM2RaWm5FRmlyQzJQdlVZWER3RGp7ZElZcDprSGpXQmE+Q0JheUEzYkZJdFtAdVJXMlMxWnVTOVFIe2NhNGdRbUZCNFJyUzlJeUAwVTpKfUQ1S29GRHhRVEB1WFRJNFJheU1uQ3ZDOVN2WWlKaDtlbGpYQVRaW2hdY3E6U0xuYVsnYjFlXkhTYng4UVAyUTlZe21mUj5GQ2g9RVRZeVI4UFhiOlRORU9JaltmZjpIWVA5UzpZMDltRz9mWFlndFpGPkA0WkZbalQ8aFRuZHthUDFVXUl+SnFReTdTYjFYTkplNFJheURdSWd4S0c2XGFlSkRNRUZHRWd9TmVqakpkb2k9ZlM4Z1VqWlljZ39jQFxpQz1LQHVWVTZBPUZCXWRqQTthX2pNb0pkdEdXNkdcSXZLZmNeTG1Gel5qWUxlSFdEZl1JXGdUWnRqYVZIbWJlRT5lZTZHRGk0ZFBReUM/S2lNZmpqUzRsYTE1WHpFX0xoNF9ObktpVTpVVGpaR1FTOkpuRjZEWV9Ee2Jac2RXVEB3X0pAdFNnNlVaVUpETUVGR0VnfU5la2ZKbkF5QT9MaUdCZHZXWltlOWNpZ3daQTpdYmlyU0xNaUdRanpIRGBwPWJhMkhEY3lJaUhofmlcZGM5WTJSOUV5OEpVdlhKXGpXRmdgN1NlfGhKU3lJaUhoeFFsaThEamA3RW1saEpTeUlpRnh9YmtkZ1NnPklpWWd3XElmQjVjeUlpTGE3WVE+TmNpZlNMRXlHWVU6UjpbZkdVZXk5aVlneElAdk1iaXJTTElpR1RvLkhaWnpHWlR5SFhXcT09PCIiciMyWnl1UmZ2M2hTaTpzdkB1Z3FDbTFCLTt6NHIjOGp0NVJmdjNjM2hoRHdAdWdxSk0xQi0yKjR4YHR6M31yIiwqdXhhfyRjQnh3WntKZVl0b0pxa2QyQUtoYXVDMVp9PToyIitRY0lsYnJiKWIsJWNEfmJ1cilyLCFoRHVlZHN+aGNlZ3xpYnVsIixCJWVhbGFndWRiJWIsIW1MYnR1YiRyLCJwU3lpZHFubVIiLC5pQUxBVjhJUUxgc0VoTk8kUX0/ajIiK3lqclFKTyVOQWJxM3NzYHp3dklFbEFEYVN/IDIxf0M0aDE2akF1Q1pfTk0zajItW1IsKF9HTEJWOEVRXW0xSjIre1NyI2J6MiIiLC1yK3JzciNiKj1yK3wjciNiejIoYiR0c3B/KjRvKWJ1ZnBycWJ0c2NpbGFlZHRzdG4jdn4hb2duJntlfm8vaWN+ZH0tL25mdWN0fSVoYk0ndWJxaWZoY3N9LS9pbGVraG0idW0lb2htIW9tQi5tXXwtdHIhdERrIzsmU0tCRVsiWmp5e2ViQ0FVMXFDOHpwPTFSLTIqNVJfJHVxV1B1WWsgW0NSdU1mPmJVQWh2UjdGP2djaWN5aVd6dmpacldBWFh4WV1lNFF2b2N5Q1JGSlF4aUVYdVp9SlliaVA9R1JfLG9NSnB3WWsuZjRhVk1mPmB1Um9GWndGNVd2V3R6aVdwVmdVMVZBWF5DPkdFNlF2aUgzWHFFSlF6WVNiNVp9Sllic2l9RVJfJHV1U1B4WWsuYXpXRk1mPmRlV1Q1WndGNVFCakR8aVdyZmRkUlZBWFZYUmM1NFF2aVN6VXJISlFyX25ENVI9SltCdWI+RFJfLkVxUjB1WWsoS01ANUtmPmpFUlJ2UzdGNVFdRTR7aVdyZmVhYlRBWFZIQld1OFF2YVNvQ1JGSlFwdDxJRlI9Sl9MQmM+RlJfLk9KVEB0WWsuYXRoRkxmPmJbZ1p1UjdGODdkZlR6aVd6byVeYlhBWFpeaUN0N1F2ZVgzY3JHSlF2T2pWNVl9SltCdVVeRVJfITVtQn9nWWsoS05CdkpmPmJFTU9mUzdGM2dlUXR8aVd2ZnNRMVdBWFE+Y2p0N1F2Z3NvRTJESlF2X2JbZlA9SlVybUdOSFJfJkV+RD9oWWssQX1MRkxmPmpLZVU2UjdGOVdiXmR8aVd+YF9BMVdBWFh4U1dVOFF2ZVg1VlJHSlFxNDRiNVI9SlFicmA+RFJfKl9PSWB4WWsicXRqdUtmPmpLZGJGWndGMUFOQ0R7aVd4XyFhMlRBWFZIVWdFNVF2bUNvRjJFSlF0b2FiVlp9Sl9HOVE+RlJfJl9BZFB3WWsmYWNpZU5mPmR1ZmlvYTpWSGJNR3N6b0xBR01BOkZIMDp1WVdxSnZbIktNR3tjOEFIV0hWUlthZkdESFNuRV1qdXVeR3VadmF0SU1BPGxvKGl1a05AenpWQkJNR3N6b0xBR1RjakdIMDBVQVJxQzZbIltIUHthOEFGd15IcVphZkNETUE+Rl1qeXVUY2ZRNmF6f2lGO2lvKGtFYFd/YDpWQThuS2R8b0xLZ1NSSkZIMDRVWlMyQDZbJGtDZUxjOEFEUjlTMV1hZkV6Y2I+R11qd1A8SHVQNmF6eU5JXGpvKGNLImZQcTpWQkhjaHR6b0xFTWRoekhIMDhFUltCQDZbJHY8T0xgOEFIQjVUMl1hZkY6YWF+Rl1qd1FEbT08IiNyIXp0aTp7JlNLQlVZNzdgPmFJfydKSkF1RnFheyphcHVYUDxIbyE1dWtiY2k6c3dAdWdhQWlZRzRgPUItMiozQzBXbWU7SUQ4SU1BM0Q2V1picUNZYThYNzJJS01vaHh4QXR4RXhSMVxLSnRqY0d0eU1iRjZmXWRMQ2hYc0RWcHhZeXJHVng9aHNlaThZR1F8bWRrYVlzcjhJSDpFMzZeQjdpY31nOyY/KT9BV0k2ZFpHVEp3OlxoV3pLR1hRVUReRFxhanxKUTY0REd/b2Y6XGdSUDlme2h+ZEtGPExKcHZCV3VhWTtkfW1kRFdReE1CeE5nNmF0O2JFcUM5ZGdaR3tAeTFyVld7TkxBVm1CRX1nNWR8Q1NIUHBfKkV1OERWTyU2dWZOZUdydTY+Y2c3ZENSRndTPWpDMH5JdWJzVEEzPmFHY0gzNWkzanE2ansqf2NaWVNzYlA1RURwdlpJc1RpcFh/QzBZOVZgNEhMY0lgMlkzdjVRf25BZ31jMzB1UFd4ODk/JmhJOD9GdnNVdXV4Q2RcYDQ1R0ReRDJHRDlpUnI7aU5IOkcxYUhxcVdwc1lqe2pbKm1LIUhMR19vRmFnM3YzcDNmSkVRVnhlQWZIOXZFT2FsRz9hRTdkX0I0R1RRUmhYOmA8YTpQO25nYHdsZVFAU2c5OHM5ZllmWT1nanhLYFduRlp5U2lhQjYyOldKe2Fda2VlSDY1QzRNbU9IZ3YyR0Z6RHFwOVJ8Q1VoVVNiRkc6VWp4RHdKZHVMa0J/KlVnWHVTNVJ0R1dof2RsTGNUMjplYVhJVjtCTk8lRnsjNUFXc2NtQ1A9RUhkYVRhZ0N/IDU2Q2dVUXhjcldGOlR2dFtjYDJnREI2R3MxdnZTQWBea2hldmhVM0FAPGVXZmJBTUFpeD8nS0F1cFh4RVVacmVGUkNYQ1JiXEJHdWk7JEM0WFRCPk5EPWk0eDRqeHsmVGhpc2M/IVRwPy1lRDlQW2UwN3NKen8ocV9mY3R5S2snQFBaeEFZfkI1Z1Nndj5oUl1kYUtgfUxBdGVEaFhnSGh0akc0N3FxR05AWENxYVlmVGNmWUhENWZSQjd1Vlh4UnpqeEYxZ2E1f0JFZD9GfUdodzFBODd6R0xCVH5nb0pVdnI2SDhmVzNmQFUxWGNLSVFhV0MxXUZ0SXd1ZFNrZUE4Z2VYSTc6WEtvZlY+Y3R+SmpZPWFnRXB7Y0FyelJIVTRheWF9RmtmcmdXS0lRZWdEU1Zzd0FvSDNSfkdfQV5rRztqV0k1eHNPQklYU2I1OURDdkN0dGpoOyV2Pk1CSTdFQlZAc1hqYURiQDdaal9GV0R2PGlQO2VzNnlFQjNTYW5geEl5eUlRNkFSOFpidnRqRVBTUV5IaktIV3skeFV/IVtBaTFcRXV5d0F0YFAxV3l9YjB6Z1N0ZEF0Z3dEYXRHSHpXNVE6TkZpPkN3M0Mwd0Z3SGhdYVY0YTZafWkwOTdiT0NSRVxNaVQ2M05mM2pyMFdtanstZ1liQlU1XmxmTWJqf25pV0VKWTh3bUp2RzBcTm1DSFhKT2snNmpvJ1hzczNqZjI5WkcyNk5ETGpQPG5CU0RLJD9DUTsldFFYbyY4cT9vJmNJUVZsZTxFYVp3Z09ER3tAXCImQiVncUNTbEc0Yjg/TyRxfyM6TUVIWTFwU1ZrKUdVNEp5e2ViLTIqN1ZpYV1jYDZSMmp2WERsakdFYDUyOVZ5OmJpb2NRUTlGVlNCV1RlNHZVY3JYQ2NAeX1COklpSnh3RGZ4eEVgMldaVXk9YmltSXJmOURQU0EyNGVEe2VoeDVVZVhjMVR6SFFZY3wiJEImZXlLREFNaDNVQXA5aypBfE5mO09iXWhDQVhCOnVobkF0VG8uayNTS0JVWy03Yi0yKjN7SypBeUExSVM4SVtmTmA8YVA6XGJRNGZtRkFvKkIxfGU/Kll6SWdtRVVMS2lBN3dvRHNnZldZOmtLKlF4YTFKUzhDa2U4QT9hUDpcZlNEY11GS0Z3eW9qZT8mWXJHWGZFVUJrbkdJem9EdVd8bW8ra0skYXhnUkVTOElLaUNDOWFQOkZeSENmXUZBZnZSP29lPypTXkhZb0VVQmAyV0l8b0R5V2FZaDN7SypBeUxiSlM4SVtiV1E2cVA6VlJHRmJdRkFvLG1vaGU/KlluQ0hjRVVMS2d5Z31vRHVXbG5oO2tLJGY2V1FDYzhJVUR3UTBxUDRsakI2ZV1GS0Z5Q0J5ZT8qQ1hhN21FVURVUlZHeW9Ec2dpMjg8a0sqVj5DMUZjOElbYVlgM3FQOkxpQzRmXUZBZnpCMXB1PypZclE3YUVVRmtmV1h6b0R5V2hjSTlrSypBd3lhSFM4Q2tmXWAycVA6XGxoRGRdRkdfIkdQfGU/KlluR0llRVVMS2lDSnlvRHlHdGdZOWtLKlF0d1NFUzhDZV5IQDxhUDpcZlIzaV1GQWZxWW9jdT8qSWlDOGZFVUJrakI5cH9EeVdyUTg3a0skYXZXUkRTOElbaGNBOWFQOkxneWNoXUZFZnZdb2J1PypZbGhIZEVVSFAyR1h8b0R5V25HSytrSypBeUNEQ1M4SUtpOEA8YVA6XGU1RGJdRkFvIldfaWU/KllmQzdmRVVCa2lDSnlvRHlHdGdZOWtLKlF0d1NFUzhDZVpHUThhUDRhNldUZF1GRWZxWW9jdT8qSWlDOGZFVUJrakI5cH9EeVdyXGg8a0sqVj5CNENjOElQOlNBOWFQOkxneWNkXUZFZnhiMn5lPyRjSkdKYFVVQmtuQjl3b0R1V2ZCOTtrSypBeUNEQ1M4SUtpPWM3YVA2UTpCNmVdRkVmdlI/a2U/JGlsYjpvRVVCa2FZZ3N/RHlHaU5oPGtLKlF2UzFGUzhJVV5CMjBxUDpRMkVUaV1GRW8iQ0B5ZT8qSWd5Z2JFVUJgMUE6d29EfUd4ZFg4a0sqVjdtYUNTOElLZ3lgOGFQOlExQTZhXUZPTyhkQH9lPy5JfURZY0VVTEtneWd5b0RzZ2ZXWTprSyRhdlRDRVM4TUtpQ0M5YVA6QTpHVGZdRkFvLkdQf2U/LklxV1lvRVVCYDJXWHlvRHNnYVloM3tLKkF5R1NFYzhJW2xoQTBxUDpcZkdGal1GQW8gd1B3ZT8mWXpDSGNFVUxLZ3lnd29Ec2d6Qj8ra0skYXZSMUVTOENrbGIzNXFQOlZeSFRlXUZLRnlGUnJ7LWheaUZIOktBWlN+SEJWRFYzaXpHUTU0QXFeZTZEOk9kYmduQ0RjVFNKTmd5YVd2WHVfbGI1W2stZmM2Xmg2S0FaXUxtZFFEVjVZbGIwMDRBeU5pQ0czT2RsQVJIVGRUU0RuaGVUWHZYeV9lN1Veay1ibmZeaDNLQVpDd3lhUURWNVl2UzA0NEF5WF5IVTNPZGJnfkIzY1RTRG5saEFQNlh5T2lDSFlrLWxIUkhZNEtBVGN4YTFWRFYzaWpDMDp0QXlTOkdWMU9kZmdhWWNjZFNKTmlMYVE2WHlfYTI4W2stZm5mVkg9S0FaU3ZIU1NEVjlZZkNBOXRNOUwiIkIpdnJaRGNJeXtlckhHZDVxQzJKczVZUHY2aFd4aDxmc1lGVThneDInYiozeTZSRVB9ZDl8SlI1YDBYOmd1Z3VeQHZaV2NYW2lvJmA7QTxgdkQ5YUQ6anp/QFlacllfJGk1UzV+TGQyNVZWUXU5Pm8qRnJZckQyU2xENnsnYTA5dXlaRlRnPywiKkIsQzY3SVFaaENHaEd0VmR+T2RCLTIqMz1HTEFWOEFBTm0xTXIiK3V5cmtqckl+SjIiK3JGdUMyanY4OFB0dmxnc1FcZnlpSnU5eH9KcktkMkFEOjIiK1g0f0Z0NmVbTEtncVA5PmB5WU5mRWRDcmBYcU5oZ2Q3dWpsZDtrSTl9Q3VVPmtncVJkdVhHXWlCbWA7YFJyOnNjenNFekE+REZNS0ZTWmllTypdanN2dzRSR3NnZDR2MjNqTUpHNmNEREZQNGc1OnheQT1gdHhFfEhFMzsoOThHNHdhWyp6TyNEc1hnZ1phNGR1NU5oMFFDSkhOT0NRMFZaRFd5fGpIc11EYTdOTm8iUXpTUn9MaUNTNH9ESVp2SU9LZ3dgOGVhdVlOYH9KdXpQWHFKeHhmaXNabGA7bGk5fkN1VTM4UjRSVHVTN0dtQWNwO2M3MVFlaWpzRjUyeVRKTUtKQ1tNZ0k6XW8jangyX0dzZDlTTEIzak1CQm";
                    var r = await decode(arrFunc, p, "p");
                    var json = r.FirstOrDefault().Value;


                    var objFinish = JsonConvert.DeserializeObject(json);
                    json = JsonConvert.SerializeObject(objFinish);
                    szDecoded = ReplaceSigns(json, is_encode);

                    objFinish = JsonConvert.DeserializeObject(szDecoded);
                    
                    await ParseJSON(map, objFinish, false);
                    var sr = JsonConvert.SerializeObject(objFinish);

                }
                else
                {
                    var map = GetMap(content, is_encode);
                    var prop_o = map["prop_o"].Trim().Split("\n");
                    map.Remove("prop_o");

                    List<string> reverseFunc = GetReverseFunc(prop_o);
                    var arrFunc = funcSelect(reverseFunc);

                    var path = currentDir + "fp.json";
                    var jsonFP = File.ReadAllText(path);
                    var objFinish = ReplaceSigns(jsonFP, is_encode);
                    await ParseJSON(map, objFinish, true);
                    var r = await encode(arrFunc, szDecoded, "p");
                    var rees84 = r.FirstOrDefault().Value;


                    var resultFp = new resultFp(rees84, tokens["st"], tokens["sr"], token.ToString(), interrogation);

                    var postBody = JsonConvert.SerializeObject(resultFp);
                }
            }

            Console.ReadLine();
            
        }

        private static List<string> GetReverseFunc(string[] arrFuncs)
        {
            var key = arrFuncs[0];//?
            List<string> tmp = new List<string>(arrFuncs);
            tmp.Add(key);
            tmp.RemoveAt(0);
            tmp.Reverse();
            return tmp;
        }

        private static Dictionary<string,string> GetMap(string content, bool is_encode)
        {
            Dictionary<string, string> map;
            map = GetRegExp(content, is_encode);
            if (map != null && map.Count < 20)
            {
                Console.WriteLine("Error parse! map!");
                return null;
            }
            return map;
        }

        private static Dictionary<string, string> GetStSr(string content)
        {
            Dictionary<string, string> tokens = new Dictionary<string, string>();
            var pattern = @"\s+\w+\['stopInternal'\]\('prop_o'\);\s+\w+\['st'\] = (\d+);\s+\w+\['sr'\] = (\d+);";
            var m = Regex.Match(content, pattern);

            if (m.Success)
            {
                tokens.Add("st", m.Groups[1].Value);
                tokens.Add("sr", m.Groups[2].Value);
            }

            return tokens;
        }

        private static Dictionary<string, string> GetSigns(string content)
        {
            var pattern = @"(\w+\[\""([a-zA-Z0-9\+\\\/\=\.]+)\""\] = | \w+\.([a-zA-Z0-9\+\\\/\=\.]+) =)";
            var m = Regex.Match(content, pattern);

            Dictionary<string, string> signs = new Dictionary<string, string>();

            string[] tokens = {"type","time_stamp","client_x","client_y","screen_x","screen_y","mouse_events","type","time_stamp","type","time_stamp","identifier","client_x","client_y","screen_x","screen_y","radius_x","radius_y","rotation_angle","force","changed_touches","bio","user_agent","language","property_descriptor","array","uageslang guageslan","build_id","date","file","performance","timeline","navigation_start","current_time","width","height","avail_height","avail_left","avail_top","avail_width","pixel_depth","inner_width","inner_height","outer_width","outer_height","device_pixel_ratio","orientation_type ","screen_x","screen_y","screen","timezone","indexed_db","add_behavior","open_database","cpu_class","platform","do_not_track","splugin","named_item_name","item_name","refresh_name","plugins_meta","winding ","towebp","towebp","blending","error","img","canvas","img","error","extensions","aliased_line_width_range","aliased_point_size_range","alpha_bits","antialiasing","blue_bits","depth_bits","green_bits","max_anisotrbopy","max_combined_texture_image_units","max_cube_map_texture_size","max_fragment_uniform_vectors","max_render_bbuffer_size","max_texture_image_units","max_texture_size","max_varying_vectors","max_vertex_attribs","max_vertex_texture_image_units","max_vertex_uniform_vectors","max_viewporbt_dims","red_bits","renderer","shading_language_version","stencil_bits","vendor","version","vertex_shader_high_float_precision ","vertex_shader_high_float_precision_range_min","vertex_shader_high_float_precision_range_max","vertex_shader_medium_float_precision","vertex_shader_medium_float_precision_range_min","vertex_shader_medium_float_precision_range_max","vertex_shader_low_float_precision","vertex_shader_low_float_precision_range_min","vertex_shader_low_float_precision_range_max","fragqment_shader_high_float_precision","fragqment_shader_high_float_precision_range_min","fragqment_shader_high_float_precision_range_max","fragqment_shader_medium_float_precision","fragqment_shader_medium_float_precision_range_min","fragqment_shader_medium_float_precision_range_max","fragqment_shader_low_float_precision","fragqment_shader_low_float_precisio","fragqment_shader_low_float_precisio","vertex_shader_high_int_precision","vertqex_shader_high_int_precision_range_min","vertqex_shader_high_int_precision_range_max","vertqex_shader_medium_int_precision","vertqex_shader_medium_int_precision_range_min","vertqex_shader_medium_int_precision_range_max","vertex_shader_low_int_precision","vertqex_shader_low_int_precision_range_min","vertqex_shader_low_int_precision_range_max","fragqment_shader_high_int_precision","fragqment_shader_high_int_precision_range_min","fragqment_shader_high_int_precision_range_max","fragqment_shader_medium_int_precision","fragqment_shader_medium_int_precision_range_min","fragqment_shader_medium_int_precision_range_max","fragment_shader_low_int_precision","fragqment_shader_low_int_precision_range_min","fragqment_shader_low_int_precision_range_mbax","unmasked_vendor","unmasked_renderer","error","img","web_gl","get_parameter_name","get_parameter_native","web_gl_meta","max_touch_points","max_touch_points","max_touch_points","touch_event ","touch_event","touch_start","touch","ogg","h4","webm","video","ogg","mp3","wav","m4a","audio","vendor","product","product_sub","ie","load_native_times","app","chrome","webdriver","has_chrome_object","connection_rtt","browser","history_length","hardware_concurrency","iframe","battery","console_debug_name","console_debug_native","has_phantom_underscore","has_phantom_call","non_native_functions","persistent","temporary","supported_entry_types","performance_observer","window","protocol","location","fonts_array","document_element","head","scripts","puppeteer_stealth_to_string_proxy ","puppeteer_stealth_web_gl_vendor_evasion","json_stringify_snippet","environment","lower","higher","window_long_properties","contextmenuon","window_last_items","width","height","scale","visual_viewport","version" };
            int i = 0;

            while (m.Success)
            {
                var token = m.Groups[2].Value == "" ? m.Groups[3].Value : m.Groups[2].Value;

                try
                {
                    //var newStr = "";
                    //var bb = Convert.FromBase64String(token);

                    ////Console.Write(++i + " " + token + " ");
                    //foreach (var b in bb) {
                    //    newStr += (char)b;
                    //}


                    //string clean_before = Regex.Replace(newStr, @"[^a-z0-9\.\-_!@#]+", "");
                    //string clean = Regex.Replace(newStr, @"[^a-z0-9\.\-_!@#]+", "");
                    //if (clean.IndexOf("q") != -1)
                    //{
                    //    var part = clean.Substring(0, clean.IndexOf("q"));
                    //    var first = clean.Substring(clean.IndexOf("q") + 1);
                    //    var clean1 = first + part;
                    //    Console.Write(clean1 + " ");
                    //}
                    //Console.WriteLine(clean.Replace("q", ""));
                    
                    var key = tokens[i++];
                    if (!signs.ContainsKey(key))
                        signs.Add(key, token);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                m = m.NextMatch();
            }
            return signs;
        }

        private static string ReplaceSigns(string jsonFP, bool is_encode)
        {
            if (is_encode)
            {
                foreach (KeyValuePair<string, string> s in signs)
                    jsonFP = jsonFP.Replace("\"" + s.Key + "\"", "\"" + s.Value + "\"");
            }
            else
            {
                foreach (KeyValuePair<string, string> s in signs)
                    jsonFP = jsonFP.Replace("\"" + s.Value + "\"", "\"" + s.Key + "\"");
            }

            var objFinish = JsonConvert.DeserializeObject(jsonFP);
            return JsonConvert.SerializeObject(objFinish);
        }
        private async static Task ParseJSON(IDictionary<string, string> map, dynamic objFinish, bool is_encode)
        {
            try
            {
                foreach (var child in objFinish)
                {
                    try
                    {
                        if (child.GetType().GetProperty("Name") != null)
                        {
                            var name = child.Name;
                            string sign = "";
                            if(is_encode)
                                sign = signs.FirstOrDefault(x => x.Value == name).Key;
                            else
                                sign = signs.FirstOrDefault(x => x.Key == name).Key;
                            var value = child.Value;
                            Type type = child.Value?.GetType();
                            string[] arrFunc = { };

                            if (type.Name == "JValue")
                            {
                                value = (string)child.Value;


                                if (is_encode && !map.ContainsKey(name))
                                {
                                    Console.WriteLine(name);
                                    continue;
                                }
                                else
                                {
                                    try
                                    {
                                        Convert.FromBase64String(value);
                                    }
                                    catch { continue; }
                                }

                                if(sign == "img")
                                    Console.WriteLine("");
                                else
                                {
                                    Dictionary<string, string> r = new Dictionary<string, string>();
                                    if (is_encode)
                                    {
                                        List<string> lst = ((string[])map[name].Trim().Split("\n")).ToList();
                                        arrFunc = funcSelect(lst);
                                        r = await encode(arrFunc, "\"" + value + "\"", name);
                                        var v = r.First().Value;
                                        szDecoded = v != null ? szDecoded.Replace(value, v) : szDecoded;
                                    }
                                    else
                                    {
                                        if (map.ContainsKey(name))
                                        {
                                            if(name == "screen" || name == "canvas" || name == "touch" || name == "audio" || name == "browser" || name == "environment" || name == "window_last_items")
                                            {
                                                Console.WriteLine(name);
                                            }
                                            var reverseFuncs = GetReverseFunc(map[name].Trim().Split("\n"));
                                            arrFunc = funcSelect(reverseFuncs);

                                            r = await decode(arrFunc, "\"" + value + "\"", name);
                                            var v = r.First().Value;
                                            v = ReplaceSigns(v, is_encode);
                                            szDecoded = v != null ? szDecoded.Replace(value, v) : szDecoded;
                                        }
                                        else
                                        {
                                            Console.WriteLine();
                                            //szDecoded = v != null ? szDecoded.Replace(value, v) : szDecoded;
                                        }
                                    }

                                }
                            }
                            else if(type.Name == "JObject")
                            {

                                if (is_encode && !map.ContainsKey(name))
                                {
                                    Console.WriteLine(name);
                                    continue;
                                }
                                Console.WriteLine(name);
                                if (sign == "canvas" || sign == "web_gl")
                                    Console.WriteLine(sign);

                                await ParseJSON(map, child.Value, is_encode);
                                value = JsonConvert.SerializeObject(child.Value);

                                Dictionary<string, string> r = new Dictionary<string, string>();
                                if (is_encode)
                                {
                                    List<string> lst = ((string[])map[name].Trim().Split("\n")).ToList();
                                    arrFunc = funcSelect(lst);
                                    r = await encode(arrFunc, value, name);
                                }
                                else
                                {
                                    if (map.ContainsKey(name))
                                    {
                                        var reverseFuncs = GetReverseFunc(map[name].Trim().Split("\n"));
                                        arrFunc = funcSelect(reverseFuncs);
                                        if (arrFunc.Length > 1)
                                            r = await decode(arrFunc, value, name);
                                    }
                                }

                                var v = r.FirstOrDefault().Value;
                                
                                szDecoded = v != null ? szDecoded.Replace(value, "\""+v+"\"") : szDecoded;
                            }
                            else if (type.Name == "JArray")
                            {
                                Console.WriteLine(name);
                                if (sign == "long_window_properties")
                                    Console.WriteLine(sign);

                                Console.WriteLine(name);
                                await ParseJSON(map, child.Value, is_encode);
                            }
                            else
                            {
                                Console.WriteLine(name);
                                await ParseJSON(map, child.Value, is_encode);
                            }

                        }
                        else
                        {
                            if (child.GetType().GetProperty("Value") != null)
                                Console.WriteLine(child.Value);
                            else
                            {
                                Console.WriteLine("Here!");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //decodeField(child.Name);
                        Console.WriteLine(ex.Message);
                    }
                }
                
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private static void decodeFields(dynamic objFinish)
        {
            string w = "";
            string r = "";

            foreach (var child in objFinish)
            {
                string s = GetAtoB(child.Name);

                while (true)
                {
                    if(s.Length < 2)
                    {
                        w += s;
                        break;
                    }
                    r = s.Substring(s.Length - 2);
                    s = s.Substring(0, s.Length - 2);
                    w += r;
                    if (string.IsNullOrEmpty(s))
                        break;
                }

                Console.WriteLine(w + " - "+ child.Name);
                w = "";
            }
        }
        private static string decodeField(string name)
        {
            string w = "";
            string r = "";


            string s = GetAtoB(name);

            while (true)
            {
                if (s.Length < 2)
                {
                    w += s;
                    break;
                }
                r = s.Substring(s.Length - 2);
                s = s.Substring(0, s.Length - 2);
                w += r;
                if (string.IsNullOrEmpty(s))
                    break;
            }
            return w;
        }
        private static string[] keyToUp(string[] lFunc)
        {
            while (true)
            {
                var s = lFunc[0];
                if (s.Contains("KEY"))
                {
                    lFunc = lFunc.Skip(1).ToArray();
                    lFunc = lFunc.Append(s).ToArray();
                }
                else
                    break;
            }
            return lFunc;
        }
        private static Dictionary<string, string> GetRegExp(string content, bool is_encode)
        {
            Dictionary<string, string> map = new Dictionary<string, string>();

            string[] internals = { "ct", "video", "audio" };
            string value = string.Empty;
            string key = string.Empty;

            foreach (var i in internals)
            {
                string pattern = @"(\w+ = KEY\(\d+, \d+\);)(\s+\w+\['startInternal'\]\('###INTERVAL###'\);)(?s)(.*?)(\s+\w+\['stopInternal'\]\('###INTERVAL###'\);(.*?)(var \w+ = window\.btoa\(\w+\.join\(""""\)\);\s+\w+\.([a-zA-Z0-9=\/\\\+]+) = \w+|\w+\[""([a-zA-Z0-9=\/\\\+]+)""\] = \w+);)";
                pattern = pattern.Replace("###INTERVAL###", i);
                var m = Regex.Match(content, pattern);

                if (m.Success)
                {
                    var token = m.Groups[1].Value;
                    value = token + m.Groups[4].Value;
                    key = m.Groups[7].Value == "" ? m.Groups[8].Value : m.Groups[7].Value;

                    var sign = signs.FirstOrDefault(x => x.Value == key).Key;
                    if(sign == null)
                        Console.WriteLine(sign + ": " + key);
                    Console.WriteLine(sign + ": " + key);

                    if (is_encode)
                    {
                        if (!map.ContainsKey(key))
                            map.Add(key, value);
                    }
                    else
                    {
                        if (!map.ContainsKey(sign))
                            map.Add(sign, value);
                    }
                }
            }

            string[] internals3 = { "webgl_meta", "canvas_fonts"};
            value = string.Empty;
            key = string.Empty;

            foreach (var i in internals3)
            {
                string pattern = @"(\w+\['startInternal'\]\('###INTERVAL###'\);)(?s)(.*?)(\s+\w+\['stopInternal'\]\('###INTERVAL###'\);(.*?)(var \w+ = window\.btoa\(\w+\.join\(""""\)\);\s+\w+\.([a-zA-Z0-9=\/\\\+]+) = \w+|\w+\[""([a-zA-Z0-9=\/\\\+]+)""\] = \w+);)";
                pattern = pattern.Replace("###INTERVAL###", i);
                var m = Regex.Match(content, pattern);

                if(m.Success)
                {
                    value = m.Groups[4].Value;
                    key = m.Groups[6].Value == "" ? m.Groups[7].Value : m.Groups[6].Value;

                    var sign = signs.FirstOrDefault(x => x.Value == key).Key;
                    if (sign == null)
                        Console.WriteLine(sign + ": " + key);
                    Console.WriteLine(sign + ": " + key);

                    if (is_encode)
                    {
                        if (!map.ContainsKey(key))
                            map.Add(key, value);
                    }
                    else
                    {
                        if (sign != null && !map.ContainsKey(sign) )
                            map.Add(sign, value);
                    }
                }
            }

            string[] internals2 = { "canvas_io", "canvas_o", "prop_o", "webgl_io", "webgl_o",  };

            foreach (var i in internals2)
            {
                string pattern = @"(\w+\['startInternal'\]\('###INTERVAL###'\);)(?s)(.*?)(\w+\[([\w'\""]+)\] = \w+;|\w+\.(\w+) = \w+;)(\s+\}\s+)?(\s+\w+\['stopInternal'\]\('###INTERVAL###'\);)";
                pattern = pattern.Replace("###INTERVAL###", i);
                var m = Regex.Match(content, pattern);

                if (m.Success)
                {
                    value = m.Groups[2].Value;
                    content = "\n" + content.Replace(value, i + "();\n");
                    key = m.Groups[4].Value == "" ? m.Groups[5].Value.Trim(new Char[] { '"', '\'' }) : m.Groups[4].Value.Trim(new Char[] { '"', '\'' });
                    var sign = signs.FirstOrDefault(x => x.Value == key).Key;
                    if (sign == null)
                    {
                        sign = i;
                    }
                    Console.WriteLine(sign + ": " + key);
                    if (key == signs["img"] && i.StartsWith("canvas_"))
                        key = "canvas_" + key;
                    else if (sign == signs["img"] && i.StartsWith("webgl_"))
                        key = "webgl_" + key;

                    if (i == "prop_o")
                    {
                        if (is_encode)
                        {
                            if (!map.ContainsKey(key))
                                map.Add(key, value);
                        }
                        else
                        {
                            if (sign != null && !map.ContainsKey(sign))
                                map.Add(sign, value);
                        }
                    }
                    else
                    {
                        if (is_encode)
                        {
                            if (!map.ContainsKey(key))
                                map.Add(key, value);
                        }
                        else
                        {
                            if (sign != null &&  !map.ContainsKey(sign))
                                map.Add(sign, value);
                        }
                    }
                }
            }

            //(\w+ = KEY\(\d+, \d+\);)(?s)(.*?)(\s+var \w+ = window\.btoa\(\w+\.join\(""""\)\))
             string p = @"(\w+ = KEY\(\d+, \d+\);)(?s)(.*?)(\w+ = stringify\(['\(\)a-zA-Z0-9\[\]]+\);\s+var \w+ = \w+\.replace\(\w+, \w+\);)(?s)(.*?)(\s+var \w+ = window\.btoa\(\w+\.join\(""""\)\);(\s+\w+\.([a-zA-Z0-9=\/\\\+]+) = \w+|\s+\w+\[""([a-zA-Z0-9=\/\\\+]+)""\] = \w+));";
            //string p = @"(\w+ = KEY\(\d+, \d+\);)(?s)(.*?)(\s+var \w+ = window\.btoa\(\w+\.join\(""""\)\);(\s+\w+\.([a-zA-Z0-9=\/\\\+]+) = \w+|\s+\w+\[""([a-zA-Z0-9=\/\\\+]+)""\] = \w+));";
            var mm = Regex.Match(content, p);
            while(mm.Success)
            {
                var token = mm.Groups[1].Value;
                value = token + mm.Groups[4].Value;
                key = mm.Groups[7].Value == "" ? mm.Groups[8].Value : mm.Groups[7].Value;
                var sign = signs.FirstOrDefault(x => x.Value == key).Key;
                Console.WriteLine(sign + ": " + key);

                if (is_encode)
                {
                    if (!map.ContainsKey(key))
                        map.Add(key, value);
                }
                else
                {
                    if (sign != null && !map.ContainsKey(sign))
                        map.Add(sign, value);
                }
                mm = mm.NextMatch();
            }

            p = @"(\w+\[""([a-zA-Z0-9=\/\\\+_\""]+)""\] = \(function\(\) \{)(?s)(.*?)(var \w+ = window\.btoa\(\w+\.join\(""""\)\);\s+return \w+;)";
            mm = Regex.Match(content, p);
            while (mm.Success)
            {
                key = mm.Groups[2].Value;
                var sign = signs.FirstOrDefault(x => x.Value == key).Key;
                Console.WriteLine(sign + ": " + key);
                value = mm.Groups[3].Value;
                if (is_encode)
                {
                    if (!map.ContainsKey(key))
                        map.Add(key, value);
                }
                else
                {
                    if (sign != null && !map.ContainsKey(sign))
                        map.Add(sign, value);
                }
                mm = mm.NextMatch();
            }

            return map;
        }
        private static Dictionary<string, string> DecodeOnStart(Dictionary<string, string> bigStrings, Dictionary<int, string> dFuncsStart)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();
            int i = 0;

            foreach(KeyValuePair<string, string> s in bigStrings)
            {
                var arrFuncs = dFuncsStart[i].Split(";");
                var realStr = GetAtoB(s.Value);
                var list = ToCharCode(realStr);

                foreach (string f in arrFuncs)
                {
                    if (string.IsNullOrEmpty(f))
                        continue;

                    if (f.Contains("SHAKE"))
                        list = Shake(list);
                    else if (f.Contains("REM"))
                    {
                        var m = Regex.Match(f, @"REM\(([0-9\, ]+)\)");
                        if (m.Success)
                        {
                            var k = Convert.ToInt32(m.Groups[1].Value);
                            list = GetRem(list, k);
                        }
                    }
                    else if (f.Contains("244"))
                        list = Get244(list);
                    else if (f.Contains("REVMIN_U"))
                        list = RevMin(list);
                    else if (f.Contains("XOR_S"))
                    {
                        var m = Regex.Match(f, @"XOR_S\(([0-9\, ]+)\)");
                        if (m.Success) 
                        {
                            var key = m.Groups[1].Value.Split(',').Select(s => Convert.ToInt64(s)).ToArray();
                            list = GetXor(list, key);
                        }
                    }
                    else
                        Console.WriteLine("Hey! Where is this function?! " + f);
                }
                var newStr = BytesToString(list.ToList());
                results.Add(s.Key, newStr);
                i++;
            }
            return results;
        }
        private static Dictionary<string, string> GetStringsOnStart(string content)
        {
            string pattern = @"\s+var (\w+) = ""([a-zA-Z0-9=\/\\\+]+)"";\s+var \w+ = window\.atob\(\w+\);";
            var matches = Regex.Matches(content, pattern).Cast<Match>().ToArray();

            Dictionary<string, string> vs = new Dictionary<string, string>();

            int i = 0;
            foreach (var match in matches)
            {
                vs.Add(match.Groups[1].Value, match.Groups[2].Value);
                i++;
            }
            return vs;
        }
        private async static Task<Dictionary<string, string>> GetFuncsOnStart(string content)
        {
            //return (pz + lW) % 4294967296;
            //const string parrentAlgo = @"\w+ = ""[a-zA-Z0-9=\/\\\+]+"";(?s).*(\s+var \w+ = window\[\w+\.substr\(\d+, \d+\)\]);";
            const string parrentAlgo = @"\w+ = ""[a-zA-Z0-9=\/\\\+]+"";(?s).*(\s+var \w+ = new window\.RegExp\(""\[\\\\u007F\-\\\\uFFFF\]""\, ""g""\);)";
            var match = Regex.Match(content, parrentAlgo);

            if(match.Success)
            {
                var LFuncs = funcSelect(match.Value.Split("\n").ToList());

                return await encode(LFuncs, null, null);
            }

            return null;
        }

        private async static Task<Dictionary<string, string>> encode(string[] arrFuncs, string value, string valueName)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();
            Dictionary<string, long[]> values = new Dictionary<string, long[]>();
            Dictionary<string, long[]> dKeys = new Dictionary<string, long[]>();

            string funcName = "";
            string funcArg = "";
            string funcValue = "";

            string result = "";
            string[] arrArgs = { };
            long[] arrKey = { };
            long[] list = { };
            string v = "";
            bool bJoinFounded = false;
            bool bDecoded = false;
            bool bFirst = false;
            int iTake = 0;

            Regex rgxValue = new Regex(@"(\w+) = ([a-zA-Z0-9=\/\\\+_""]+);");
            Regex rgxFunc = new Regex(@"(\w+) = ([\w+.]+)\(([a-zA-Z0-9,""_ ]+)\);");
            Regex rgxJoin = new Regex(@"(\w+) = (\w+)\.join\(""""\);");

            if(value != null)
            {
                list = ToCharCode(value);
                values = ChangeKey(values, valueName, value, list);
                bFirst = true;
            }
            try
            {
                foreach (var f in arrFuncs)
                {
                    if (f == "Pg = KEY(187585459, 28);")
                    {
                        Console.WriteLine();
                    }

                    if (bDecoded)
                        break;

                    var matchValue = rgxValue.Match(f);
                    var matchFunc = rgxFunc.Match(f);
                    var matchJoin = rgxJoin.Match(f);

                    var param1 = "";
                    var param2 = "";
                    var param3 = "";
                    var param4 = "";

                    if (matchValue.Success)
                    {
                         value = matchValue.Groups[2].Value.Trim('"').Trim();
                         valueName = matchValue.Groups[1].Value.Trim();

                        if (bJoinFounded)
                        {
                            v = results[value];
                            results.Remove(value);
                            results[valueName] = v;
                            bJoinFounded = false;
                            continue;
                        }

                        if (value.Length > 3 && value != "undefined")
                        {
                            result = GetAtoB(value);
                            list = ToCharCode(result);
                            values = ChangeKey(values, valueName, value, list);
                        }
                        else
                        {
                            if(values.Keys.Contains(value))
                                values = ChangeKey(values, valueName, value, values[value]);
                            if (bFirst)
                            {
                                values = ChangeKey(values, valueName, value, values.Values.FirstOrDefault());
                                bFirst = false;
                            }
                        }
                    }
                    else if (matchFunc.Success)
                    {
                        funcName = matchFunc.Groups[2].Value.Trim();
                        funcArg = matchFunc.Groups[3].Value.Trim();
                        funcValue = matchFunc.Groups[1].Value.Trim();

                        switch (funcName)
                        {
                            case "KEY":
                                arrArgs = funcArg.Split(',');//encode
                                param1 = arrArgs[0];
                                param2 = arrArgs[1];
                                arrKey = await UpdateKey(token.ToString(), param1, Convert.ToInt32(param2));
                                dKeys.Add(funcValue, arrKey);
                                break;
                            case "window.atob":
                                values = ChangeKey(values, funcValue, funcArg, values[funcArg]);
                                break;
                            case string a when a.Contains("tocharcode_u"):
                                values = ChangeKey(values, funcValue, funcArg, values[funcArg]);
                                break;
                            case string a when a.Contains("tocharcode_e"):
                                values = ChangeKey(values, funcValue, funcArg, values[funcArg]);
                                break;
                            case string a when a.Contains("tocharcode_p"):
                                values = ChangeKey(values, funcArg, funcValue, list);
                                break;
                            case "REVMIN_U":
                                list = RevMin(values[funcArg]);
                                values = ChangeKey(values, funcValue, funcArg, list);
                                break;
                            case string a when a.Contains("REM"):
                                arrArgs = funcArg.Split(',');

                                param1 = arrArgs[0];
                                param2 = arrArgs[1];

                                if (funcName == "REM_A")
                                {
                                    param3 = arrArgs[2];
                                    var key = (int)dKeys[param2][Convert.ToInt32(param3)];

                                    list = GetRemA(values[param1], key);
                                    values = ChangeKey(values, funcValue, param1, list);
                                }
                                else
                                {
                                    list = GetRem(values[param1], Convert.ToInt32(param2));
                                    values = ChangeKey(values, funcValue, param1, list);
                                }
                                break;
                            case "fromcharcode_u":
                                var s = BytesToString(values[funcArg].ToList());
                                results.Add(funcValue, s);
                                break;
                            case "fromcharcode_n":
                                var ss = BytesToString(values[funcArg].ToList());
                                results.Add(funcValue, ss);
                                break;
                            case "fromcharcode":
                                results.Add(funcValue, GetBToA(values[valueName]));

                                break;
                            case string a when a.Contains("XOR"):
                                Regex rgxXorU = new Regex(@"(\w+)\, ""([0-9, ]+)""");
                                var mXorU = rgxXorU.Match(funcArg);
                                if (mXorU.Success)
                                {
                                    param1 = mXorU.Groups[1].Value;
                                    param2 = mXorU.Groups[2].Value;
                                    list = GetXor(values[param1], param2.Split(',').Select(c => Convert.ToInt64(c)).ToArray());
                                    values = ChangeKey(values, funcValue, param1, list);
                                }
                                else
                                {
                                    rgxXorU = new Regex(@"(\w+)\, (\w+)\, ([0-9, ]+)");
                                    mXorU = rgxXorU.Match(funcArg);//
                                    if (mXorU.Success)
                                    {
                                        param1 = mXorU.Groups[1].Value;
                                        param2 = mXorU.Groups[2].Value;
                                        var pp = mXorU.Groups[3].Value.Split(',').Select(c => Convert.ToInt32(c)).ToArray();

                                        iTake = pp[1] - pp[0];
                                        list = GetXor(values[param2], dKeys[param1].Skip(pp[0]).Take(iTake).ToArray());
                                        values = ChangeKey(values, funcValue, param2, list);
                                    }
                                }
                                break;
                            case "DPUSH":
                                arrArgs = funcArg.Split(',');
                                param1 = arrArgs[0].Trim();
                                param2 = arrArgs[1].Trim();
                                param3 = arrArgs[2].Trim();
                                param4 = arrArgs[3].Trim();

                                iTake = Convert.ToInt32(param3) - Convert.ToInt32(param2);
                                list = PushWithKey(values[param4], dKeys[param1].Skip(Convert.ToInt32(param2)).Take(iTake).ToArray());
                                values = ChangeKey(values, funcValue, param4, list);
                                break;
                            case "244_U":
                                list = Get244(values[funcArg]);
                                values = ChangeKey(values, funcValue, funcArg, list);
                                break;
                            case string a when a.Contains("SHAKE"):
                                list = Shake(values[funcArg]);
                                values = ChangeKey(values, funcValue, funcArg, list);
                                break;
                            case "MIN":
                                list = RevMin(values[funcArg]);
                                values = ChangeKey(values, funcValue, funcArg, list);
                                break;
                            case string a when a.Contains("NOP"):
                                values = ChangeKey(values, funcValue, funcArg, values[funcArg]);
                                break;
                            case string a when a.Contains("replace"):
                                arrArgs = funcArg.Split(',');
                                param1 = arrArgs[0];
                                param2 = arrArgs[1];
                                Regex r1 = new Regex(@"[\\u007F-\\uFFFF]");
                                Regex r2 = new Regex(@"");
                                //Regex.Replace(r1,"");
                                values = ChangeKey(values, funcValue, funcArg, null);
                                break;
                            default:
                                if (!funcName.Contains("join") && !funcName.Contains("stringify"))
                                    Console.WriteLine("Case not exists! " + f);
                                break;
                        }
                    }
                    if (matchJoin.Success)
                    {
                        value = matchJoin.Groups[2].Value.Trim('"');
                        valueName = matchJoin.Groups[1].Value;

                        v = results[value];
                        results.Remove(value);
                        results[valueName] = v;

                        bJoinFounded = true;
                    }
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return results;
        }

        private async static Task<Dictionary<string, string>> decode(string[] arrFuncs, string value, string valueName)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();
            Dictionary<string, long[]> values = new Dictionary<string, long[]>();
            Dictionary<string, long[]> dKeys = new Dictionary<string, long[]>();

            string funcName = "";
            string funcArg = "";
            string funcValue = "";

            string result = "";
            string[] arrArgs = { };
            long[] arrKey = { };
            long[] list = { };
            string v = "";
            bool bJoinFounded = false;
            bool bDecoded = false;
            bool bFirst = false;
            int iTake = 0;

            Regex rgxValue = new Regex(@"(\w+) = ([a-zA-Z0-9=\/\\\+_""]+);");
            Regex rgxFunc = new Regex(@"(\w+) = ([\w+.]+)\(([a-zA-Z0-9,""_ ]+)\);");
            Regex rgxJoin = new Regex(@"(\w+) = (\w+)\.join\(""""\);");

            if (value != null)
            {
                result = GetAtoB(value.Trim('\"'));
                list = ToCharCode(result);
                values = ChangeKey(values, valueName, value, list);
                bFirst = true;
            }
            try
            {
                foreach (var f in arrFuncs)
                {
                    if (f == "Pg = KEY(187585459, 28);")
                    {
                        Console.WriteLine();
                    }

                    if (bDecoded)
                        break;

                    var matchValue = rgxValue.Match(f);
                    var matchFunc = rgxFunc.Match(f);
                    var matchJoin = rgxJoin.Match(f);

                    var param1 = "";
                    var param2 = "";
                    var param3 = "";
                    var param4 = "";

                    if (matchValue.Success)
                    {
                        value = matchValue.Groups[1].Value.Trim('"').Trim();
                        valueName = matchValue.Groups[2].Value.Trim();

                        if (bJoinFounded)
                        {
                            v = results[value];
                            results.Remove(value);
                            results[valueName] = v;
                            bJoinFounded = false;
                            continue;
                        }

                        if (value.Length > 3 && value != "undefined")
                        {
                            result = GetAtoB(value);
                            list = ToCharCode(result);
                            values = ChangeKey(values, valueName, value, list);
                        }
                        else
                        {
                            if (values.Keys.Contains(value))
                                values = ChangeKey(values, valueName, value, values[value]);
                            if (bFirst)
                            {
                                values = ChangeKey(values, valueName, value, values.Values.FirstOrDefault());
                                bFirst = false;
                            }
                        }
                    }
                    else if (matchFunc.Success)
                    {
                        funcName = matchFunc.Groups[2].Value.Trim();
                        funcArg = matchFunc.Groups[3].Value.Trim();
                        funcValue = matchFunc.Groups[1].Value.Trim();

                        Console.WriteLine(funcName);
                        switch (funcName)
                        {
                            case "KEY":
                                arrArgs = funcArg.Split(',');//encode
                                var t = arrArgs[0];
                                var size = arrArgs[1];
                                arrKey = await UpdateKey(token.ToString(), t, Convert.ToInt32(size));
                                dKeys.Add(funcValue, arrKey);
                                break;
                            case "window.btoa":
                                result = GetAtoB(value);
                                list = ToCharCode(result);
                                values = ChangeKey(values, valueName, value, list);
                                break;
                            case string a when a.Contains("tocharcode"):
                                var s = BytesToString(values[funcArg].ToList());
                                results.Add(funcArg, s);
                                break;
                            case "fromcharcode":
                                if (bFirst)
                                    values = ChangeKey(values, funcValue, funcArg, values.FirstOrDefault().Value);                               
                                break;
                            case "REVMIN_U":
                                list = RevMin(values[funcArg]);
                                values = ChangeKey(values, funcValue, funcArg, list);
                                break;
                            case string a when a.Contains("REM"):
                                arrArgs = funcArg.Split(',');

                                param1 = arrArgs[0];
                                param2 = arrArgs[1];

                                if (funcName == "REM_A")
                                {
                                    param3 = arrArgs[2];
                                    var key = (int)dKeys[param2][Convert.ToInt32(param3)];

                                    list = GetRemA(values[funcValue], key);
                                    values = ChangeKey(values, param1, funcValue, list);
                                }
                                else
                                {
                                    list = GetRem(values[funcValue], Convert.ToInt32(param2));
                                    values = ChangeKey(values, param1, funcValue, list);
                                }
                                break;

                            case string a when a.Contains("XOR"):
                                Regex rgxXorU = new Regex(@"(\w+)\, ""([0-9, ]+)""");
                                var mXorU = rgxXorU.Match(funcArg);
                                if (mXorU.Success)
                                {
                                    param1 = mXorU.Groups[1].Value;
                                    param2 = mXorU.Groups[2].Value;
                                    list = GetXor(values[param1], param2.Split(',').Select(c => Convert.ToInt64(c)).ToArray());
                                    values = ChangeKey(values, funcValue, param1, list);
                                }
                                else
                                {
                                    rgxXorU = new Regex(@"(\w+)\, (\w+)\, ([0-9, ]+)");
                                    mXorU = rgxXorU.Match(funcArg);//
                                    if (mXorU.Success)
                                    {
                                        param1 = mXorU.Groups[1].Value;
                                        param2 = mXorU.Groups[2].Value;
                                        var pp = mXorU.Groups[3].Value.Split(',').Select(c => Convert.ToInt32(c)).ToArray();

                                        iTake = pp[1] - pp[0];
                                        list = GetXor(values[funcValue], dKeys[param1].Skip(pp[0]).Take(iTake).ToArray());
                                        values = ChangeKey(values, param2, funcValue, list);
                                    }
                                }
                                break;
                            case "DPUSH":
                                arrArgs = funcArg.Split(',');
                                param1 = arrArgs[0].Trim();
                                param2 = arrArgs[1].Trim();
                                param3 = arrArgs[2].Trim();
                                param4 = arrArgs[3].Trim();

                                iTake = Convert.ToInt32(param3) - Convert.ToInt32(param2);
                                //list = PushWithKey(values[funcValue], dKeys[param1].Skip(Convert.ToInt32(param2)).Take(iTake).ToArray());
                                list = UnPush(values[funcValue]);
                                values = ChangeKey(values, param4, funcValue, list);
                                break;
                            case "244_U":
                                list = Get244(values[funcValue]);
                                values = ChangeKey(values, funcArg, funcValue, list);
                                break;
                            case string a when a.Contains("SHAKE"):
                                list = Shake(values[funcValue]);
                                values = ChangeKey(values, funcArg, funcValue, list);
                                break;
                            case "MIN":
                                list = RevMin(values[funcValue]);
                                values = ChangeKey(values, funcArg, funcValue, list);
                                break;
                            case string a when a.Contains("NOP"):
                                values = ChangeKey(values, funcArg, funcValue, values[funcValue]);
                                break;
                            case string a when a.Contains("replace"):
                                arrArgs = funcArg.Split(',');
                                param1 = arrArgs[0];
                                param2 = arrArgs[1];
                                Regex r1 = new Regex(@"[\\u007F-\\uFFFF]");
                                Regex r2 = new Regex(@"");
                                //Regex.Replace(r1,"");
                                values = ChangeKey(values, funcValue, funcArg, null);
                                break;
                            default:
                                if (!funcName.Contains("join") && !funcName.Contains("stringify"))
                                    Console.WriteLine("Case not exists! " + f);
                                break;
                        }
                    }
                    if (matchJoin.Success)
                    {
                        value = matchJoin.Groups[2].Value.Trim('"');
                        valueName = matchJoin.Groups[1].Value;

                        v = results[value];
                        results.Remove(value);
                        results[valueName] = v;

                        bJoinFounded = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return results;
        }
        private static string[] funcSelect(List<string> arrFuncs)
        {
            Regex rgx = new Regex(@"\s+\w+ = [a-zA-Z0-9""=\\\/\+_]{2,};");

            var LFuncs = arrFuncs.Select((s, i) => new { i, s }).Where(t =>
                    (rgx.Match(t.s).Success
                        || !t.s.StartsWith("\n")
                        || t.s.Contains("window.atob")
                        || t.s.Contains(".join")
                        || t.s.Contains(".replace")
                    )).Select(t => t.s.Replace("\n", "").Replace("  ", "")).ToArray();

            return LFuncs;
        }

        private static Dictionary<string, long[]> ChangeKey(Dictionary<string, long[]> values, string newKey, string oldKey, long[] value)
        {
            if (values.ContainsKey(oldKey))
            {
                if (value == null)
                    value = values[oldKey];
                values.Remove(oldKey);
                values[newKey] = value;
            }
            else
            {
                if (!values.ContainsKey(newKey))
                    values.Add(newKey, value);
                else
                    values[newKey] = value;
            }
            return values;
        }
        private static Dictionary<int, string> GetFuncs(string content)
        {
            const string parentAlgo = @"(?s)(?<=tocharcode_u\(\);(?s)).*?(?=fromcharcode_u)";
            var matches = Regex.Matches(content, parentAlgo).Cast<Match>().ToArray();
            Dictionary<int, string> dFuncs = new Dictionary<int, string>();

            int i = 0;
            foreach (var match in matches)
            {
                var value = match.Value;
                var arrFuncs = value.Split("\n");
                var LFuncs = arrFuncs.Select((s, i) => new { i, s }).Where(t => (t.i != 1 && t.i != 0 && t.i != arrFuncs.Length - 1 && !t.s.Contains("NOP") && !t.s.Contains("var "))).Select(t => t.s).ToArray().Reverse();
                var r = string.Join("", LFuncs);
                dFuncs.Add(i, r);
                i++;
            }

            return dFuncs;
        }
        private static void GetFpProperties(string content)
        {
            var m = Regex.Match(content, @"(\w+)\[""[a-zA-Z0-9=\/\\\+]+""\] = ""[a-zA-Z0-9=\/\\\+]+"";");
            if (m.Success)
            {
                var ReeseName = m.Groups[1].Value;
                var mm = Regex.Matches(content, @ReeseName + @"\[""([a-zA-Z0-9=\/\\\+\-]+)""\]");
            }
        }
        //private static Dictionary<int, string> GetTokens(string content)
        //{
        //    const string patternCode =
        //    @"\s+var \w+ = \w+\((\d+), \w+\);
        //    \s+var \w+ = \[\];
        //    \s+var \w+ = 0;
        //    \s+while \(\w+ < (\d+)\)";

        //    Dictionary<int, string> dCodes = new Dictionary<int, string>();
        //    var matches = Regex.Matches(content, patternCode).Cast<Match>().ToArray();

        //    int i = 0;
        //    foreach (var match in matches)
        //    {
        //        var value = match.Value;
        //        var MatchCode = Regex.Match(value, patternCode);

        //        var Code = MatchCode.Groups[1].Value;
        //        var Length = MatchCode.Groups[2].Value;

        //        dCodes.Add(i, Code + ":" + Length);
        //        //AddCodeToDict(i,Code + ":" + Length, dCodes);
        //        i++;
        //    }

        //    return dCodes;
        //}

        //private static bool GetFuncsForSign(string content)
        //{
        //    Dictionary<int, string[]> dFuncs = new Dictionary<int, string[]>();
        //    var m = Regex.Match(content, @"(var \w+ = \w+;)?\s+var \w+ = \w+;\s+\w+ = stringify\(\w+\);\s+[a-zA-Z0-9_\[\]\(\)\,\. \+\s\;\=]+\s+\w+ = fromcharcode\(\w+\);\s+var \w+ = window\.btoa\(\w+.join\(""""\)\);");

        //    int i = 0;
        //    while (m.Success)
        //    {
        //        var v = m.Groups[0].Value.Trim();
        //        var arrFunc = funcSelect(v.Split('\n').ToList());
        //        dFuncs.Add(i++, arrFunc);

        //        m = m.NextMatch();
        //    }
        //    return true; 

        //}

        private static string ReplaceInFile(string FileName)
        {
            string FileNameNew = FileName.Substring(0, FileName.IndexOf('.')) + "_new.js";

            var currentDir = @"D:\headless\Headless\";
            var path = currentDir + FileName;

            var content = File.ReadAllText(path);
            var beaty = GetBeaty(content);
            //var beaty = content;

            #region func
            Dictionary<string, string> reg = new Dictionary<string, string>();
            reg["NOP"] = @"var \w+ = \[\];\s+for \(var \w+ in \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+if \(\w+\.hasOwnProperty\(\w+\)\) {\s+(\w+)\.push\(\w+\);\s+}\s+}";

            reg["NOP_S"] = @"\s+for \(var \w+ in \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+if \(\w+\.hasOwnProperty\(\w+\)\) {\s+(\w+)\.push\(\w+\);\s+}\s+}";

            reg["244_U"] = @"\s+for \(var \w+ in \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+if \(\w+\.hasOwnProperty\(\w+\)\) {\s+var \w+ = \w+ << 4 & 240 \| \w+ >> 4;\s+(\w+).push\(\w+\);\s+}\s+}";
            reg["244"] = @"\s+for \(var \w+ in \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+if \(\w+.hasOwnProperty\(\w+\)\) {\s+var \w+ = \w+ << 4 & 240 \| \w+ >> 4;\s+(\w+).push\(\w+\);\s+}\s+}";

            //DPUSH KEY
            reg["DPUSH"] = @"\s+var \w+ = \w+\.length;\s+var \w+ = (\w+)\[\w+\.substr\(\d+, \d+\)\]\((\d+, \d+)\)\.length;\s+var \w+ = \[\];\s+var \w+ = 0;\s+while \(\w+ < \w+\) {\s+\w+\.push\((\w+)\[\w+\]\);\s+(\w+)\.push\(\w+\[\w+\.substr\(\d+, \d+\)\]\(\d+, \d+\)\[\w+ % \w+\]\);\s+\w+ \+= 1;\s+}";

            //XOR
            reg["XOR"] = @"\s+var \w+ = \w+\.length;\s+var \w+ = (\w+)\[\w+\.substr\(\d+, \d+\)\]\((\d+), (\d+)\)\.length;\s+var \w+ = \[\];\s+var \w+ = 0;\s+while \(\w+ < \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+var \w+ = \w+\[\w+\.substr\(\d+, \d+\)\]\(\d+, \d+\)\[\w+ % \w+\];\s+(\w+)\.push\(\w+ \^ \w+\);\s+\w+ \+= 1;\s+}";

            reg["XOR_S"] = @"\s+while \(\w+ < \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+var \w+ = \[([0-9\, ]+)\]\[\w+ \% \w+\];\s+(\w+)\.push\(\w+ \^ \w+\);\s+\w+ \+= 1;\s+}";

            reg["XOR_U"] = @"\s+while \(\w+ < \w+\) {\s+var \w+ = \[([0-9\, ]+)\]\[\w+ \% \w+\];\s+var \w+ = (\w+)\[\w+\];\s+(\w+)\.push\(\w+ \^ \w+\);\s+\w+ \+= 1;\s+}";
            //REMANDER
            reg["REM"] = @"\s+var \w+ = (\d+) \% \w+;\s+var \w+ = [\w+\[\]]+;\s+while \(\w+ < \w+\) {\s+(\w+)\.push\((\w+)\[\(\w+ \+ \w+ - \w+\) \% \w+\]\);\s+\w+ \+= 1;\s+}";

            reg["REM_S"] = @"\s+var \w+ = (\d+) \% \w+;\s+var \w+ = [\w+\[\]]+;\s+var \w+ = [\w+\[\]]+;\s+while \(\w+ < \w+\) {\s+(\w+)\.push\((\w+)\[\(\w+ \+ \w+ - \w+\) \% \w+\]\);\s+\w+ \+= 1;\s+}";
            reg["REM_U"] = @"\s+var \w+ = \w+\.length;\s+while \(\w+ < \w+\) {\s+var \w+ = \w+\[\w+\];\s+var \w+ = \[([0-9\, ]+)\]\[\w+ \% \w+\];\s+(\w+)\.push\(\w+ \^ \w+\);\s+\w+ \+= 1;\s+}";
            reg["REM_P"] = @"\s+var \w+ = (\d+) % \w+;\s+while \(\w+ < \w+\) {\s+(\w+)\.push\((\w+)\[\(\w+ \+ \w+ - \w+\) \% \w+\]\);\s+\w+ \+= 1;\s+}";
            reg["REM_A"] = @"\s+while \(\w+ < \w+\) {\s+(\w+)\.push\((\w+)\[\(\w+ \+ (\w+)\[(\d+)\]\) % \w+\]\);\s+\w+ \+= 1;\s+}";

            //SHAKE
            reg["SHAKE"] = @"\s+var \w+ = \w+\.length;\s+var \w+ = 0;\s+while \(\w+ \+ 1 < \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+\w+\[\w+\] = \w+\[\w+ \+ 1\];\s+(\w+)\[\w+ \+ 1\] = \w+;\s+\w+ \+= 2;\s+}";

            reg["SHAKE_S"] = @"\s+var \w+ = \w+\.length;\s+while \(\w+ \+ 1 < \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+\w+\[\w+\] = \w+\[\w+ \+ 1\];\s+(\w+)\[\w+ \+ 1\] = \w+;\s+\w+ \+= 2;\s+}";
            //MIN
            reg["MIN"] = @"\s+var \w+ = \w+\.length;\s+var \w+ = \[\];\s+var \w+ = \w+ - 1;\s+while \(\w+ >= 0\) {\s+(\w+)\.push\((\w+)\[\w+\]\);\s+\w+ -= 1;\s+}";

            //REVMIN
            reg["REVMIN_U"] =@"\s+while \(\w+ >= 0\) {\s+(\w+)\.push\((\w+)\[\w+\]\);\s+\w+ -= 1;\s+}";

            reg["REVMIN"] =@"\s+var \w+ = \w+\.length;\s+var \w+ = \[\];\s+var \w+ = \w+ - 1;\s+while \(\w+ >= 0\) {\s+(\w+)\.push\((\w+)\[\w+\]\);\s+\w+ -= 1;\s+}";

            reg["REVMIN_S"] =@"\s+var \w+ = \[\];\s+var \w+ = \w+;\s+var \w+ = \w+\.length;\s+var \w+ = \w+ - 1;\s+while \(\w+ >= 0\) {\s+(\w+)\.push\((\w+)\[\w+\]\);\s+\w+ \-= 1;\s+}";

            //PUSHKEY
            reg["PUSHKEY"] =@"\s+var \w+ = \w+\.length;\s+var \w+ = \[\];\s+var \w+ = 0;\s+while \(\w+ < \w+\) {\s+(\w+)\.push\((\w+)\[\(\w+ \+ \w+\[(\d+)\]\) % \w+\]\);\s+\w+ \+= 1;\s+}";

            reg["stringify"] =@"\s+var (\w+) = window\.JSON\.stringify\(([a-zA-Z0-9_\[\]\(\)\,\. \+]+), function\(\w+, \w+\) {\s+return \w+ === undefined \? null : \w+;\s+}\);";
            //tocharcode
            reg["tocharcode_u"] =@"\s+while \(\w+ < \w+\) {\s+var \w+ = (\w+)\.charCodeAt\(\w+\);\s+(\w+)\.push\(\w+\);\s+\w+ \+= 1;\s+}";

            reg["tocharcode_e"] =@"\s+while \(\w+ < \w+\) {\s+var \w+ = (\w+)\.charCodeAt\(\w+\);\s+\w+ \+= 1;\s+(\w+)\.push\(\w+\);\s+}";

            reg["tocharcode_p"] = @"\s+while \(\w+ < \w+\.length\) {\s+(\w+)\.push\((\w+)\.charCodeAt\(\w+\)\);\s+\w+ \+= 1;\s+}";
            reg["tocharcode"] =@"\s+var \w+ = window\.atob\(\w+\);\s+var \w+ = \[\];\s+var \w+ = \w+\.length;\s+while \(\w+ < \w+\) {\s+var \w+ = (\w+)\.charCodeAt\(\w+\);\s+(\w+)\.push\(\w+\);\s+\w+ \+= 1;\s+}";

            //fromcharcode
            reg["fromcharcode_n"] = @"\s+var \w+ = 0;\s+var \w+ = \[\];\s+var \w+ = (\w+)\.length;\s+while \(\w+ < \w+\) {\s+var \w+ = \w+\[\w+\];\s+\w+ \+= 1;\s+var \w+ = window\.String\.fromCharCode\(\w+\);\s+(\w+)\.push\(\w+\);\s+}";
            reg["fromcharcode_u"] =@"\s+while \(\w+ < \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+var \w+ = window\.String\.fromCharCode\(\w+\);\s+(\w+)\.push\(\w+\);\s+\w+ \+= 1;\s+}";

            reg["fromcharcode"] =@"\s+var \w+ = \[\];\s+for \(var \w+ in \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+if \(\w+\.hasOwnProperty\(\w+\)\) {\s+var \w+ = window\.String\.fromCharCode\(\w+\);\s+(\w+)\.push\(\w+\);\s+}\s+}";

            reg["fromcharcode_s"] =@"\s+var \w+ = 0;\s+var \w+ = \[\];\s+var \w+ = \w+\.length;\s+while \(\w+ < \w+\) {\s+var \w+ = (\w+)\[\w+\];\s+var \w+ = window\.String\.fromCharCode\(\w+\);\s+(\w+)\.push\(\w+\);\s+\w+ \+= 1;\s+}";

            reg["KEY"] =@"\s+var \w+ = \w+\((\d+), \w+\);\s+var \w+ = \[\];\s+var \w+ = 0;\s+while \(\w+ < (\d+)\) {\s+\w+\.push\(\w+\(\) & 255\);\s+\w+ \+= 1;\s+}\s+var \w+ = \w+;\s+var (\w+) = \w+;";
            reg["KEY_U"] =@"\s+var \w+ = \w+\((\d+), \w+\);\s+var \w+ = \[\];\s+var \w+ = 0;\s+while \(\w+ < (\d+)\) {\s+(\w+)\.push\(\w+\(\) & 255\);\s+\w+ \+= 1;\s+}";
            #endregion

            foreach (var item in reg)
            {
                var pattern = item.Value;
                var Key = item.Key;

                if (Key == "XOR")
                {
                    var m = Regex.Match(beaty, pattern);
                    while(m.Success)
                    {
                        var KeyName = m.Groups[1].Value;
                        var startKey = m.Groups[2].Value;
                        var endKey = m.Groups[3].Value;
                        var valueArg = m.Groups[m.Groups.Count - 2].Value;
                        var valueName = m.Groups[m.Groups.Count - 1].Value;
                        var xorPattern = pattern.Replace("(\\d+), (\\d+)", startKey + ", " + endKey);

                        var replacment = "\n " + valueName + " = " + Key + "("+ KeyName + ", " + valueArg + ", " + startKey + ", " + endKey + ");";
                        var rgx = new Regex(xorPattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if (Key == "DPUSH")
                {
                    var m = Regex.Match(beaty, pattern);
                    while (m.Success)
                    {
                        var keyName = m.Groups[1].Value;
                        var keyLen = m.Groups[m.Groups.Count - 3].Value;
                        var valueName = m.Groups[m.Groups.Count - 1].Value;
                        var argName = m.Groups[m.Groups.Count - 2].Value;
                        var replacment = "\n" + valueName + " = " + Key + "(" + keyName + "," + keyLen+ "," + argName + ");";
                        var rgx = new Regex(pattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if (Key == "XOR_S")
                {
                    var m = Regex.Match(beaty, pattern);
                    while (m.Success)
                    {
                        var argName = m.Groups[m.Groups.Count - 3].Value;
                        var keyToken = m.Groups[m.Groups.Count - 2].Value;
                        var valueName = m.Groups[m.Groups.Count - 1].Value;
                        var replacment = "\n" + valueName + " = " + Key + "(" + argName + ", \"" + keyToken + "\");";
                        var rgx = new Regex(pattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if (Key == "XOR_U")
                {
                    var m = Regex.Match(beaty, pattern);
                    while (m.Success)
                    {
                        var argName = m.Groups[m.Groups.Count - 2].Value;
                        var keyToken = m.Groups[m.Groups.Count - 3].Value;
                        var valueName = m.Groups[m.Groups.Count - 1].Value;
                        var replacment = "\n" + valueName + " = " + Key + "(" + argName + ", \"" + keyToken + "\");";
                        var rgx = new Regex(pattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if (Key == "PUSHKEY")
                {
                    var m = Regex.Match(beaty, pattern);
                    while(m.Success)
                    {
                        var pushKey = m.Groups[1].Value;
                        var pushPattern = pattern.Replace("(\\d+)", pushKey);
                        var valueName = m.Groups[m.Groups.Count - 1].Value;

                        var replacment = "\n" + valueName + " = " + Key + "(" + pushKey + ");";
                        var rgx = new Regex(pushPattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if (Key == "REM")
                {
                    var m = Regex.Match(beaty, pattern);
                    while(m.Success)
                    {
                        var keyRem = m.Groups[1].Value;
                        var remPattern = pattern.Replace("(\\d+)", keyRem);

                        var valueName = m.Groups[m.Groups.Count - 2].Value;
                        var argName = m.Groups[m.Groups.Count - 1].Value;

                        var replacment = "\n" + valueName + " = " + Key + "(" + argName + "," + keyRem + ");";
                        var rgx = new Regex(remPattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if (Key == "REM_S")
                {
                    var m = Regex.Match(beaty, pattern);
                    while(m.Success)
                    {
                        var keyRem = m.Groups[1].Value;
                        var remPattern = pattern.Replace("([0-9\\, ]+)", keyRem);

                        var valueName = m.Groups[m.Groups.Count - 2].Value;
                        var argName = m.Groups[m.Groups.Count - 1].Value;

                        var replacment = "\n" + valueName + " = " + Key + "(" + argName+ "," + keyRem + ");";
                        var rgx = new Regex(remPattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if (Key == "REM_A")
                {
                    var m = Regex.Match(beaty, pattern);
                    while (m.Success)
                    {
                        var keyRem = m.Groups[m.Groups.Count - 1].Value;
                        var keyName = m.Groups[m.Groups.Count - 2].Value;
                        var remPattern = pattern.Replace("(\\d+)", keyRem);

                        var valueName = m.Groups[m.Groups.Count - 4].Value;
                        var argName = m.Groups[m.Groups.Count - 3].Value;

                        var replacment = "\n" + valueName + " = " + Key + "(" + argName + "," + keyName + "," + keyRem + ");";
                        var rgx = new Regex(remPattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if(Key == "REM_P")
                {
                    var m = Regex.Match(beaty, pattern);
                    while (m.Success)
                    {
                        var keyRem = m.Groups[1].Value;
                        var remPattern = pattern.Replace("(\\d+)", keyRem);

                        var valueName = m.Groups[m.Groups.Count - 2].Value;
                        var argName = m.Groups[m.Groups.Count - 1].Value;

                        var replacment = "\n" + valueName + " = " + Key + "(" + argName + "," + keyRem + ");";
                        var rgx = new Regex(remPattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if (Key == "KEY")
                {
                    var m = Regex.Match(beaty, pattern);
                    while(m.Success)
                    {
                        var keyGen = m.Groups[1].Value;
                        var keyLength = m.Groups[2].Value;
                        var valueName = m.Groups[m.Groups.Count - 1].Value;

                        var keyGenPattern = pattern.Replace(@"\w+\((\d+), \w+\);", @"\w+\(" + keyGen + @", \w+\);").Replace(@"while \(\w+ < (\d+)\)", @"while \(\w+ < (" + keyLength + @")\)");

                        var replacment = "\n" + valueName + " = " + Key + "(" + keyGen + ", " + keyLength + ");";
                        var rgx = new Regex(keyGenPattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if (Key == "KEY_U")
                {
                    var m = Regex.Match(beaty, pattern);
                    while (m.Success)
                    {
                        var keyGen = m.Groups[1].Value;
                        var keyLength = m.Groups[2].Value;
                        var valueName = m.Groups[m.Groups.Count - 1].Value;

                        var keyGenPattern = pattern.Replace(@"\w+\((\d+), \w+\);", @"\w+\(" + keyGen + @", \w+\);").Replace(@"while \(\w+ < (\d+)\)", @"while \(\w+ < (" + keyLength + @")\)");

                        var replacment = "\n" + valueName + " = KEY(" + keyGen + ", " + keyLength + ");";
                        var rgx = new Regex(keyGenPattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if(Key.StartsWith("REVMIN"))
                {
                    var m = Regex.Match(beaty, pattern);
                    try
                    {
                        while (m.Success)
                        {
                            var valueName = m.Groups[m.Groups.Count - 2].Value;
                            var argName = m.Groups[m.Groups.Count - 1].Value;
                            var replacment = "\n" + valueName + " = " + Key + "(" + argName + ");";
                            var rgx = new Regex(pattern);
                            beaty = rgx.Replace(beaty, replacment, 1);
                            m = m.NextMatch();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else if (Key.StartsWith("NOP"))
                {
                    var m = Regex.Match(beaty, pattern);
                    while (m.Success)
                    {
                        var valueName = m.Groups[m.Groups.Count - 1].Value;
                        var argName = m.Groups[m.Groups.Count - 2].Value;
                        var replacment = "\n" + valueName + " = " + Key + "(" + argName + ");";
                        var rgx = new Regex(pattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if (Key.StartsWith("MIN"))
                {
                    var m = Regex.Match(beaty, pattern);
                    while (m.Success)
                    {
                        var valueName = m.Groups[m.Groups.Count - 2].Value;
                        var argName = m.Groups[m.Groups.Count - 1].Value;
                        var replacment = "\n" + valueName + " = " + Key + "(" + argName + ");";
                        var rgx = new Regex(pattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if(Key == "stringify")
                {
                    var m = Regex.Match(beaty, pattern);
                    while (m.Success)
                    {
                        var valueName = m.Groups[m.Groups.Count - 2].Value;
                        var argName = m.Groups[m.Groups.Count - 1].Value;

                        var replacment = "\n" + valueName + " = " + Key + "(" + argName + ");";
                        var rgx = new Regex(pattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else if(Key == "stringify_ex")
                {
                    var m = Regex.Match(beaty, pattern);
                    while (m.Success)
                    {
                        var valueName = m.Groups[m.Groups.Count - 2].Value;
                        var argName = m.Groups[m.Groups.Count - 1].Value;

                        var replacment = "\n" + valueName + " = " + Key + "(" + argName + ");\n";
                        var rgx = new Regex(pattern);
                        beaty = rgx.Replace(beaty, replacment, 1);
                        m = m.NextMatch();
                    }
                }
                else
                {
                    var m = Regex.Match(beaty, pattern);
                    try
                    {
                        while (m.Success)
                        {
                            var valueName = m.Groups[m.Groups.Count - 1].Value;
                            var argName = m.Groups[m.Groups.Count - 2].Value;
                            var replacment = "\n" + valueName + " = " + Key + "(" + argName + ");";
                            var rgx = new Regex(pattern);
                            beaty = rgx.Replace(beaty, replacment, 1);
                            m = m.NextMatch();
                        }
                    }catch(Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }

            path = currentDir + FileNameNew;
            File.WriteAllText(path, beaty);

            return beaty;
        }
        private static string DecodedReplace(string content, Dictionary<string, string> strings, string fileName)
        {
            try
            {
                foreach (KeyValuePair<string, string> s in strings)
                {
                    var v = s.Key;
                    string pattern = v + @"\.substr\((\d+), (\d+)\)";
                    Match m = Regex.Match(content, pattern);

                    while (m.Success)
                    {
                        int startAt = Convert.ToInt32(m.Groups[1].Value);
                        int FinishAt = Convert.ToInt32(m.Groups[2].Value);

                        var new_word = "'" + s.Value.Substring(startAt, FinishAt) + "'";

                        content = content.Replace(m.Groups[0].Value, new_word);
                        m = m.NextMatch();
                    }
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            
            File.WriteAllText(fileName, content);
            return content;
        }
 
        static string BytesToString(List<long> list)
        {
            string newStr = "";
            for (int i = 0; i < list.Count; i++)
                newStr += (char)(list[i]);

            return newStr;
        }
        static long[] ToCharCode(string c4)
        {
            long[] arrOut = new long[c4.Length];
            for (int i = 0; i < c4.Length; i++)
                arrOut[i] = (int)c4[i];
            return arrOut;
        }
        static List<long> FromCharCode(string p)
        {
            var bb = Convert.FromBase64String(p);
            List<long> list = new List<long>();

            for (int i = 0; i < bb.Length; i++)
            {
                list.Add(bb[i]);
            }

            return list;
        }

        private static string GetBToA(long [] arr)
        {
            string ss = "";
            byte [] bb = new byte [arr.Length];

            for (int i = 0; i < arr.Length; i++)
                bb[i] = (byte)arr[i];


            ss = Convert.ToBase64String(bb);
            return ss;
        }
        private static string GetAtoB(string base64Encoded)
        {
            string base64Decoded = "";
            byte[] data = Convert.FromBase64String(base64Encoded);
            
            for (int i = 0; i < data.Length; i++)
                base64Decoded += (char)(data[i]);
            return base64Decoded;
        }

        private static long[] GetRem(long[] arr, int k)
        {
            long [] arrOut = new long[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                arrOut[i] = arr[(i + arr.Length - k) % arr.Length];

            return arrOut;
        }
        private static long[] GetRemA(long[] arr, int k)
        {
            long[] arrOut = new long[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                arrOut[i] = arr[(i + k) % arr.Length];
            return arrOut;
        }

        private static long[] GetURemA(long[] arr, int k)
        {
            long[] arrOut = new long[arr.Length];
            int j = 0;
            for (int i = arr.Length - k % arr.Length; i < arr.Length; i++)
            {
                arrOut[j] = arr[i];
                j++;
            }

            for (int i = 0; i < arr.Length - k % arr.Length; i++)
            {
                arrOut[j] = arr[i];
                j++;
            }

            return arrOut;
        }
        private static long[] GetRemP(long[] arr, int k)
        {
            long[] arrOut = new long[arr.Length];
            var OW = k % arr.Length;

            for (int i = 0; i < arr.Length; i++)
                arrOut[i] = arr[(i + arr.Length - OW) % arr.Length];

            return arrOut;
        }
        static long[] Get244(long[] arr)
        {
            long[] arrOut = new long[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                arrOut[i] = arr[i] << 4 & 240 | arr[i] >> 4;

            return arrOut;
        }
        static long[] GetXor(long[] arr, long[] key)
        {
            long[] arrOut = new long[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                var Bk = arr[i];
                var Bv = key[i % key.Length];
                arrOut[i] = (Bk ^ Bv);
            }
            return arrOut;
        }
        static long[] Shake(long[] arr)
        {
            var bl = arr.Length;
            var c2 = 0;
            while (c2 + 1 < bl)
            {
                var dI = arr[c2];
                arr[c2] = arr[c2 + 1];
                arr[c2 + 1] = dI;
                c2 += 2;
            }

            return arr;
        }
        static long[] RevMin(long[] arr)
        {
            long[] arrOut = new long[arr.Length];
            int j = 0;
            for (int i = arr.Length - 1; i >= 0; i--)
                arrOut[j++] = arr[i];
            return arrOut;
        }
        static long[] PushWithKey(long[] arr, long[] key)
        {
            long[] arrOut = new long[arr.Length * 2];
            int j = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                arrOut[j++] = arr[i];
                arrOut[j++] = (int)key[i % key.Length];
            }

            return arrOut;
        }
        static long[] UnPush(long[] arr)
        {
            long[] arrOut = new long[arr.Length / 2 + arr.Length % 2];
            int j = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (i % 2 == 0)
                    arrOut[j++] = arr[i];
            }
            return arrOut;
        }
        static long[] UnPushWithKey(long[] list, int key)
        {
            long[] newBb = new long[list.Length];
            int j = 0;

            for (int i = list.Length - key; i < list.Length; i++, j++)
                newBb[j] = list[i];

            for (int i = 0; i < list.Length - key; i++, j++)
                newBb[j] = list[i];

            return newBb;
        }
        private static long[] GetKey(Dictionary<int,long[]> dict, int index)
        {
            return dict[index];
        }

        private static async Task<long[]> UpdateKey(string random_token_each_session, string token, int size)
        {
            long[] key = new long[size];
            var engine = new Engine();

            Func<byte[], string> btoa = b => Convert.ToBase64String(b);

            engine.SetValue("btoa", btoa);

            var script = @"
                    function getKey(TE, token, size) {
                        function hr(D_, wF)
                        {
                            var gk = wF;
                            var el = D_;
                            return function() {
                                var LJ = gk;
                                var jZ = el;
                                el = LJ;
                                jZ ^= jZ << 23;
                                jZ ^= jZ >> 17;
                                jZ ^= LJ;
                                jZ ^= LJ >> 26;
                                gk = jZ;
                                return (el + gk) % 4294967296;
                            };
                        }
                        var R1 = hr(token, TE);
                        var fb = [];
                        var x4 = 0;
                        while (x4 < size)
                        {
                            fb.push(R1() & 255);
                            x4 += 1;
                        }
                        return btoa(fb);
                    }
                    var a = getKey(##TE##, ##TOKEN##, ##SIZE##);
                ";

            script = script.Replace("##TE##", random_token_each_session).Replace("##TOKEN##", token).Replace("##SIZE##", size.ToString());
            try
            {
                var ns = engine.Execute(script);
                var value = ns.GetValue("a").AsString();
                var bb = Convert.FromBase64String(value);

                for (int i = 0; i < bb.Length; i++)
                    key[i] += (int)(bb[i]);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return key;
        }

    }
}
